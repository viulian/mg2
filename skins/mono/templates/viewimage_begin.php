<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
  <title><?php $mg2->output('gallerytitle') ?></title>
  <meta name="title" content="<?php $mg2->output('gallerytitle') ?>" />
  <meta name="robots" content="noindex,nofollow" />
  <meta http-equiv="Content-Type" content="text/html; charset=<?php $mg2->output('charset') ?>" />
  <link href="skins/<?php $mg2->output('activeskin') ?>/css/style.css" rel="stylesheet" type="text/css"></link>
</head>
<body class="mg2body">
<table cellspacing="0" cellpadding="0" class="table-top" width="100%">
<tr valign="top">
  <td><?php $mg2->gallerynavigation(":") ?></td>
</tr>
</table>
<table cellspacing="0" cellpadding="0" class="table-headline" width="100%">
<tr>
  <td class="iconbar">
    <a href="<?php $mg2->output('indexfile') ?>?slideshow=<?php $mg2->output('startimage') ?>"><img src="skins/<?php $mg2->output('activeskin') ?>/images/slideshow.gif" width="16" height="16" alt="<?php echo $mg2->lang['viewslideshow'] ?>" title="<?php echo $mg2->lang['viewslideshow'] ?>" border="0" /></a>
  </td>
  <td class="headline"><?php $mg2->output('title') ?></td></tr>
</table>
<br />
<div align="center">
  <?php $mg2->output('nav_first') ?> &nbsp;
  <?php $mg2->output('nav_prev') ?> &nbsp;
  <?php $mg2->output('nav_current') ?> <?php echo $mg2->lang['of'] ?> <?php $mg2->output('nav_total') ?> &nbsp;
  <?php $mg2->output('nav_next') ?> &nbsp;
  <?php $mg2->output('nav_last') ?>
</div>
<br />
  <div align="center"><?php echo $mg2->fullsizelink ?></div>
<table cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td class="viewimage"><a href="<?php $mg2->output('link') ?>" target="<?php $mg2->output('target') ?>" title="<?php echo $title ?>"><img src="<?php $mg2->output('imagefile') ?>" border="0" width="<?php $mg2->output('width') ?>" height="<?php $mg2->output('height') ?>" alt="" title="" /></a></td>
  </tr>
</table>
<br />
<div class="description"><?php $mg2->output('description') ?></div><br />
<div class="copyright"><?php $mg2->output('copyright') ?></div>

