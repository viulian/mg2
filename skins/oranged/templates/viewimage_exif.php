<br />
<br />
<div class="exif" align="center">
  <b><?php echo $mg2->lang['exif info'] ?></b>
  <br />
  <br />
  <strong><?php echo $mg2->lang['model'] ?></strong>
  <?php echo $exif_data['Model'] ?>
  <strong><?php echo $mg2->lang['shutter'] ?></strong>
  <?php echo $exif_data['ExposureTime'] ?>
  <strong><?php echo $mg2->lang['aperture'] ?></strong>
  <?php echo $exif_data['FNumber'] ?>
  <strong><?php echo $mg2->lang['focallength'] ?></strong>
  <?php echo $exif_data['FocalLength'] ?>
  <strong><?php echo $mg2->lang['iso'] ?></strong>
  <?php echo $exif_data['ISOSpeedRating'] ?>
  <br />
  <strong><?php echo $mg2->lang['exposurecomp'] ?></strong>
  <?php echo $exif_data['ExposureBias'] ?>
  <strong><?php echo $mg2->lang['flash'] ?></strong>
  <?php echo $exif_data['Flash'][1] ?>
  <strong><?php echo $mg2->lang['original'] ?></strong>
  <?php echo $exif_data['DateTime'] ?>
</div>
