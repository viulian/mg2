# Introduction

MiniGal (MG2) 0.5.1 is a very old image gallery software written by Thomas Rybak (www.minigal.dk).

This repository is the original code, but updated to work with PHP 7.4 with few bugs fixed.

# CVEs

MG2 0.5.1 suffers from few reported bugs:

| CVE | Description |
| ----------- | ----------- |
|CVE-2006-0493|Cross-site scripting (XSS) vulnerability in MG2 (formerly known as Minigal) 0.5.1 allows remote attackers to inject arbitrary web script or HTML via the Name field in a comment associated with a picture.|
|CVE-2005-3432|MiniGal 2 (MG2) 0.5.1 allows remote attackers to list password protected images via a request to index.php with the list parameter set to * (wildcard) and the page parameter set to all.|
|CVE-2008-1228|Cross-site scripting (XSS) vulnerability in admin.php in MG2 (formerly Minigal) allows remote attackers to inject arbitrary web script or HTML via the list parameter in an import action.|

## Sanitizing input arguments

After checking the source code, it is obvious that it accesses `$_REQUEST[...]` values, and sometimes these values get a bit sanitized, but most of the time they do not.

I've extracted the $_REQUEST arguments using:

```find . -name \*.php -exec perl -ne 'print "$1\n" if /_REQUEST\[(.*?)\]/' {} \; | sort | uniq```

I discovered that the code is well thought of, most of the arguments are supposed to be IDs (IDs of albums, or of a photo, or where the slide show to start, etc). These are easily sanitized, and since I do not know of a better way, I decided to create a `sanitize.php` script that is to be imported from either `index.php` as well as from `admin.php` which will make sure that the ID arguments are really integers, otherwise, they will be reset to empty string.

An interesting one is 'page', which can be either an Integer, or it can be 'all'. This is handled independently also.

Below, a detailed list of arguments that are either sanitized (or harmless), or not sanitized (but only available to admin users).

Sanitized or harmless:

|argument|observations|
|------|-----|
|action||
|list||
|page||
|password||
|action||
|changelang|sanitized to empty as `changelanguage()` method (that it would trigger) doesn't exist|
|deleteID|sanitized (but used only in admin)|
|deletefolder|sanitized (but used only in admin)|
|deletepassword|sanitized (but used only in admin)|
|deletethumb|sanitized (but used only in admin)|
|direction|harmless (but used only in admin, value checked to match either `"left"` or `"right"`)|
|editID|sanitized (but used only in admin)|
|editfolder|sanitized (but used only in admin)|
|erasefolder|sanitized (but used only in admin)|
|id|sanitized|
|list|sanitized|
|moveto|sanitized|
|next|removed. Was used only in `admin2_edit.php` but the `<input>` element was never used.|
|oldpassword|harmless (only used with md5 etc)|
|page|sanitized|
|rebuildID|sanitized (but used only in admin)|
|rotate|sanitized (but used only in admin)|
|slideshow|sanitized|

Not sanitized, as they are used as "admin" OR during installation:

|argument|observations|
|------|-----|
|activeskin||
|adminemail||
|autolang||
|copyright||
|dateformat||
|defaultlang||
|extensions||
|foldericons||
|gallerytitle||
|imagecolumns||
|imagerows||
|indexfile||
|marknew||
|mediumimge||
|"selectfile" . $i||
|sendmail||
|showcomments||
|showexif||
|slideshowdelay||
|thumbquality||
|websitelink||
|'comment' . $i|accessed in deletecomments() in admin screens.|
|description||
|email|used for comments feature (which should be disabled)|
|filename|used for comments feature (which should be disabled) or in editID() as admin. Very hard to sanitize|
|gallerytitle|
|input|used for comments feature (which should be disabled)|
introtext|description for folder, used in admin only|
|name|used for comments feature (which should be disabled) or as the name of the folder in the admin screens|
|password||
|selectsize||
|setthumb||
|sortby||
|step||
|submit||
|title||
|totalcomments||
|uploadto||
|websitelink||

This sanitization fixes `CVE-2005-3432` for non-admin users (by whitelisting allowed values before they get handled by the code). This also fixes `CVE-2008-1228` as `list` argument part of the values that are sanitized. 

## Disabing comments

Unfortunatelly, the `addcomments` feature seems tedious to be made right thus, it would be better to disable the functionality alltoghether. Trouble is that the `'addcomments'` action can be triggered even if `'showcomments'` is disabled -> leading to possible abuses.

Thus, I have also modified the `index.php` to condition the `'addcomments'` action based on the `'showcomments'` setting. Much safer (for now) to keep the Comments functionality disabled.

To disable the coments feature, modify `mg2_settings.php` to:

```
...
$mg2->showcomments = "";
...
```

Also, it is safer to inspect any `*.comments` file that is present in the gallery, as they may contain unwanted behaviours.

This avoids `CVE-2006-0493`.