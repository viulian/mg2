﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                    Icelandic/Íslenska                               //
//                                                                                     //
//                               TRANSLATED BY: Ingaló.                           	   //
//                               EMAIL: kattarofeti@hotmail.com                        //
//                                                                                     //
//                               LAST UPDATED: 24. Feb 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////


//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Myndamappa";
$mg2->lang['of']                                  = "af";
$mg2->lang['first']                               = "Fyrsta";
$mg2->lang['prev']                                = "Fyrri";
$mg2->lang['next']                                = "N&aelig;sta";
$mg2->lang['last']                                = "Seinasta";
$mg2->lang['thumbs']                              = "Sm&aacute;myndir";
$mg2->lang['exif info']                           = "Exif Upplýsingar";
$mg2->lang['model']                               = "Ger&eth;";
$mg2->lang['shutter']                             = "Lokahra&eth;i";
$mg2->lang['viewslideshow']                       = "Rennis&yacute;ning";
$mg2->lang['stopslideshow']                       = "St&ouml;&eth;va rennis&yacute;ningu";
$mg2->lang['aperture']                            = "Lj&oacute;sop";
$mg2->lang['flash']                               = "Flass";
$mg2->lang['focallength']                         = "Brenniv&iacute;dd";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Lj&oacute;smagnsuppb&oacute;tarstilling (+/-)";
$mg2->lang['original']                            = "Upprunaleg mynd";
$mg2->lang['metering']                            = "Lj&oacute;smagn";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "sek.";
$mg2->lang['page']                                = "Bla&eth;s&iacute;&eth;a";
$mg2->lang['all']                                 = "Allar";
$mg2->lang['fullsize']                            = "Sko&eth;a mynd &iacute; fullri st&aelig;r&eth;";
$mg2->lang['addcomment']                          = "B&aelig;ta vi&eth; athugasemd";
$mg2->lang['name']                                = "Nafn";
$mg2->lang['email']                               = "Netfang";
$mg2->lang['commentadded']                        = "Athugasemd hefur veri&eth; b&aelig;tt vi&eth;";
$mg2->lang['commentexists']                       = "VILLA: Athugasemd er n&uacute; &thorn;egar til sta&eth;ar!";
$mg2->lang['commentmissing']                      = "VILLA: Fylla ver&eth;ur inn alla reiti!";
$mg2->lang['enterpassword']                       = "Sl&aacute;&eth;u inn lykilor&eth;";
$mg2->lang['thissection']                         = "&THORN;essi mappa er varin me&eth; lykilor&eth;i.";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "R&oacute;t myndam&ouml;ppu";
$mg2->lang['thumb']                               = "Sm&aacute;mynd";
$mg2->lang['dateadded']                           = "B&aelig;tt vi&eth; &thorn;ann";
$mg2->lang['upload']                              = "N&aacute; &iacute; skr&aacute;r";
$mg2->lang['import']                              = "Senda skr&aacute;r sem n&aacute;&eth; hefur veri&eth; &iacute; til";
$mg2->lang['newfolder']                           = "N&yacute; mappa";
$mg2->lang['viewgallery']                         = "Sko&eth;a myndam&ouml;ppu";
$mg2->lang['setup']                               = "Uppsetning";
$mg2->lang['logoff']                              = "&Uacute;tskr&aacute;";
$mg2->lang['menutxt_upload']                      = "N&aacute; &iacute; myndir";
$mg2->lang['menutxt_import']                      = "Flytja inn";
$mg2->lang['menutxt_newfolder']                   = "N&yacute; mappa";
$mg2->lang['menutxt_viewgallery']                 = "Sko&eth;a Myndam&ouml;ppu";
$mg2->lang['menutxt_setup']                       = "Uppsetning";
$mg2->lang['menutxt_logoff']                      = "&Uacute;tskr&aacute;";
$mg2->lang['delete']                              = "Ey&eth;a";
$mg2->lang['cancel']                              = "H&aelig;tta vi&eth;";
$mg2->lang['ok']                                  = "&iacute; lagi";
$mg2->lang['deletefolder']                        = "Ey&eth;a m&ouml;ppu";
$mg2->lang['navigation']                          = "Lei&eth;arv&iacute;sir";
$mg2->lang['images']                              = "mynd(ir)";
$mg2->lang['filename']                            = "Skr&aacute;arnafn";
$mg2->lang['title']                               = "Titill";
$mg2->lang['description']                         = "L&yacute;sing";
$mg2->lang['setasthumb']                          = "Nota sem sm&aacute;mynd fyrir m&ouml;ppuna";
$mg2->lang['editfolder']                          = "Breyta m&ouml;ppu";
$mg2->lang['editimage']                           = "Breyta mynd";
$mg2->lang['nofolderselected']                    = "Engin mappa er valin";
$mg2->lang['foldername']                          = "Nafn m&ouml;ppu";
$mg2->lang['newpassword']                         = "N&yacute;tt lykilor&eth;";
$mg2->lang['deletepassword']                      = "Ey&eth;a lykilor&eth;i";
$mg2->lang['introtext']                           = "Inngangstexti";
$mg2->lang['deletethumb']                         = "Ey&eth;a sm&aacute;mynd";
$mg2->lang['moveto']                              = "F&aelig;ra til";
$mg2->lang['id']                                  = "Au&eth;kenni";
$mg2->lang['filesize']                            = "Skr&aacute;arst&aelig;r&eth;";
$mg2->lang['width']                               = "Breidd";
$mg2->lang['height']                              = "H&aelig;&eth;";
$mg2->lang['date']                                = "Dagsetning";
$mg2->lang['ascending']                           = "Uppsveigt";
$mg2->lang['descending']                          = "Ni&eth;ursveigt";
$mg2->lang['newfolder']                           = "N&yacute; mappa";
$mg2->lang['password']                            = "Lykilor&eth;";
$mg2->lang['direction']                           = "&Aacute;tt";
$mg2->lang['sortby']                              = "Ra&eth;a eftir";
$mg2->lang['gallerytitle']                        = "Nafn m&ouml;ppu";
$mg2->lang['adminemail']                          = "Netfang kerfisstj&oacute;ra";
$mg2->lang['language']                            = "Tungum&aacute;l";
$mg2->lang['skin']                                = "&Uacute;tlit";
$mg2->lang['dateformat']                          = "Birting dagsetningar";
$mg2->lang['DDMMYY']                              = "DD MMM &Aacute;&Aacute;&Aacute;&Aacute;";
$mg2->lang['MMDDYY']                              = "MMM DD, &Aacute;&Aacute;&Aacute;&Aacute;";
$mg2->lang['MM.DD.YY']                            = "MM.DD.&Aacute;&Aacute;";
$mg2->lang['DD.MM.YY']                            = "DD.MM.&Aacute;&Aacute;";
$mg2->lang['YYYYMMDD']                            = "&Aacute;&Aacute;&Aacute;&Aacute;MMDD";
$mg2->lang['sendmail']                            = "Senda p&oacute;st me&eth; athugasemdum";
$mg2->lang['foldericons']                         = "Ney&eth;a t&aacute;knmynd fyrir m&ouml;ppu";
$mg2->lang['showexif']                            = "S&yacute;na Exif-uppl&yacute;singar";
$mg2->lang['allowcomments']                       = "Leyfa athugasemdir";
$mg2->lang['copyright']                           = "H&ouml;fundarr&eacute;ttarklausa";
$mg2->lang['passwordchange']                      = "Breyta lykilor&eth;i (3 x t&oacute;m = n&uacute;verandi helst)";
$mg2->lang['oldpasswordsetup']                    = "N&uacute;verandi lykilor&eth;";
$mg2->lang['newpasswordsetup']                    = "N&yacute;tt lykilor&eth; (t&oacute;mt = n&uacute;verandi nota&eth;)";
$mg2->lang['newpasswordsetupconfirm']             = "N&yacute;tt lykilor&eth; aftur";
$mg2->lang['advanced']                            = "N&aacute;kv&aelig;mar";
$mg2->lang['allowedextensions']                   = "Leyf&eth;ar endingar";
$mg2->lang['imgwidth']                            = "H&aacute;marksbreidd mynda (0 = &oacute;virkt)";
$mg2->lang['indexfile']                           = "Index skr&aacute; myndam&ouml;ppu";
$mg2->lang['thumbquality']                        = "G&aelig;&eth;i sm&aacute;mynda";
$mg2->lang['uploadimport']                        = "Mundu a&eth; flytja inn myndirnar &thorn;&iacute;nar eftir a&eth; hafa n&aacute;&eth; &iacute; &thorn;&aelig;r!";
$mg2->lang['image']                               = "Mynd";
$mg2->lang['edit']                                = "Breyta";
$mg2->lang['editcurrentfolder']                   = "Breyta &thorn;essari m&ouml;ppu";
$mg2->lang['deletecurrentfolder']                 = "Ey&eth;a &thorn;essari m&ouml;ppu";
$mg2->lang['by']                                  = "eftir";
$mg2->lang['loginagain']                          = "Innskr&aacute; aftur";
$mg2->lang['securitylogoff']                      = "Öryggis&uacute;tskr&aacute;ning";
$mg2->lang['autologoff']                          = "&THORN;&uacute; hefur veri&eth; &uacute;tskr&aacute;&eth;(ur) sj&aacute;lfvirkt eftir 15 m&iacute;n&uacute;tna &oacute;virkni.";
$mg2->lang['logoff']                              = "&Uacute;tskr&aacute;";
$mg2->lang['forsecurity']                         = "Af &ouml;ryggis&aacute;st&aelig;&eth;um er m&aelig;lt me&eth; &thorn;v&iacute; a&eth; &thorn;&uacute; lokir &thorn;essum vafraglugga.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">&THORN;essi uppsetning er X daga g&ouml;mul. Smelltu h&eacute;r til a&eth; n&aacute; &iacute; uppb&aelig;tur!</a></b>";
$mg2->lang['updatesuccess']                       = "Uppf&aelig;rsla t&oacute;kst";
$mg2->lang['renamefailure']                       = "VILLA: Skr&aacute;arnafn inniheldur &oacute;l&ouml;gleg t&aacute;kn!";
$mg2->lang['filedeleted']                         = "Skr&aacute; hefur veri&eth; eytt";
$mg2->lang['filenotfound']                        = "Skr&aacute; finnst ekki!";
$mg2->lang['filesimported']                       = "skr&aacute;(r) innfluttar";
$mg2->lang['nofilestoimport']                     = "VILLA: Engar skr&aacute;r til a&eth; flytja inn!";
$mg2->lang['foldernotempty']                      = "VILLA: Mappan er ekki t&oacute;m!";
$mg2->lang['folderdeleted']                       = "M&ouml;ppu hefur veri&eth; eytt";
$mg2->lang['folderupdated']                       = "Mappa hefur veri&eth; uppf&aelig;r&eth;";
$mg2->lang['foldercreated']                       = "Mappa hefur veri&eth; b&uacute;in til";
$mg2->lang['folderexists']                        = "VILLA: Nafn m&ouml;ppu n&uacute; &thorn;egar til!";
$mg2->lang['filesuploaded']                       = "N&aacute;&eth; hefur veri&eth; &iacute; skr&aacute;r";
$mg2->lang['settingssaved']                       = "Stillingar vista&eth;ar";
$mg2->lang['nopwdmatch']                          = "Stillingar vista&eth;ar<br /><br />VILLA: Sitthvort lykilor&eth;i&eth;, lykilor&eth; ekki vista&eth;!";
$mg2->lang['filesmovedto']                        = "skr&aacute;(r) f&aelig;r&eth;ar til";
$mg2->lang['filesdeleted']                        = "skr&aacute;(m) eytt!";
$mg2->lang['file']                                = "skr&aacute;";
$mg2->lang['files']                               = "skr&aacute;r";
$mg2->lang['folder']                              = "mappa";
$mg2->lang['folders']                             = "m&ouml;ppur";
$mg2->lang['rebuild']                             = "Endurbyggja";
$mg2->lang['rebuildimages']                       = "Endurbyggja sm&aacute;myndir";
$mg2->lang['rebuildsuccess']                      = "Endurbyggingu loki&eth;";
$mg2->lang['donate']                              = "MG2 er gjaldfr&iacute;r hugb&uacute;na&eth;ur, skr&aacute;&eth;ur undir GPL. Ef hugb&uacute;na&eth;urinn hefur veri&eth; &thorn;&eacute;r gagnlegur, vinsamlegast styrktu h&ouml;fundinn me&eth; &thorn;v&iacute; a&eth; smella &aacute; hnappinn h&eacute;r fyrir ne&eth;an.";
$mg2->lang['from']                                = "Fr&aacute;";
$mg2->lang['comment']                             = "Athugasemd";
$mg2->lang['comments']                            = "Athugasemdir";
$mg2->lang['by']                                  = "eftir";
$mg2->lang['commentsdeleted']                     = "Athugasemd(um) hefur veri&eth; eytt";
$mg2->lang['buttonmove']                          = "F&aelig;ra";
$mg2->lang['buttondelete']                        = "Ey&eth;a";
$mg2->lang['deleteconfirm']                       = "Eyða völdum skrám?";
$mg2->lang['imagecolumns']                        = "D&aacute;lkar af myndum";
$mg2->lang['imagerows']                           = "Ra&eth;ir af myndum";
$mg2->lang['viewfolder']                          = "Sko&eth;a m&ouml;ppu";
$mg2->lang['viewimage']                           = "Sko&eth;a mynd";
$mg2->lang['viewgallery']                         = "Sko&eth;a myndam&ouml;ppu";
$mg2->lang['rotateright']                         = "Sn&uacute;a um 90 gr&aacute;&eth;ur til h&aelig;gri";
$mg2->lang['rotateleft']                          = "Sn&uacute;a um 90 gr&aacute;&eth;ur til vinstri";
$mg2->lang['imagerotated']                        = "Mynd hefur veri&eth; sn&uacute;i&eth;!";
$mg2->lang['gifnotrotated']                       = "VILLA: Ekki er h&aelig;gt a&eth; sn&uacute;a .GIF myndum vegna takmarkana &iacute; GD lib!";
$mg2->lang['help']                                = "Hj&aacute;lp";
?>
