<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                MG2 LANGUAGE FILE:                                   //
//                               http://www.minigal.dk                                 //
//                                                                                     //
//                                  Portugese (BR)                                     //
//                                                                                     //
//                          Translated by: Eduardo Villas Boas                         //
//                             Email: villasweb@villasweb.com                          //
//                                                                                     //
//                             Last updated: 16. Jul  2005                             //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS (variáveis de linguagem)
$mg2->lang['gallery']                             = "Galeria";
$mg2->lang['of']                                  = "de";
$mg2->lang['first']                               = "Primeira";
$mg2->lang['prev']                                = "Anterior";
$mg2->lang['next']                                = "Pr&oacute;xima";
$mg2->lang['last']                                = "&Uacute;ltima";
$mg2->lang['thumbs']                              = "Miniaturas";
$mg2->lang['exif info']                           = "Informa&ccedil;&atilde;o Exif da Foto";
$mg2->lang['model']                               = "Mod&ecirc;lo:";
$mg2->lang['shutter']                             = "Velocidade do obturador:";
$mg2->lang['viewslideshow']                       = "Ver slideshow";
$mg2->lang['stopslideshow']                       = "Parar slideshow";
$mg2->lang['aperture']                            = "Diafragma:";
$mg2->lang['flash']                               = "Flash:";
$mg2->lang['focallength']                         = "Dist&acirc;ncia focal:";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Compensa&ccedil;&atilde;o de Exposi&ccedil;&atilde;o:";
$mg2->lang['original']                            = "Original:";
$mg2->lang['metering']                            = "Medi&ccedil;&otilde;o:";
$mg2->lang['iso']                                 = "ISO:";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "P&aacute;gina";
$mg2->lang['all']                                 = "Todas";
$mg2->lang['fullsize']                            = "Ampliar";
$mg2->lang['addcomment']                          = "Comentar";
$mg2->lang['name']                                = "Nome";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Coment&aacute;rio adicionado";
$mg2->lang['commentexists']                       = "ERRO: Coment&aacute;rio j&aacute; existente!";
$mg2->lang['commentmissing']                      = "ERRO: Deve preencher todos os campos!";
$mg2->lang['enterpassword']                       = "Digite a Senha";
$mg2->lang['thissection']                         = "Esta se&ccedil;&atilde;o est&aacute; protegida por senha";

// ADMIN LANGUAGE STRINGS (variáveis do painel admin)
$mg2->lang['root']                                = "Raiz";
$mg2->lang['thumb']                               = "Miniatura";
$mg2->lang['dateadded']                           = "Adicionado em";
$mg2->lang['upload']                              = "Enviar arquivos";
$mg2->lang['import']                              = "Importar arquivos enviados para";
$mg2->lang['newfolder']                           = "Nova Pasta";
$mg2->lang['viewgallery']                         = "Ver galeria";
$mg2->lang['setup']                               = "Configura&ccedil;&otilde;es";
$mg2->lang['logoff']                              = "Sair";
$mg2->lang['menutxt_upload']                      = "Enviar";
$mg2->lang['menutxt_import']                      = "Importar";
$mg2->lang['menutxt_newfolder']                   = "Nova Pasta";
$mg2->lang['menutxt_viewgallery']                 = "Ver galeria";
$mg2->lang['menutxt_setup']                       = "Configura&ccedil;&otilde;es";
$mg2->lang['menutxt_logoff']                      = "Sair";
$mg2->lang['delete']                              = "Deletar";
$mg2->lang['cancel']                              = "Cancelar";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Deletar pasta";
$mg2->lang['navigation']                          = "Navega&ccedil;&atilde;o";
$mg2->lang['images']                              = "imagem(s)";
$mg2->lang['filename']                            = "Nome do arquivo";
$mg2->lang['title']                               = "T&iacute;tulo";
$mg2->lang['description']                         = "Descri&ccedil;&atilde;o";
$mg2->lang['setasthumb']                          = "Definida pasta para miniaturas";
$mg2->lang['editfolder']                          = "Editar pasta";
$mg2->lang['editimage']                           = "Editar imagem";
$mg2->lang['nofolderselected']                    = "Nenhuma pasta selecionada";
$mg2->lang['foldername']                          = "Nome da pasta";
$mg2->lang['newpassword']                         = "Nova senha";
$mg2->lang['deletepassword']                      = "Deletar senha";
$mg2->lang['introtext']                           = "Texto de introdu&ccedil;&atilde;o";
$mg2->lang['deletethumb']                         = "Deletar Miniatura";
$mg2->lang['moveto']                              = "Mover para";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Arquivo (tipo/tamanho)";
$mg2->lang['width']                               = "Largura";
$mg2->lang['height']                              = "Altura";
$mg2->lang['date']                                = "Data";
$mg2->lang['ascending']                           = "Ascendente";
$mg2->lang['descending']                          = "Descendente";
$mg2->lang['newfolder']                           = "Nova Pasta";
$mg2->lang['password']                            = "Senha";
$mg2->lang['direction']                           = "Ordena&ccedil;&atilde;o";
$mg2->lang['sortby']                              = "Ordenar por";
$mg2->lang['gallerytitle']                        = "T&iacute;tulo da galeria";
$mg2->lang['adminemail']                          = "Email do Administrador";
$mg2->lang['language']                            = "Idioma";
$mg2->lang['skin']                                = "Estilo";
$mg2->lang['dateformat']                          = "Escolha o formato da data";
$mg2->lang['DDMMYY']                              = "DD MMM AAAA";
$mg2->lang['MMDDYY']                              = "MMM DD, AAAA";
$mg2->lang['MM.DD.YY']                            = "MM.DD.AA";
$mg2->lang['DD.MM.YY']                            = "DD.MM.AA";
$mg2->lang['YYYYMMDD']                            = "AAAAMMDD";
$mg2->lang['sendmail']                            = "Enviar novos coment&aacute;rios por e-mail";
$mg2->lang['foldericons']                         = "For&ccedil;ar icones de pasta";
$mg2->lang['showexif']                            = "Mostrar informa&ccedil;&atilde;o Exif das imagens";
$mg2->lang['allowcomments']                       = "Permitir coment&aacute;rios";
$mg2->lang['copyright']                           = "Nota de Copyright";
$mg2->lang['passwordchange']                      = "Mudar senha (3 x branco = manter atual)";
$mg2->lang['oldpasswordsetup']                    = "Digite a senha atual";
$mg2->lang['newpasswordsetup']                    = "Nova senha (branco = usar atual)";
$mg2->lang['newpasswordsetupconfirm']             = "Redigite a nova senha";
$mg2->lang['advanced']                            = "Avan&ccedil;ado";
$mg2->lang['allowedextensions']                   = "Permitir extens&otilde;es";
$mg2->lang['imgwidth']                            = "M&aacute;x. largura da imagem (0 = desabilitar)";
$mg2->lang['indexfile']                           = "Arquivo In&iacute;cio da galeria";
$mg2->lang['thumbquality']                        = "Qualidade JPG das miniaturas (0-100)";
$mg2->lang['image']                               = "Imagem";
$mg2->lang['edit']                                = "Editar";
$mg2->lang['editcurrentfolder']                   = "Editar pasta atual";
$mg2->lang['deletecurrentfolder']                 = "Deletar pasta atual";
$mg2->lang['by']                                  = "por";
$mg2->lang['loginagain']                          = "Entrar novamente";
$mg2->lang['securitylogoff']                      = "Desconectado por raz&otilde;es de seguran&ccedil;a";
$mg2->lang['autologoff']                          = "Voc&ecirc; foi automaticamente desconectado ap&oacute;s 15 minutos de inatividade.";
$mg2->lang['logoff']                              = "Sair";
$mg2->lang['forsecurity']                         = "Por raz&otilde;es de seguran&ccedil;a, &eacute; recomendado que feche esta janela!";
$mg2->lang['updatesuccess']                       = "Atualiza&ccedil;&atilde;o completada";
$mg2->lang['renamefailure']                       = "ERRO: N&atilde;o s&atilde;o suportados caracteres especiais!";
$mg2->lang['filedeleted']                         = "Arquivo deletado";
$mg2->lang['filenotfound']                        = "Arquivo n&atilde;o localizado!";
$mg2->lang['filesimported']                       = "Arquivo(s) importado(s)";
$mg2->lang['nofilestoimport']                     = "ERRO: Sem arquivos para importar!";
$mg2->lang['foldernotempty']                      = "ERRO: A Pasta n&atilde;o est&aacute; vazia!";
$mg2->lang['folderdeleted']                       = "Pasta deletada";
$mg2->lang['folderupdated']                       = "Pasta atualizada";
$mg2->lang['foldercreated']                       = "Pasta criada";
$mg2->lang['folderexists']                        = "ERRO: Nome de pasta j&aacute; existente!";
$mg2->lang['filesuploaded']                       = "Arquivo(s) enviado(s) - Importando...";
$mg2->lang['settingssaved']                       = "Configura&ccedil;&atilde;o salva";
$mg2->lang['nopwdmatch']                          = "Configura&ccedil;&atilde;o salva<br /><br />ERRO: Senha Incorreta - a nova senha n&atilde;o foi salva!";
$mg2->lang['filesmovedto']                        = "arquivo(s) movido para";
$mg2->lang['filesdeleted']                        = "arquivo(s) deletado!";
$mg2->lang['file']                                = "arquivo";
$mg2->lang['files']                               = "arquivos";
$mg2->lang['folder']                              = "pasta";
$mg2->lang['folders']                             = "pastas";
$mg2->lang['rebuild']                             = "Reconstruir";
$mg2->lang['rebuildimages']                       = "Reconstruir miniaturas";
$mg2->lang['rebuildsuccess']                      = "Reconstru&ccedil;&atilde;o completa";
$mg2->lang['donate']                              = "MG2 &eacute; um software livre, licenciado sob o GPL. Se voc&ecirc; achar este software &uacute;til, por favor fa&ccedil;a uma doa&ccedil;&atilde;o ao autor pressionando o bot&atilde;o abaixo.";
$mg2->lang['from']                                = "De";
$mg2->lang['comment']                             = "Coment&aacute;rio";
$mg2->lang['comments']                            = "Coment&aacute;rios";
$mg2->lang['by']                                  = "por";
$mg2->lang['commentsdeleted']                     = "Coment&aacute;rio(s) deletado";
$mg2->lang['buttonmove']                          = "Mover";
$mg2->lang['buttondelete']                        = "Deletar";
$mg2->lang['deleteconfirm']                       = "Deletar arquivos selecionados ?";
$mg2->lang['imagecolumns']                        = "Imagens por coluna";
$mg2->lang['imagerows']                           = "Linhas por p&aacute;gina";
$mg2->lang['viewfolder']                          = "Ver pasta";
$mg2->lang['viewimage']                           = "Ver imagem";
$mg2->lang['viewgallery']                         = "Ver galeria";
$mg2->lang['rotateright']                         = "Gire 90 graus &agrave; direita";
$mg2->lang['rotateleft']                          = "Gire 90 graus &agrave; esquerda";
$mg2->lang['imagerotated']                        = "Imagem girada!";
$mg2->lang['gifnotrotated']                       = "ERRO: arquivos .GIF n&atilde;o podem ser girados por limita&ccedil;&otilde;es na GD lib!";
$mg2->lang['help']                                = "Ajuda";
/*----------------------------------------------*/
$mg2->lang['slideshowdelay']                      = "Tempo entre os Slides";
$mg2->lang['websitelink']                         = "Link para Website (branco = desabilitado)";
$mg2->lang['marknew']                             = "Marque itens mais novos que X dias(0=desabilitado)";
$mg2->lang['folderempty']                         = "Esta pasta est&aacute; vazia";
$mg2->lang['noimage']                             = "A imagem solicitada n&atilde;o existe!";
/*----------------------------------------------*/
$mg2->lang['actions']                             = "A&ccedil;&otilde;es";
$mg2->lang['backupcomplete']                      = "Backup dos Dados completo";
$mg2->lang['backuplink']                          = "Fazer backup dos dados";
$mg2->lang['viewlogfile']                         = "Ver arquivo de Log";
$mg2->lang['website']                             = "Vai para o Website";
$mg2->lang['backtofolder']                        = "Retornar a Pasta";
$mg2->lang['permerror1']                          = "ERRO DE PERMISS&Atilde;O : N&atilde;o dispon&iacute;vel grava&ccedil;&atilde;o na pasta raiz da galeria!";
$mg2->lang['whattodo1']                           = "Fa&ccedil;a um CHMOD 777 na pasta raiz da galeria";
$mg2->lang['permerror2']                          = "ERRO DE PERMISS&Atilde;O : N&atilde;o dispon&iacute;vel grava&ccedil;&atilde;o na pasta 'pictures' !";
$mg2->lang['whattodo2']                           = "Fa&ccedil;a um CHMOD 777 na pasta 'pictures' da galeria";
$mg2->lang['permerror3']                          = "ERRO DE PERMISS&Atilde;O : N&atilde;o dispon&iacute;vel grava&ccedil;&atilde;o no arquivo 'mg2db_idatabase.php' !";
$mg2->lang['whattodo3']                           = "Fa&ccedil;a um CHMOD 777 no arquivo 'mg2db_idatabase.php'";
$mg2->lang['permerror4']                          = "ERRO DE PERMISS&Atilde;O: N&atilde;o dispon&iacute;vel grava&ccedil;&atilde;o no arquivo 'mg2db_idatabase_temp.php' !";
$mg2->lang['whattodo4']                           = "Fa&ccedil;a um CHMOD 777 no arquivo 'mg2db_idatabase.php' ";
$mg2->lang['permerror5']                          = "ERRO DE PERMISS&Atilde;O: N&atilde;o dispon&iacute;vel grava&ccedil;&atilde;o no arquivo 'mg2db_fdatabase.php' !";
$mg2->lang['whattodo5']                           = "Fa&ccedil;a um CHMOD 777 no arquivo 'mg2db_fdatabase.php'";
$mg2->lang['permerror6']                          = "ERRO DE PERMISS&Atilde;O: N&atilde;o dispon&iacute;vel grava&ccedil;&atilde;o no arquivo 'mg2db_fdatabase_temp.php' !";
$mg2->lang['whattodo6']                           = "Fa&ccedil;a um CHMOD 777 no arquivo 'mg2db_fdatabase_temp.php'";
?>