﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Russian                                     //
//                                                                                     //
//                               TRANSLATED BY: Anonymous                              //
//                               EMAIL: nouveau@pisem.net                              //
//                                                                                     //
//                               LAST UPDATED: 20. Mar 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Галерея";
$mg2->lang['of']                                  = "из";
$mg2->lang['first']                               = "Перв.";
$mg2->lang['prev']                                = "Предыд.";
$mg2->lang['next']                                = "След.";
$mg2->lang['last']                                = "Послед.";
$mg2->lang['thumbs']                              = "Иконки";
$mg2->lang['exif info']                           = "Информация Exif";
$mg2->lang['model']                               = "Модель";
$mg2->lang['shutter']                             = "выдержка";
$mg2->lang['viewslideshow']                       = "Начать показ слайдов";
$mg2->lang['stopslideshow']                       = "Остановить показ слайдов";
$mg2->lang['aperture']                            = "Апертура";
$mg2->lang['flash']                               = "Вспышка";
$mg2->lang['focallength']                         = "Фок. расстояние";
$mg2->lang['mm']                                  = "мм";
$mg2->lang['exposurecomp']                        = "Компенсация выдержки";
$mg2->lang['original']                            = "Оригинал";
$mg2->lang['metering']                            = "Замер";
$mg2->lang['iso']                                 = "Светочувствительность (ISO)";
$mg2->lang['seconds']                             = "сек.";
$mg2->lang['page']                                = "Страница";
$mg2->lang['all']                                 = "Все";
$mg2->lang['fullsize']                            = "Просмотреть изображение в полный размер";
$mg2->lang['addcomment']                          = "Доб. комментарий";
$mg2->lang['name']                                = "Имя";
$mg2->lang['email']                               = "Эл. почта";
$mg2->lang['commentadded']                        = "Комментарий добавлен";
$mg2->lang['commentexists']                       = "ОШИБКА: Комментарий уже существует!";
$mg2->lang['commentmissing']                      = "ОШИБКА: Все поля комментариев должны быть защищены!";
$mg2->lang['enterpassword']                       = "Введите пароль";
$mg2->lang['thissection']                         = "Этот раздел защищен паролем";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Корн. каталог";
$mg2->lang['thumb']                               = "Иконка";
$mg2->lang['dateadded']                           = "Дата добавления";
$mg2->lang['upload']                              = "Загрузить файлы";
$mg2->lang['import']                              = "Импорт загруженных файлов в";
$mg2->lang['newfolder']                           = "Новая папка";
$mg2->lang['viewgallery']                         = "Просмотр галереи";
$mg2->lang['setup']                               = "Установка";
$mg2->lang['logoff']                              = "Выйти";
$mg2->lang['menutxt_upload']                      = "Загрузка";
$mg2->lang['menutxt_import']                      = "Импорт";
$mg2->lang['menutxt_newfolder']                   = "Новая папка";
$mg2->lang['menutxt_viewgallery']                 = "Просмотр галереи";
$mg2->lang['menutxt_setup']                       = "Установка";
$mg2->lang['menutxt_logoff']                      = "Выйти";
$mg2->lang['delete']                              = "Удалить";
$mg2->lang['cancel']                              = "Отмена";
$mg2->lang['ok']                                  = "Ладно";
$mg2->lang['deletefolder']                        = "Удалить папку";
$mg2->lang['navigation']                          = "Навигация";
$mg2->lang['images']                              = "изображение(я)";
$mg2->lang['filename']                            = "Имя файла";
$mg2->lang['title']                               = "Название";
$mg2->lang['description']                         = "Описание";
$mg2->lang['setasthumb']                          = "Выбрать в качестве иконки для папок";
$mg2->lang['editfolder']                          = "Редактировать папку";
$mg2->lang['editimage']                           = "Редактировать изображение";
$mg2->lang['nofolderselected']                    = "Папка не выбрана";
$mg2->lang['foldername']                          = "Имя папки";
$mg2->lang['newpassword']                         = "Новый пароль";
$mg2->lang['deletepassword']                      = "Удалить пароль";
$mg2->lang['introtext']                           = "Вводный текст";
$mg2->lang['deletethumb']                         = "Удалить иконку";
$mg2->lang['moveto']                              = "Переместить в";
$mg2->lang['id']                                  = "Идентификатор";
$mg2->lang['filesize']                            = "Размер файла";
$mg2->lang['width']                               = "Ширина";
$mg2->lang['height']                              = "Высота";
$mg2->lang['date']                                = "Дата";
$mg2->lang['ascending']                           = "По возрастанию";
$mg2->lang['descending']                          = "По убыванию";
$mg2->lang['newfolder']                           = "Новая папка";
$mg2->lang['password']                            = "Пароль";
$mg2->lang['direction']                           = "Направление";
$mg2->lang['sortby']                              = "Сортировать по";
$mg2->lang['gallerytitle']                        = "Название галереи";
$mg2->lang['adminemail']                          = "Эл. почта админа";
$mg2->lang['language']                            = "Язык";
$mg2->lang['skin']                                = "Шкурка";
$mg2->lang['dateformat']                          = "Формат даты";
$mg2->lang['DDMMYY']                              = "ДД МММ ГГГГ";
$mg2->lang['MMDDYY']                              = "МММ ДД, ГГГГ";
$mg2->lang['MM.DD.YY']                            = "ММ.ММ.ГГ";
$mg2->lang['DD.MM.YY']                            = "ДД.ММ.ГГ";
$mg2->lang['YYYYMMDD']                            = "ГГГГММДД";
$mg2->lang['sendmail']                            = "Послать комментарий по эл. почте";
$mg2->lang['foldericons']                         = "Принудительный выбор иконок для папок";
$mg2->lang['showexif']                            = "Показать Exif";
$mg2->lang['allowcomments']                       = "Разрешить комментировать";
$mg2->lang['copyright']                           = "Замечание об авторском праве";
$mg2->lang['passwordchange']                      = "Изменить пароль (3 x пропуск = сохранить текущий)";
$mg2->lang['oldpasswordsetup']                    = "Ввести текущий пароль";
$mg2->lang['newpasswordsetup']                    = "Новый пароль (пропуск = использовать текущий)";
$mg2->lang['newpasswordsetupconfirm']             = "Введите текущий пароль";
$mg2->lang['advanced']                            = "Для продвинутых";
$mg2->lang['allowedextensions']                   = "Разрешенные расширения";
$mg2->lang['imgwidth']                            = "Макс. ширина изображения (0 = отключить)";
$mg2->lang['indexfile']                           = "Файл списка галерей";
$mg2->lang['thumbquality']                        = "Качество иконок";
$mg2->lang['uploadimport']                        = "Не забудьте импортировать изображения после загрузки!";
$mg2->lang['image']                               = "Изображение";
$mg2->lang['edit']                                = "Редактировать";
$mg2->lang['editcurrentfolder']                   = "Редактировать тек. папку";
$mg2->lang['deletecurrentfolder']                 = "Удалить тек. папку";
$mg2->lang['by']                                  = "по";
$mg2->lang['loginagain']                          = "Ввойти снова";
$mg2->lang['securitylogoff']                      = "ВЫход из системы";
$mg2->lang['autologoff']                          = "Вы автоматически вышли из системы по истечении 15 мин. бездействия.";
$mg2->lang['logoff']                              = "Выйти";
$mg2->lang['forsecurity']                         = "Из соображений безопасности, рекомендуется закрыть окно браузера.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Этой инсталяции X дней. Нажмите сюда для проверки на наличие новой версии!</a></b>";
$mg2->lang['updatesuccess']                       = "Загрузка успешна";
$mg2->lang['renamefailure']                       = "ОШИБКА: Имя файла содержит недопускаемые символы!";
$mg2->lang['filedeleted']                         = "Файл удален";
$mg2->lang['filenotfound']                        = "Файл не найден!";
$mg2->lang['filesimported']                       = "файл(ы) импортирован(ы)";
$mg2->lang['nofilestoimport']                     = "ОШИБКА: нечего импортировать!";
$mg2->lang['foldernotempty']                      = "ОШИБКА: Папка не пуста!";
$mg2->lang['folderdeleted']                       = "Папка удалена";
$mg2->lang['folderupdated']                       = "Папка актуализирована";
$mg2->lang['foldercreated']                       = "Папка создана";
$mg2->lang['folderexists']                        = "ОШИБКА: Имя файла существует!";
$mg2->lang['filesuploaded']                       = "файл(ы) загружены";
$mg2->lang['settingssaved']                       = "Установки сохранены";
$mg2->lang['nopwdmatch']                          = "Установки сохранены<br /><br />ОШИБКА: Пароль не соответвтвует - новый пароль не сохранен!";
$mg2->lang['filesmovedto']                        = "файл(ы) перемещены";
$mg2->lang['filesdeleted']                        = "файл(ы) удалены!";
$mg2->lang['file']                                = "файл";
$mg2->lang['files']                               = "файл(ов)";
$mg2->lang['folder']                              = "папка";
$mg2->lang['folders']                             = "папки";
$mg2->lang['rebuild']                             = "Реконструировать";
$mg2->lang['rebuildimages']                       = "Реконструировать иконки";
$mg2->lang['rebuildsuccess']                      = "Реконструкция завершена";
$mg2->lang['donate']                              = "MG2 является бесплатным программным обеспечением, лицензированным по GPL. Если Вы находите это программное обеспечение полезным, сделайте, пожалуйста, пожертвование автору, нажав на кнопку ниже.";
$mg2->lang['from']                                = "От";
$mg2->lang['comment']                             = "Комментарий";
$mg2->lang['comments']                            = "Комментарии";
$mg2->lang['by']                                  = "по";
$mg2->lang['commentsdeleted']                     = "Комментарий(и) удален(ы)";
$mg2->lang['buttonmove']                          = "Переместить";
$mg2->lang['buttondelete']                        = "Удалить";
$mg2->lang['deleteconfirm']                       = "Удалить выбранные файлы?";
$mg2->lang['imagecolumns']                        = "Столбцы изображений";
$mg2->lang['imagerows']                           = "Ряды изображений";
$mg2->lang['viewfolder']                          = "Просмотр папки";
$mg2->lang['viewimage']                           = "Просмотр изображения";
$mg2->lang['viewgallery']                         = "Просмотр галереи";
$mg2->lang['rotateright']                         = "Повернуть на 90 градусов вправо";
$mg2->lang['rotateleft']                          = "Повернуть на 90 градусов влево";
$mg2->lang['imagerotated']                        = "Изображение повернуто!";
$mg2->lang['gifnotrotated']                       = "ОШИБКА: .GIF файлы не могут быть повернуты из-за ограничений в библиотеке GD lib!";
$mg2->lang['help']                                = "Помощь";
$mg2->lang['slideshowdelay']                      = "Задержка в показе слайдов";
$mg2->lang['websitelink']                         = "Ссылка на страничку (пропуск = отключить)";
$mg2->lang['marknew']                             = "Отметить пункты за последние X дней (0 = отключить)";
$mg2->lang['folderempty']                         = "Эта папка пуста";
$mg2->lang['noimage']                             = "Запрошенное изображение не существует!";
?>
