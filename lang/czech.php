<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Czech                                       //
//                                                                                     //
//                               TRANSLATED BY: Josef Klimosz                          //
//                               EMAIL: ok2wo@centrum.cz                               //
//                               Charset: UTF-8                                        //
//                               LAST UPDATED: 10. 3. 2005                             //
//                                                                                     //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galerie";
$mg2->lang['of']                                  = "z";
$mg2->lang['first']                               = "První";
$mg2->lang['prev']                                = "Předchozí";
$mg2->lang['next']                                = "Další";
$mg2->lang['last']                                = "Poslední";
$mg2->lang['thumbs']                              = "Náhledy";
$mg2->lang['exif info']                           = "Informace EXIF";
$mg2->lang['model']                               = "Model";
$mg2->lang['shutter']                             = "Rychlost závěrky";
$mg2->lang['viewslideshow']                       = "Spustit promítání";
$mg2->lang['stopslideshow']                       = "Zastavit promítání";
$mg2->lang['aperture']                            = "Clona";
$mg2->lang['flash']                               = "Blesk";
$mg2->lang['focallength']                         = "Ohnisková vzdálenost";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Kompenzace expozice";
$mg2->lang['original']                            = "Originál";
$mg2->lang['metering']                            = "Vzdálenost";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Stránka";
$mg2->lang['all']                                 = "Vše";
$mg2->lang['fullsize']                            = "Zobrazit plnou velikost";
$mg2->lang['addcomment']                          = "Přidat komentář";
$mg2->lang['name']                                = "Jméno";
$mg2->lang['email']                               = "E-mail";
$mg2->lang['commentadded']                        = "Komentář přidán";
$mg2->lang['commentexists']                       = "Chyba: Komentář už existuje!";
$mg2->lang['commentmissing']                      = "Chyba: Všechna pole musí být vyplněna!";
$mg2->lang['enterpassword']                       = "Vložte heslo";
$mg2->lang['thissection']                         = "Tato sekce je chráněna heslem";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Hlavní adresář";
$mg2->lang['thumb']                               = "Náhled";
$mg2->lang['dateadded']                           = "Přidáno datum";
$mg2->lang['upload']                              = "Nahrávat soubory";
$mg2->lang['import']                              = "Importovat nahrané soubory do";
$mg2->lang['newfolder']                           = "Nový adresář";
$mg2->lang['viewgallery']                         = "Ukázat galerii";
$mg2->lang['setup']                               = "Nastavení";
$mg2->lang['logoff']                              = "Odhlášení";
$mg2->lang['menutxt_upload']                      = "Nahrávání";
$mg2->lang['menutxt_import']                      = "Import";
$mg2->lang['menutxt_newfolder']                   = "Nový adresář";
$mg2->lang['menutxt_viewgallery']                 = "Ukázat galerii";
$mg2->lang['menutxt_setup']                       = "Nastavení";
$mg2->lang['menutxt_logoff']                      = "Odhlášení";
$mg2->lang['delete']                              = "Vymazat";
$mg2->lang['cancel']                              = "Zrušit";
$mg2->lang['ok']                                  = "OK";
$mg2->lang['deletefolder']                        = "Vymazat adresář";
$mg2->lang['navigation']                          = "Navigace";
$mg2->lang['images']                              = "obrázků";
$mg2->lang['filename']                            = "Název souboru";
$mg2->lang['title']                               = "Titulek";
$mg2->lang['description']                         = "Popis";
$mg2->lang['setasthumb']                          = "Nastavit jako náhled adresáře";
$mg2->lang['editfolder']                          = "Editovat adresář";
$mg2->lang['editimage']                           = "Editovat obrázek";
$mg2->lang['nofolderselected']                    = "Žádný adresář nevybrán";
$mg2->lang['foldername']                          = "Název adresáře";
$mg2->lang['newpassword']                         = "Nové heslo";
$mg2->lang['deletepassword']                      = "Vymazat heslo";
$mg2->lang['introtext']                           = "Úvodní text";
$mg2->lang['deletethumb']                         = "Vymazat náhled";
$mg2->lang['moveto']                              = "Přemístit do";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Velikost souboru";
$mg2->lang['width']                               = "Šířka";
$mg2->lang['height']                              = "Výška";
$mg2->lang['date']                                = "Datum";
$mg2->lang['ascending']                           = "Vzestupně";
$mg2->lang['descending']                          = "Sestupně";
$mg2->lang['newfolder']                           = "Nový adresář";
$mg2->lang['password']                            = "Heslo";
$mg2->lang['direction']                           = "Směr";
$mg2->lang['sortby']                              = "Seřadit podle";
$mg2->lang['gallerytitle']                        = "Název galerie";
$mg2->lang['adminemail']                          = "E-mail admina";
$mg2->lang['language']                            = "Jazyk";
$mg2->lang['skin']                                = "Vzhled";
$mg2->lang['dateformat']                          = "Formát data";
$mg2->lang['DDMMYY']                              = "DD MMM RRRR";
$mg2->lang['MMDDYY']                              = "MMM DD, RRRR";
$mg2->lang['MM.DD.YY']                            = "MM.DD.RR";
$mg2->lang['DD.MM.YY']                            = "DD.MM.RR";
$mg2->lang['YYYYMMDD']                            = "RRRRMMDD";
$mg2->lang['sendmail']                            = "Posílat komentáře e-mailem";
$mg2->lang['foldericons']                         = "Vnutit adresářové ikony";
$mg2->lang['showexif']                            = "Zobrazit EXIF";
$mg2->lang['allowcomments']                       = "Umožnit komentáře";
$mg2->lang['copyright']                           = "Copyrightová poznámka";
$mg2->lang['passwordchange']                      = "Změna hesla (3 x prázdné = ponechat stávající)";
$mg2->lang['oldpasswordsetup']                    = "Vložte stávající heslo";
$mg2->lang['newpasswordsetup']                    = "Nové heslo (prázdné = stávající)";
$mg2->lang['newpasswordsetupconfirm']             = "Vložte nové heslo znovu";
$mg2->lang['advanced']                            = "Rozšířené";
$mg2->lang['allowedextensions']                   = "Povolené přípony";
$mg2->lang['imgwidth']                            = "Max. šířka obrázku (0 = vypnuto)";
$mg2->lang['indexfile']                           = "Indexový soubor galerie";
$mg2->lang['thumbquality']                        = "Kvalita náhledu";
$mg2->lang['uploadimport']                        = "Nezapomeňte vaše obrázky po nahrání importovat!";
$mg2->lang['image']                               = "Obrázek";
$mg2->lang['edit']                                = "Editovat";
$mg2->lang['editcurrentfolder']                   = "Editovat stávající adresář";
$mg2->lang['deletecurrentfolder']                 = "Vymazat stávající adresář";
$mg2->lang['by']                                  = "od";
$mg2->lang['loginagain']                          = "Přihlásit znovu";
$mg2->lang['securitylogoff']                      = "Bezpečnostní odhlášení";
$mg2->lang['autologoff']                          = "Byl jste automaticky odhlášen po 15 minutách neaktivity.";
$mg2->lang['logoff']                              = "Odhlášení";
$mg2->lang['forsecurity']                         = "Z bezpečnostních důvodů je doporučeno zavřít toto okno prohlížeče.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Tato instalace je stará X dní. Klikněte a hledejte obnovení!</a></b>";
$mg2->lang['updatesuccess']                       = "Obnovení úspěšné";
$mg2->lang['renamefailure']                       = "Chyba: Název souboru obsahuje nepovolené znaky!";
$mg2->lang['filedeleted']                         = "Soubor vymazán";
$mg2->lang['filenotfound']                        = "Soubor nebyl nalezen!";
$mg2->lang['filesimported']                       = "soubor(y) importován(y)";
$mg2->lang['nofilestoimport']                     = "Chyba: Žádné soubory k importu!";
$mg2->lang['foldernotempty']                      = "Chyba: Adresář není prázdný!";
$mg2->lang['folderdeleted']                       = "Adresář vymazán";
$mg2->lang['folderupdated']                       = "Adresář obnoven";
$mg2->lang['foldercreated']                       = "Adresář vytvořen";
$mg2->lang['folderexists']                        = "Chyba: Název souboru už existuje!";
$mg2->lang['filesuploaded']                       = "Soubor(y) nahrán(y)";
$mg2->lang['settingssaved']                       = "Nastavení uloženo";
$mg2->lang['nopwdmatch']                          = "Nastavení uloženo<br /><br />Chyba: Heslo nesouhlasí - nové heslo nebylo uloženo!";
$mg2->lang['filesmovedto']                        = "soubor(y) přemístěny do";
$mg2->lang['filesdeleted']                        = "soubor(y) vymazán(y)!";
$mg2->lang['file']                                = "soubor";
$mg2->lang['files']                               = "soubory";
$mg2->lang['folder']                              = "adresář";
$mg2->lang['folders']                             = "adresáře";
$mg2->lang['rebuild']                             = "Opravit";
$mg2->lang['rebuildimages']                       = "Opravit náhledy";
$mg2->lang['rebuildsuccess']                      = "Opravy hotovy";
$mg2->lang['donate']                              = "MG2 je svobodný software pod licencí GPL. Jestliže shledáte tento software užitečným, můžete podpořit autora po kliknutí na tlačítko níže.";
$mg2->lang['from']                                = "Od";
$mg2->lang['comment']                             = "Komentář";
$mg2->lang['comments']                            = "Komentáře";
$mg2->lang['by']                                  = "s";
$mg2->lang['commentsdeleted']                     = "Komentář(e) vymazán(y)";
$mg2->lang['buttonmove']                          = "Přemístit";
$mg2->lang['buttondelete']                        = "Vymazat";
$mg2->lang['deleteconfirm']                       = "Vymazat vybrané soubory?";
$mg2->lang['imagecolumns']                        = "Sloupce obrázků";
$mg2->lang['imagerows']                           = "Řádky obrázků ";
$mg2->lang['viewfolder']                          = "Ukázat adresář";
$mg2->lang['viewimage']                           = "Ukázat obrázek";
$mg2->lang['viewgallery']                         = "Ukázat galerii";
$mg2->lang['rotateright']                         = "Otočit o 90 stupňů doprava";
$mg2->lang['rotateleft']                          = "Otočit o 90 stupňů doleva";
$mg2->lang['imagerotated']                        = "Obrázek otočen!";
$mg2->lang['gifnotrotated']                       = "Chyba: .GIF soubory nelze otáčet kvůli omezením v GD knihovně!";
$mg2->lang['help']                                = "Pomoc";
?>
