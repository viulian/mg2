<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Latviski                                    //
//                                                                                     //
//                               TRANSLATED BY: Valters Mitāns                         //
//                               EMAIL: mitans@inbox.lv                                //
//                                                                                     //
//                               LAST UPDATED: 16. Mar 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galerija";
$mg2->lang['of']                                  = "no";
$mg2->lang['first']                               = "Pirmā";
$mg2->lang['prev']                                = "Iepriekšējā";
$mg2->lang['next']                                = "Nākamā";
$mg2->lang['last']                                = "Pēdējā";
$mg2->lang['thumbs']                              = "Minibildes";
$mg2->lang['exif info']                           = "Exif Informācija";
$mg2->lang['model']                               = "Modelis";
$mg2->lang['shutter']                             = "Slēdža ātrums";
$mg2->lang['viewslideshow']                       = "Skatīt slaidšovu";
$mg2->lang['stopslideshow']                       = "Apturēt slaidšovu";
$mg2->lang['aperture']                            = "Diafragma";
$mg2->lang['flash']                               = "Zibspuldze";
$mg2->lang['focallength']                         = "Fokuss";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Ekspozīcijas kompensācija";
$mg2->lang['original']                            = "Oriģināls";
$mg2->lang['metering']                            = "Mērīšana";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Lapa";
$mg2->lang['all']                                 = "Visas";
$mg2->lang['fullsize']                            = "Skatīt bildi pilnā izmērā";
$mg2->lang['addcomment']                          = "Pievienot komentāru";
$mg2->lang['name']                                = "Vārds";
$mg2->lang['email']                               = "E-mail";
$mg2->lang['commentadded']                        = "Komentārs pievienots";
$mg2->lang['commentexists']                       = "KĻŪDA: Komentārs jau ir ievietots!";
$mg2->lang['commentmissing']                      = "KĻŪDA: Visām komentāra ailēm jābūt aizpildītām!";
$mg2->lang['enterpassword']                       = "Ievadi paroli";
$mg2->lang['thissection']                         = "Šī sadaļa ir aizsargāta ar paroli";

// ADMIN LANGUAGE STRINGS

$mg2->lang['root']                                = "Root";
$mg2->lang['thumb']                               = "Minibildes";
$mg2->lang['dateadded']                           = "Datums pievienots";
$mg2->lang['upload']                              = "Ielādēt failus";
$mg2->lang['import']                              = "Importēt ielādētos failus uz";
$mg2->lang['newfolder']                           = "Jauns folderis";
$mg2->lang['viewgallery']                         = "Skatīt galeriju";
$mg2->lang['setup']                               = "Iestādījumi";
$mg2->lang['logoff']                              = "Izlogoties";
$mg2->lang['menutxt_upload']                      = "Ielādēt";
$mg2->lang['menutxt_import']                      = "Importēt";
$mg2->lang['menutxt_newfolder']                   = "Jauns folderis";
$mg2->lang['menutxt_viewgallery']                 = "Skatīt galeriju";
$mg2->lang['menutxt_setup']                       = "Iestādījumi";
$mg2->lang['menutxt_logoff']                      = "Izlogoties";
$mg2->lang['delete']                              = "Dzēst";
$mg2->lang['cancel']                              = "Atcelt";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Dzēst folderi";
$mg2->lang['navigation']                          = "Navigācija";
$mg2->lang['images']                              = "bilde(s)";
$mg2->lang['filename']                            = "Faila nosaukums";
$mg2->lang['title']                               = "Virsraksts";
$mg2->lang['description']                         = "Apraksts";
$mg2->lang['setasthumb']                          = "Ievietot kā minibildi uz foldera";
$mg2->lang['editfolder']                          = "Rediģēt folderi";
$mg2->lang['editimage']                           = "Rediģēt bildi";
$mg2->lang['nofolderselected']                    = "Nav norādīts";
$mg2->lang['foldername']                          = "Foldera nosaukums";
$mg2->lang['newpassword']                         = "Jauna parole";
$mg2->lang['deletepassword']                      = "Dzēst paroli";
$mg2->lang['introtext']                           = "Ievadteksts";
$mg2->lang['deletethumb']                         = "Dzēst minibildi";
$mg2->lang['moveto']                              = "Pārvietot uz";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Faila izmērs";
$mg2->lang['width']                               = "Platums";
$mg2->lang['height']                              = "Augstums";
$mg2->lang['date']                                = "Datums";
$mg2->lang['ascending']                           = "Augošs";
$mg2->lang['descending']                          = "Dilstošs";
$mg2->lang['newfolder']                           = "Jauns folderis";
$mg2->lang['password']                            = "Parole";
$mg2->lang['direction']                           = "Virziens";
$mg2->lang['sortby']                              = "Šķirot pēc";
$mg2->lang['gallerytitle']                        = "Galerijas virsraksts";
$mg2->lang['adminemail']                          = "Administratora e-mail";
$mg2->lang['language']                            = "Valoda";
$mg2->lang['skin']                                = "Ādiņa";
$mg2->lang['dateformat']                          = "Datuma formāts";
$mg2->lang['DDMMYY']                              = "DD MM GGGG";
$mg2->lang['MMDDYY']                              = "MM DD GGGG";
$mg2->lang['MM.DD.YY']                            = "MM.DD.GG";
$mg2->lang['DD.MM.YY']                            = "DD.MM.GG";
$mg2->lang['YYYYMMDD']                            = "GGGGMMDD";
$mg2->lang['sendmail']                            = "Sūtīt komentāru e-mailus";
$mg2->lang['foldericons']                         = "Forsētas foldera ikonas";
$mg2->lang['showexif']                            = "Rādīt Exif";
$mg2->lang['allowcomments']                       = "Atļaut komentēt";
$mg2->lang['copyright']                           = "Copyright paziņojums";
$mg2->lang['passwordchange']                      = "Mainīt paroli (3 x tukšs = paturēt esošo)";
$mg2->lang['oldpasswordsetup']                    = "Ievadi esošo paroli";
$mg2->lang['newpasswordsetup']                    = "Jaunā parole (tikšs = paturēt esošo)";
$mg2->lang['newpasswordsetupconfirm']             = "Ievadi jauno paroli atkārtoti";
$mg2->lang['advanced']                            = "Advancētā daļa";
$mg2->lang['allowedextensions']                   = "Atļautie paplašinājumi";
$mg2->lang['imgwidth']                            = "Max. bildes platums (0 = neierobežots)";
$mg2->lang['indexfile']                           = "Galerijas index fails";
$mg2->lang['thumbquality']                        = "Minibilžu kvalitāte";
$mg2->lang['uploadimport']                        = "Neaizmirsti failus importēt pēc ielādes veikšanas!";
$mg2->lang['image']                               = "Bilde";
$mg2->lang['edit']                                = "Rediģēt";
$mg2->lang['editcurrentfolder']                   = "Rediģēt šo folderi";
$mg2->lang['deletecurrentfolder']                 = "Dzēst šo folderi";
$mg2->lang['by']                                  = "no";
$mg2->lang['loginagain']                          = "Ielogojies no jauna";
$mg2->lang['securitylogoff']                      = "Automātiskā drošības izlogošanās";
$mg2->lang['autologoff']                          = "Tu automātiski tiki izlogots, jo 15 minūšu laikā neko šeit netiki darījis";
$mg2->lang['logoff']                              = "Izlogoties";
$mg2->lang['forsecurity']                         = "Drošības nolūkos ieteicams aizvērt šo interneta pārlūka logu.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Šī instalācija X dienas veca. Spied šeit, lai pārbaudītu, vai nav pieejama jaunāka versija!</a></b>";
$mg2->lang['updatesuccess']                       = "Updeitots veiksmīgi";
$mg2->lang['renamefailure']                       = "KĻŪDA: Faila nosaukums satur neatļautus simbolus!";
$mg2->lang['filedeleted']                         = "Fails dzēsts";
$mg2->lang['filenotfound']                        = "Fails netika atrasts!";
$mg2->lang['filesimported']                       = "fails(i) importēti";
$mg2->lang['nofilestoimport']                     = "KĻŪDA: Nav failu, ko importēt!";
$mg2->lang['foldernotempty']                      = "KĻŪDA: Folderis nav tukšs!";
$mg2->lang['folderdeleted']                       = "Folderis dzēsts";
$mg2->lang['folderupdated']                       = "Folderis updeitots";
$mg2->lang['foldercreated']                       = "Folderis izveidots";
$mg2->lang['folderexists']                        = "KĻŪDA: Šāds foldera nosaukums jau eksistē!";
$mg2->lang['filesuploaded']                       = "Fails(i) ielādēti";
$mg2->lang['settingssaved']                       = "Iestādījumi saglabāti";
$mg2->lang['nopwdmatch']                          = "Iestādījumi saglabāti<br /><br />KĻŪDA: Paroļu nesakritība - jaunā parole nav saglabāta!";
$mg2->lang['filesmovedto']                        = "fails(i) pārvietoti uz";
$mg2->lang['filesdeleted']                        = "fails(i) dzēsti!";
$mg2->lang['file']                                = "fails";
$mg2->lang['files']                               = "faili";
$mg2->lang['folder']                              = "folderis";
$mg2->lang['folders']                             = "folderi";
$mg2->lang['rebuild']                             = "Pārkārtot";
$mg2->lang['rebuildimages']                       = "Pārkārtot minibildes";
$mg2->lang['rebuildsuccess']                      = "Pārkārtošana pabeigta";
$mg2->lang['donate']                              = "MG2 ir bezmaksas programma, licenzēta ar GPL. Ja tev šī programma liekas noderīga un laba, aicinām ziedot programmas autoram, nospiežot uz apakšā redzamās saites.";
$mg2->lang['from']                                = "No";
$mg2->lang['comment']                             = "Komentārs";
$mg2->lang['comments']                            = "Komentāri";
$mg2->lang['by']                                  = "no";
$mg2->lang['commentsdeleted']                     = "Komentārs(i) dzēsts(i)";
$mg2->lang['buttonmove']                          = "Pārvietot";
$mg2->lang['buttondelete']                        = "Dzēst";
$mg2->lang['deleteconfirm']                       = "Dzēst atzīmētos failus?";
$mg2->lang['imagecolumns']                        = "Bilžu kolonnas";
$mg2->lang['imagerows']                           = "Bilžu rindas";
$mg2->lang['viewfolder']                          = "Skatīt folderi";
$mg2->lang['viewimage']                           = "Skatīt bildi";
$mg2->lang['viewgallery']                         = "Skatīt galeriju";
$mg2->lang['rotateright']                         = "Pagriezt 90 grādos pa labi";
$mg2->lang['rotateleft']                          = "Pagriezt 90 grādos pa kreisi";
$mg2->lang['imagerotated']                        = "Bilžu pagriešana!";
$mg2->lang['gifnotrotated']                       = "KĻŪDA: .GIF failus nav iesējams pagriezt GD lib limitu dēļ!";
$mg2->lang['help']                                = "Palīgā";
$mg2->lang['slideshowdelay']                      = "Slaidšova aizture";
$mg2->lang['websitelink']                         = "Mājaslapa (tukšs = neaktīvs)";
$mg2->lang['marknew']                             = "Atzīmē to, kas jaunāks par X dienām (0 = neaktīvs)";
$mg2->lang['folderempty']                         = "Šis folderis ir tukšs";
$mg2->lang['noimage']                             = "Pieprasītais attēls neeksistē!";
?>
