<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Chinese                                     //
//                                                                                     //
//                               TRANSLATED BY: Elton Kong                             //
//                               EMAIL: freya@netvigator.com                           //
//                                                                                     //
//                               LAST UPDATED: 17. Mar 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "藝廊";
$mg2->lang['of']                                  = "/";
$mg2->lang['first']                               = "首頁";
$mg2->lang['prev']                                = "上頁";
$mg2->lang['next']                                = "下頁";
$mg2->lang['last']                                = "末頁";
$mg2->lang['thumbs']                              = "縮圖";
$mg2->lang['exif info']                           = "Exif 資料";
$mg2->lang['model']                               = "型號";
$mg2->lang['shutter']                             = "快門";
$mg2->lang['viewslideshow']                       = "觀看幻燈片";
$mg2->lang['stopslideshow']                       = "停止幻燈片";
$mg2->lang['aperture']                            = "光圈";
$mg2->lang['flash']                               = "閃光燈";
$mg2->lang['focallength']                         = "焦距";
$mg2->lang['mm']                                  = "毫米";
$mg2->lang['exposurecomp']                        = "曝光";
$mg2->lang['original']                            = "原圖";
$mg2->lang['metering']                            = "測光";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "秒";
$mg2->lang['page']                                = "頁";
$mg2->lang['all']                                 = "所有";
$mg2->lang['fullsize']                            = "顯示原圖";
$mg2->lang['addcomment']                          = "提供意見";
$mg2->lang['name']                                = "你的名字";
$mg2->lang['email']                               = "你的電郵";
$mg2->lang['commentadded']                        = "意見已加入";
$mg2->lang['commentexists']                       = "錯誤: 意見已存在!";
$mg2->lang['commentmissing']                      = "錯誤: 必須填寫所有意見欄!";
$mg2->lang['enterpassword']                       = "輸入密碼";
$mg2->lang['thissection']                         = "此部份受密碼保護";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "根目錄";
$mg2->lang['thumb']                               = "縮圖";
$mg2->lang['dateadded']                           = "加入日期";
$mg2->lang['upload']                              = "上傳圖檔";
$mg2->lang['import']                              = "匯入上傳圖檔至";
$mg2->lang['newfolder']                           = "建立資料夾";
$mg2->lang['viewgallery']                         = "觀看藝廊";
$mg2->lang['setup']                               = "設定";
$mg2->lang['logoff']                              = "登出";
$mg2->lang['menutxt_upload']                      = "上傳圖檔";
$mg2->lang['menutxt_import']                      = "匯入圖檔";
$mg2->lang['menutxt_newfolder']                   = "建立資料夾";
$mg2->lang['menutxt_viewgallery']                 = "觀看藝廊";
$mg2->lang['menutxt_setup']                       = "設定";
$mg2->lang['menutxt_logoff']                      = "登出";
$mg2->lang['delete']                              = "刪除";
$mg2->lang['cancel']                              = "取消";
$mg2->lang['ok']                                  = "確定";
$mg2->lang['deletefolder']                        = "刪除資料夾";
$mg2->lang['navigation']                          = "導航";
$mg2->lang['images']                              = "圖像";
$mg2->lang['filename']                            = "圖檔名稱";
$mg2->lang['title']                               = "標題";
$mg2->lang['description']                         = "介紹";
$mg2->lang['setasthumb']                          = "設為資料夾縮圖";
$mg2->lang['editfolder']                          = "編輯資料夾";
$mg2->lang['editimage']                           = "編輯圖像";
$mg2->lang['nofolderselected']                    = "沒有選擇資料夾";
$mg2->lang['foldername']                          = "資料夾名稱";
$mg2->lang['newpassword']                         = "新密碼";
$mg2->lang['deletepassword']                      = "刪除密碼";
$mg2->lang['introtext']                           = "簡介";
$mg2->lang['deletethumb']                         = "刪除縮圖";
$mg2->lang['moveto']                              = "移至";
$mg2->lang['id']                                  = "編號";
$mg2->lang['filesize']                            = "圖檔大小";
$mg2->lang['width']                               = "圖寬";
$mg2->lang['height']                              = "圖高";
$mg2->lang['date']                                = "日期";
$mg2->lang['ascending']                           = "遞加排序";
$mg2->lang['descending']                          = "遞減排序";
$mg2->lang['newfolder']                           = "新資料夾";
$mg2->lang['password']                            = "密碼";
$mg2->lang['direction']                           = "方向";
$mg2->lang['sortby']                              = "排序法";
$mg2->lang['gallerytitle']                        = "藝廊名稱";
$mg2->lang['adminemail']                          = "管理員電郵";
$mg2->lang['language']                            = "語言";
$mg2->lang['skin']                                = "面板";
$mg2->lang['dateformat']                          = "日期格式";
$mg2->lang['DDMMYY']                              = "DD MMM YYYY";
$mg2->lang['MMDDYY']                              = "MMM DD, YYYY";
$mg2->lang['MM.DD.YY']                            = "MM.DD.YY";
$mg2->lang['DD.MM.YY']                            = "DD.MM.YY";
$mg2->lang['YYYYMMDD']                            = "YYYYMMDD";
$mg2->lang['sendmail']                            = "發送電郵";
$mg2->lang['foldericons']                         = "強迫資料夾圖示";
$mg2->lang['showexif']                            = "顯示 Exif";
$mg2->lang['allowcomments']                       = "准許意見";
$mg2->lang['copyright']                           = "版權聲明";
$mg2->lang['passwordchange']                      = "更改密碼 (3 x 空白 = 保留現有密碼)";
$mg2->lang['oldpasswordsetup']                    = "輸入舊密碼";
$mg2->lang['newpasswordsetup']                    = "輸入新密碼 (空白 = 使用現有密碼)";
$mg2->lang['newpasswordsetupconfirm']             = "再輸入新密碼";
$mg2->lang['advanced']                            = "進階";
$mg2->lang['allowedextensions']                   = "容許的擴充";
$mg2->lang['imgwidth']                            = "最大圖寬 (0 = 不使用)";
$mg2->lang['indexfile']                           = "藝廊索引檔";
$mg2->lang['thumbquality']                        = "縮圖素質";
$mg2->lang['uploadimport']                        = "上傳後記得匯入圖像!";
$mg2->lang['image']                               = "圖像";
$mg2->lang['edit']                                = "編輯";
$mg2->lang['editcurrentfolder']                   = "編輯當前資料夾";
$mg2->lang['deletecurrentfolder']                 = "刪除當前資料夾";
$mg2->lang['by']                                  = "由";
$mg2->lang['loginagain']                          = "再次登入";
$mg2->lang['securitylogoff']                      = "登出";
$mg2->lang['autologoff']                          = "基於15分鐘的閒置, 你已被自動登出.";
$mg2->lang['logoff']                              = "登出";
$mg2->lang['forsecurity']                         = "基於安全理由, 建議關閉此瀏覽器視窗.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">此安裝已有 X 天. 點此查看新版本!</a></b>";
$mg2->lang['updatesuccess']                       = "更新成功";
$mg2->lang['renamefailure']                       = "錯誤: 圖檔名稱含有無效字元!";
$mg2->lang['filedeleted']                         = "圖檔已刪除";
$mg2->lang['filenotfound']                        = "無法找到圖檔!";
$mg2->lang['filesimported']                       = "圖檔已匯入";
$mg2->lang['nofilestoimport']                     = "錯誤: 沒有圖檔進行匯入!";
$mg2->lang['foldernotempty']                      = "錯誤: 不是空資料夾!";
$mg2->lang['folderdeleted']                       = "資料夾已刪除";
$mg2->lang['folderupdated']                       = "資料夾已更新";
$mg2->lang['foldercreated']                       = "資料夾已建立";
$mg2->lang['folderexists']                        = "錯誤: 資料夾名稱已存在!";
$mg2->lang['filesuploaded']                       = "檔案已上傳";
$mg2->lang['settingssaved']                       = "設定已儲存";
$mg2->lang['nopwdmatch']                          = "設定已儲存<br /><br />錯誤: 密碼核對錯誤 - 新密碼並未儲存!";
$mg2->lang['filesmovedto']                        = "檔案已移至";
$mg2->lang['filesdeleted']                        = "檔案已刪除";
$mg2->lang['file']                                = "圖檔";
$mg2->lang['files']                               = "圖檔";
$mg2->lang['folder']                              = "資料夾";
$mg2->lang['folders']                             = "資料夾";
$mg2->lang['rebuild']                             = "重建";
$mg2->lang['rebuildimages']                       = "重建縮圖";
$mg2->lang['rebuildsuccess']                      = "重建完成";
$mg2->lang['donate']                              = "MG2 為 GPL 授權方式的免費軟件. 如果你覺得它有用, 請按以下按鈕捐款給作者.";
$mg2->lang['from']                                = "來自";
$mg2->lang['comment']                             = "意見";
$mg2->lang['comments']                            = "意見";
$mg2->lang['by']                                  = "由";
$mg2->lang['commentsdeleted']                     = "意見已刪除";
$mg2->lang['buttonmove']                          = "移至";
$mg2->lang['buttondelete']                        = "刪除";
$mg2->lang['deleteconfirm']                       = "刪除選擇圖檔?";
$mg2->lang['imagecolumns']                        = "每行圖數";
$mg2->lang['imagerows']                           = "每頁行數";
$mg2->lang['viewfolder']                          = "觀看資料夾";
$mg2->lang['viewimage']                           = "觀看圖像";
$mg2->lang['viewgallery']                         = "觀看藝廊";
$mg2->lang['rotateright']                         = "向右90度旋轉";
$mg2->lang['rotateleft']                          = "向左90度旋轉";
$mg2->lang['imagerotated']                        = "圖像已旋轉!";
$mg2->lang['gifnotrotated']                       = "錯誤: 由於 GD lib 的限制 .GIF 圖檔不能旋轉!";
$mg2->lang['help']                                = "幫助";

$mg2->lang['slideshowdelay']                      = "幻燈片延遲";
$mg2->lang['websitelink']                         = "網站連結 (空白 = 不使用)";
$mg2->lang['marknew']                             = "標記新於 X 日的項目 (0 = 不使用)";
$mg2->lang['folderempty']                         = "此為空資料夾";
$mg2->lang['noimage']                             = "請求的圖像不存在!";
?>