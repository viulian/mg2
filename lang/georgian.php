﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Georgian                                    //
//                                                                                     //
//                               TRANSLATED BY: Guka Mesameli                          //
//             EMAIL: ITGuka@gmail.com Info@php.ge Guka@php.ge Guka@Guka.ge            //
//                                                                                     //
//                               LAST UPDATED: 03. August 2009                         //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////
// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "გალერეა";
$mg2->lang['of']                                  = "of";
$mg2->lang['first']                               = "პირველი";
$mg2->lang['prev']                                = "წინა";
$mg2->lang['next']                                = "შემდეგი";
$mg2->lang['last']                                = "ბოლო";
$mg2->lang['thumbs']                              = "იკონი";
$mg2->lang['exif info']                           = "ინფორმაცია სურათზე";
$mg2->lang['model']                               = "მოდელი";
$mg2->lang['shutter']                             = "გადაღების ხარისხი";
$mg2->lang['viewslideshow']                       = "სლაიდშოუ";
$mg2->lang['stopslideshow']                       = "სლაიდშოუს გამორთვა";
$mg2->lang['aperture']                            = "დიაფრაგმა";
$mg2->lang['flash']                               = "ხარისხი";
$mg2->lang['focallength']                         = "ფოკუსის დიამეტრი";
$mg2->lang['mm']                                  = " მმ";
$mg2->lang['exposurecomp']                        = "კომპენსაცია";
$mg2->lang['original']                            = "დრო / თარიღი";
$mg2->lang['metering']                            = "მარეგულირებელი";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "გვერდი";
$mg2->lang['all']                                 = "ყველა";
$mg2->lang['fullsize']                            = "სურათის სრულ ზომაში ნახვა";
$mg2->lang['addcomment']                          = "კომენტარის დამატება";
$mg2->lang['name']                                = "სახელი";
$mg2->lang['email']                               = "მაილი";
$mg2->lang['commentadded']                        = "კომენტარი დამატებულია";
$mg2->lang['commentexists']                       = "მოხდა შეცდომა! კომენტარი უკვე დამატებულია!";
$mg2->lang['commentmissing']                      = "მოხდა შეცდომა! ყველა ველის შევსება აუცილებელია!";
$mg2->lang['enterpassword']                       = "შეიყვანე პაროლი";
$mg2->lang['thissection']                         = "ალბომის სანახავად შეიყვანეთ პაროლი";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "მთავარი საქაღალდე";
$mg2->lang['thumb']                               = "იკონი";
$mg2->lang['dateadded']                           = "დამატების დრო";
$mg2->lang['upload']                              = "ფაილების ატვირთვა";
$mg2->lang['import']                              = "ატვირთული ფაილების იმპორტირება";
$mg2->lang['newfolder']                           = "ახალი საქაღალე";
$mg2->lang['viewgallery']                         = "გალერეის ნახვა";
$mg2->lang['setup']                               = "კონფიგურაცია";
$mg2->lang['logoff']                              = "გამოსვლა!";
$mg2->lang['menutxt_upload']                      = "ატვირთვა";
$mg2->lang['menutxt_import']                      = "იმპორტირება";
$mg2->lang['menutxt_newfolder']                   = "ახალი საქაღალე";
$mg2->lang['menutxt_viewgallery']                 = "გალერეის ნახვა";
$mg2->lang['menutxt_setup']                       = "კონფიგურაცია";
$mg2->lang['menutxt_logoff']                      = "გამოსვლა!";
$mg2->lang['delete']                              = "წაშლა";
$mg2->lang['cancel']                              = "შეწყვეტა";
$mg2->lang['ok']                                  = "ოკ :)";
$mg2->lang['deletefolder']                        = "საქაღალდის წაშლა";
$mg2->lang['navigation']                          = "ნავიგაცია";
$mg2->lang['images']                              = "სურათ(ებ)ი";
$mg2->lang['filename']                            = "დასახელება";
$mg2->lang['title']                               = "წარწერა";
$mg2->lang['description']                         = "აღწერა";
$mg2->lang['setasthumb']                          = "საქაღალდეზე იკონად დაყენება";
$mg2->lang['editfolder']                          = "საქაღალდის რედაქტირება";
$mg2->lang['editimage']                           = "სურათის რედაქტირება";
$mg2->lang['nofolderselected']                    = "საქაღალდე არ არის მონიშნული";
$mg2->lang['foldername']                          = "საქაღალდის სახელი";
$mg2->lang['newpassword']                         = "ახალი პაროლი";
$mg2->lang['deletepassword']                      = "პაროლის წაშლა";
$mg2->lang['introtext']                           = "ტექსტი";
$mg2->lang['deletethumb']                         = "იკონის წაშლა";
$mg2->lang['moveto']                              = "გადატანა";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "ზომა";
$mg2->lang['width']                               = "სიგრძე";
$mg2->lang['height']                              = "სიგანე";
$mg2->lang['date']                                = "თარიღი";
$mg2->lang['ascending']                           = "ზრდადი";
$mg2->lang['descending']                          = "კლებადი";
$mg2->lang['newfolder']                           = "ახალი საქაღალდე";
$mg2->lang['password']                            = "პაროლი";
$mg2->lang['direction']                           = "მიმართულება";
$mg2->lang['sortby']                              = "დავალაგო როგორც";
$mg2->lang['gallerytitle']                        = "გალერიის წარწერა";
$mg2->lang['adminemail']                          = "ადმინის მაილი";
$mg2->lang['language']                            = "ენა";
$mg2->lang['skin']                                = "სკინი";
$mg2->lang['dateformat']                          = "თარიღის ფორმატი";
$mg2->lang['DDMMYY']                              = "DD MMM YYYY";
$mg2->lang['MMDDYY']                              = "MMM DD, YYYY";
$mg2->lang['MM.DD.YY']                            = "MM.DD.YY";
$mg2->lang['DD.MM.YY']                            = "DD.MM.YY";
$mg2->lang['YYYYMMDD']                            = "YYYYMMDD";
$mg2->lang['sendmail']                            = "გამომიგზავნე კომენტარები მაილზე";
$mg2->lang['foldericons']                         = "საქაღალდის იკონები";
$mg2->lang['showexif']                            = "ინფორმაციის ჩვენება";
$mg2->lang['allowcomments']                       = "კომენტარების დაშვება";
$mg2->lang['copyright']                           = "საიტის ხელმოწერა";
$mg2->lang['passwordchange']                      = "პაროლის შეცვლა";
$mg2->lang['oldpasswordsetup']                    = "მიმდინარე პაროლი";
$mg2->lang['newpasswordsetup']                    = "ახალი პაროლი ";
$mg2->lang['newpasswordsetupconfirm']             = "გაიმეორეთ პაროლი";
$mg2->lang['advanced']                            = "ძირითადი";
$mg2->lang['allowedextensions']                   = "დაშვებული გაფართიებები";
$mg2->lang['imgwidth']                            = "მაქსიმალური ზომა სურათის (0 = გამორთვა)";
$mg2->lang['indexfile']                           = "გალერიის საწყისი";
$mg2->lang['thumbquality']                        = "იკონის ხარისხი";
$mg2->lang['image']                               = "სურათი";
$mg2->lang['edit']                                = "შეცვლა";
$mg2->lang['editcurrentfolder']                   = "მოცემული საქაღალდის რედაქტირება";
$mg2->lang['deletecurrentfolder']                 = "მოცემული საქაღალდის წაშლა";

$mg2->lang['loginagain']                          = "ახლიდან შესვლა";
$mg2->lang['securitylogoff']                      = "დაცული გამოსვლა!";
$mg2->lang['autologoff']                          = "თქვენ ავტომატურად გახვედით სისტემიდან, 15 წუთი უმოქმედობის გამო.";
$mg2->lang['logoff']                              = "გასვლა!";
$mg2->lang['forsecurity']                         = "თქვენივე უსაფრთხოებისათვის რეკომენდებულია დახუროთ ბრაუზერის ეს ფანჯარა.";
$mg2->lang['updatesuccess']                       = "განახლდა წარმატებით";
$mg2->lang['renamefailure']                       = "მოხდა შეცდომა: ფაილის სახელი შეიცვას უცხო სიმბოლოებს!";
$mg2->lang['filedeleted']                         = "ფაილი წაშლილია";
$mg2->lang['filenotfound']                        = "ფაილის პოვნა ვერ მოხერხდა";
$mg2->lang['filesimported']                       = "ფაილ(ებ)ი იმპორტირებულია";
$mg2->lang['nofilestoimport']                     = "მოხდა შეცდომა: იმპორტირებისთვის ფაილები ვერ მოიძებნა!";
$mg2->lang['foldernotempty']                      = "მოხდა შეცდომა: საქაღალდე არ არის ცარიელი!";
$mg2->lang['folderdeleted']                       = "საქაღალდე წაშლილია";
$mg2->lang['folderupdated']                       = "საქაღალდე განახლებულია";
$mg2->lang['foldercreated']                       = "საქაღალდე შექმნილია";
$mg2->lang['folderexists']                        = "მოხდა შეცდომა: მსგავსი სახელის საქაღალდე უკვე არსებობს!";
$mg2->lang['filesuploaded']                       = "ფაილ(ებ)ი აიტვირთა - მიმდინარეობს იმპორტირება...";
$mg2->lang['settingssaved']                       = "მონაცემები შენახულია";
$mg2->lang['nopwdmatch']                          = "მონაცემები შენახულია<br /><br />მოხდა შეცდომა: ახალი პაროლი ვერ დავიმახსოვრე!";
$mg2->lang['filesmovedto']                        = "ფაილ(ებ)ი გადატანილია";
$mg2->lang['filesdeleted']                        = "ფაილ(ებ)ი წაშლილია!";
$mg2->lang['file']                                = "ფაილი";
$mg2->lang['files']                               = "ფაილები";
$mg2->lang['folder']                              = "საქაღალდე";
$mg2->lang['folders']                             = "საქაღალდეები";
$mg2->lang['rebuild']                             = "ახლიდან შექმნა";
$mg2->lang['rebuildimages']                       = "იკონების ახლიდან შექმნა";
$mg2->lang['rebuildsuccess']                      = "ახლიდან შექმნა დასრულებულია";

$mg2->lang['from']                                = "From";
$mg2->lang['comment']                             = "კომენტარი";
$mg2->lang['comments']                            = "კომენატრები";
$mg2->lang['commentsdeleted']                     = "კომენტარ(ებ)ი წაშლილია";
$mg2->lang['buttonmove']                          = "გადატანა";
$mg2->lang['buttondelete']                        = "წაშლა";
$mg2->lang['deleteconfirm']                       = "წავშალო აღნიშნული ფაილი?";
$mg2->lang['imagecolumns']                        = "სურათების რაოდენობა ჰორიზონტალიურად";
$mg2->lang['imagerows']                           = "სურათების რაოდენობა ვერტიკალურად";
$mg2->lang['viewfolder']                          = "საქაღალდის ნახვა";
$mg2->lang['viewimage']                           = "სურათის ნახვა";
$mg2->lang['viewgallery']                         = "გალერეის ნახვა";
$mg2->lang['rotateright']                         = "90 გრადუსით შემობრუნება (მარჯვნივ)";
$mg2->lang['rotateleft']                          = "90 გრადუსით შემობრუნება (მარცხნივ)";
$mg2->lang['imagerotated']                        = "სურათი მობრუნდა";
$mg2->lang['gifnotrotated']                       = "მოხდა შეცდომა";
$mg2->lang['help']                                = "დახმარება";
$mg2->lang['slideshowdelay']                      = "სლაიდშოუს ხანგრძლივობა";
$mg2->lang['websitelink']                         = "საიტის ლინკი (ცარიელი = გათიშვა";
$mg2->lang['marknew']                             = "ავღნიშნოთ პუნკტები ახლად, უახლოესი X დღის განმავლობაში (0 = გამორთვა)";
$mg2->lang['folderempty']                         = "საქაღალდე ცარიელია";
$mg2->lang['noimage']                             = "სურათის მოძებნა ვერ მოხერხდა!";


$mg2->lang['actions']                             = "მოქმედება";
$mg2->lang['backupcomplete']                      = "ბაზების შენახვა დასრულებულია";
$mg2->lang['backuplink']                          = "ბაზების შენახვა";
$mg2->lang['viewlogfile']                         = "ლოგფაილის ნახვა";
$mg2->lang['website']                             = "საიტზე";
$mg2->lang['goto']                                = "გადასვლა";
$mg2->lang['backtofolder']                        = "საქაღალდეში დაბრუნება";
$mg2->lang['permerror1']                          = "ფერმიშენ შეცდომა: შეუძლებელია მთავარი დირექტორიის წაკითხვა!";
$mg2->lang['whattodo1']                           = "აუცილებელი მოთხოვნა: ჩმოდები მთავარ დრიექტორიაზე 777";
$mg2->lang['permerror2']                          = "ფერმიშენ შეცდომა: შეუძლებელია 'pictures' დირექტორიის წაკითხვა!";
$mg2->lang['whattodo2']                           = "აუცილებელი მოთხოვნა: ჩმოდები 'pictures' დირექტორიაზე 777";
$mg2->lang['permerror3']                          = "ფერმიშენ შეცდომა: შეუძლებელია 'mg2db_idatabase.php' დირექტორიის წაკითხვა!";
$mg2->lang['whattodo3']                           = "აუცილებელი მოთხოვნა: ჩმოდები 'mg2db_idatabase.php' დირექტორიაზე 777";
$mg2->lang['permerror4']                          = "ფერმიშენ შეცდომა: შეუძლებელია 'mg2db_idatabase_temp.php' დირექტორიის წაკითხვა!";
$mg2->lang['whattodo4']                           = "აუცილებელი მოთხოვნა: ჩმოდები 'mg2db_idatabase_temp.php' დირექტორიაზე 777";
$mg2->lang['permerror5']                          = "ფერმიშენ შეცდომა: შეუძლებელია 'mg2db_fdatabase.php' დირექტორიის წაკითხვა!";
$mg2->lang['whattodo5']                           = "აუცილებელი მოთხოვნა: ჩმოდები 'mg2db_fdatabase.php' დირექტორიაზე 777";
$mg2->lang['permerror6']                          = "ფერმიშენ შეცდომა: შეუძლებელია 'mg2db_fdatabase_temp.php' დირექტორიის წაკითხვა!";
$mg2->lang['whattodo6']                           = "აუცილებელი მოთხოვნა: ჩმოდები 'mg2db_fdatabase_temp.php' დირექტორიაზე 777";

?>
