﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Polish                                      //
//                                                                                     //
//                               TRANSLATED BY: Pawel Stanczuk (update dżafeł)         //
//                               EMAIL: support@minigal.dk                             //
//                                                                                     //
//                               LAST UPDATED: 03. Aug 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "iso-8859-2";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galeria";
$mg2->lang['of']                                  = "z";
$mg2->lang['first']                               = "Pierwszy";
$mg2->lang['prev']                                = "Poprzedni";
$mg2->lang['next']                                = "Następny";
$mg2->lang['last']                                = "Ostatni";
$mg2->lang['thumbs']                              = "Miniaturki";
$mg2->lang['exif info']                           = "Informacja Exif";
$mg2->lang['model']                               = "Model";
$mg2->lang['shutter']                             = "Szybkość migawki";
$mg2->lang['viewslideshow']                       = "Oglądaj pokaz slajdów";
$mg2->lang['stopslideshow']                       = "Zatrzymaj pokaz slajdów";
$mg2->lang['aperture']                            = "Aperture";
$mg2->lang['flash']                               = "Lampa błyskowa";
$mg2->lang['focallength']                         = "Długość ogniskowej";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Ustawienie jasności";
$mg2->lang['original']                            = "Oryginalny";
$mg2->lang['metering']                            = "Metrowanie";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Strona";
$mg2->lang['all']                                 = "Wszystko";
$mg2->lang['fullsize']                            = "Zobacz obrazek w pełnych wymiarach";
$mg2->lang['addcomment']                          = "Dodaj komentarz";
$mg2->lang['name']                                = "Imię";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Komentarz dodany";
$mg2->lang['commentexists']                       = "BŁĄD: Komentarz już istnieje";
$mg2->lang['commentmissing']                      = "BŁĄD: Wszystkie pola komentarza muszą być uzupełnione";
$mg2->lang['enterpassword']                       = "Wpisz hasło";
$mg2->lang['thissection']                         = "Ta sekcja jest chroniona hasłem";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Administrator";
$mg2->lang['thumb']                               = "Miniaturka";
$mg2->lang['dateadded']                           = "Data dodania";
$mg2->lang['upload']                              = "Uploaduj";
$mg2->lang['import']                              = "Importuj wrzucone pliki do";
$mg2->lang['newfolder']                           = "Nowy folder";
$mg2->lang['viewgallery']                         = "Oglądaj galerię";
$mg2->lang['setup']                               = "Ustawienia";
$mg2->lang['logoff']                              = "Wyloguj";
$mg2->lang['menutxt_upload']                      = "Wrzuć plik na serwer";
$mg2->lang['menutxt_import']                      = "Importuj";
$mg2->lang['menutxt_newfolder']                   = "Nowy folder";
$mg2->lang['menutxt_viewgallery']                 = "Oglądaj galerię";
$mg2->lang['menutxt_setup']                       = "Ustawienia";
$mg2->lang['menutxt_logoff']                      = "Wyloguj";
$mg2->lang['delete']                              = "Usuń";
$mg2->lang['cancel']                              = "Anuluj";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Usuń folder";
$mg2->lang['navigation']                          = "Nawigacja";
$mg2->lang['images']                              = "Obrazki";
$mg2->lang['filename']                            = "Nazwa pliku";
$mg2->lang['title']                               = "Tytuł";
$mg2->lang['description']                         = "Opis";
$mg2->lang['setasthumb']                          = "Ustaw jako miniaturka folderu";
$mg2->lang['editfolder']                          = "Edytuj folder";
$mg2->lang['editimage']                           = "Edytuj obrazek";
$mg2->lang['nofolderselected']                    = "Nie ma zaznaczonych folderów";
$mg2->lang['foldername']                          = "Nazwa folderu";
$mg2->lang['newpassword']                         = "Nowe hasło";
$mg2->lang['deletepassword']                      = "Usuń hasło";
$mg2->lang['introtext']                           = "Tekst powitalny";
$mg2->lang['deletethumb']                         = "Usuń miniaturkę";
$mg2->lang['moveto']                              = "Idź do";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Rozmiar pliku";
$mg2->lang['width']                               = "Szerokość";
$mg2->lang['height']                              = "Wysokość";
$mg2->lang['date']                                = "Data";
$mg2->lang['ascending']                           = "Rosnąco";
$mg2->lang['descending']                          = "Malejąco";
$mg2->lang['newfolder']                           = "Nowy folder";
$mg2->lang['password']                            = "Hasło";
$mg2->lang['direction']                           = "Kierunek";
$mg2->lang['sortby']                              = "Sortowanie";
$mg2->lang['gallerytitle']                        = "Nazwa galerii";
$mg2->lang['adminemail']                          = "Email administratora";
$mg2->lang['language']                            = "Język";
$mg2->lang['skin']                                = "Skórka";
$mg2->lang['dateformat']                          = "Format daty";
$mg2->lang['DDMMYY']                              = "DD MMM RRRR";
$mg2->lang['MMDDYY']                              = "MMM DD, RRRR";
$mg2->lang['MM.DD.YY']                            = "MM.DD.RR";
$mg2->lang['DD.MM.YY']                            = "DD.MM.RR";
$mg2->lang['YYYYMMDD']                            = "RRRRMMDD";
$mg2->lang['sendmail']                            = "Wyślij komentarz emailem";
$mg2->lang['foldericons']                         = "Force folder icons";
$mg2->lang['showexif']                            = "Pokazuj Exif";
$mg2->lang['allowcomments']                       = "Pozwól komentować";
$mg2->lang['copyright']                           = "Notka na temat praw autorskich";
$mg2->lang['passwordchange']                      = "Zmień hasło (3 x puste pole = zostaw obecne)";
$mg2->lang['oldpasswordsetup']                    = "Wpisz obecne hasło";
$mg2->lang['newpasswordsetup']                    = "Nowe hasło (puste pole = użyj obecnego)";
$mg2->lang['newpasswordsetupconfirm']             = "Wpisz nowe hasło ponownie";
$mg2->lang['advanced']                            = "Zaawansowane";
$mg2->lang['allowedextensions']                   = "Dozwolone formaty";
$mg2->lang['imgwidth']                            = "Max. szerokośc pliku (0 = zablokowane)";
$mg2->lang['indexfile']                           = "Plik index galerii";
$mg2->lang['thumbquality']                        = "Jakośc miniaturek";
$mg2->lang['image']                               = "Obrazek";
$mg2->lang['edit']                                = "Edytuj";
$mg2->lang['editcurrentfolder']                   = "Edytuj obecny folder";
$mg2->lang['deletecurrentfolder']                 = "Usuń obecny folder";
$mg2->lang['by']                                  = "przez";
$mg2->lang['loginagain']                          = "Zaloguj się ponownie";
$mg2->lang['securitylogoff']                      = "Bezpieczne wylogowanie";
$mg2->lang['autologoff']                          = "Zostałeś automatycznie wylogowany po 15 minutach nieaktywności.";
$mg2->lang['logoff']                              = "Wyloguj";
$mg2->lang['forsecurity']                         = "Ze względów bezpieczeństwa, jest zalecane, aby zamknąć to okno przeglądarki.";
$mg2->lang['updatesuccess']                       = "Uaktualnianie zakończone powodzeniem";
$mg2->lang['renamefailure']                       = "BŁĄD: Nazwa pliku zawiera niedowzolone znaki!";
$mg2->lang['filedeleted']                         = "Plik usunięty";
$mg2->lang['filenotfound']                        = "Plik nie został znaleziony!";
$mg2->lang['filesimported']                       = "Pliki zimportowane";
$mg2->lang['nofilestoimport']                     = "BŁĄD: Brak plików do importowania!";
$mg2->lang['foldernotempty']                      = "BŁĄD: Folder nie jest pusty!";
$mg2->lang['folderdeleted']                       = "Folder został usunięty";
$mg2->lang['folderupdated']                       = "Folder został uaktualniony";
$mg2->lang['foldercreated']                       = "Folder został utworzony";
$mg2->lang['folderexists']                        = "BŁĄD: folder o takiej nazwie nie istnieje!";
$mg2->lang['filesuploaded']                       = "Pliki zosały wrzucone";
$mg2->lang['settingssaved']                       = "Zapisano ustawienia";
$mg2->lang['nopwdmatch']                          = "Zapisano ustawienia<br /><br />BŁĄD: Błędne hasło - nowe hasło nie zostało zapisane!";
$mg2->lang['filesmovedto']                        = "pliki przeniesiono do";
$mg2->lang['filesdeleted']                        = "pliki usunięte!";
$mg2->lang['file']                                = "plik";
$mg2->lang['files']                               = "pliki";
$mg2->lang['folder']                              = "folder";
$mg2->lang['folders']                             = "foldery";
$mg2->lang['rebuild']                             = "Przebuduj";
$mg2->lang['rebuildimages']                       = "Przebuduj miniaturki";
$mg2->lang['rebuildsuccess']                      = "Przebudowywanie zakończone";
$mg2->lang['donate']                              = "MG2 jest darmowym oprogramowaniem, na licencji GPL. Jeżeli korzystasz z niego i uważasz je za przydatne, proszę, wspomóż autora finansowo poprzez naciśnięcie poniższego przycisku.";
$mg2->lang['from']                                = "Z";
$mg2->lang['comment']                             = "Komentarz";
$mg2->lang['comments']                            = "Komentarze";
$mg2->lang['by']                                  = "przez";
$mg2->lang['commentsdeleted']                     = "Komentarze usunięte";
$mg2->lang['buttonmove']                          = "Przenieś";
$mg2->lang['buttondelete']                        = "Usuń";
$mg2->lang['deleteconfirm']                       = "Usunąć zaznaczone pliki?";
$mg2->lang['imagecolumns']                        = "Ilość kolumn";
$mg2->lang['imagerows']                           = "Ilość wierszy";
$mg2->lang['viewfolder']                          = "Obejrzyj folder";
$mg2->lang['viewimage']                           = "Obejrzyj obrazek";
$mg2->lang['viewgallery']                         = "Obejrzyj galerię";
$mg2->lang['rotateright']                         = "Obróć 90 stopni w prawo";
$mg2->lang['rotateleft']                          = "Obróć 90 stopni w lewo";
$mg2->lang['imagerotated']                        = "Obrazek obrócony!";
$mg2->lang['gifnotrotated']                       = "BŁĄD: pliki .GIF  nie mogą być obracane z powodu ograniczeń biblioteki GD!";
$mg2->lang['help']                                = "Pomoc";

// LAST UPDATE (by dżafeł - www.AllegroFun.pl)
$mg2->lang['slideshowdelay']                      = "Opóźnienie pokazu sjaldów";
$mg2->lang['websitelink']                         = "Odnośnik (blank = wyłączony)";
$mg2->lang['marknew']                             = "Zaznacz jako nowe dla X dni (0 = wyłączone)";
$mg2->lang['folderempty']                         = "Ten folder jest pusty";
$mg2->lang['noimage']                             = "Obrazek, który próbujesz wyświetlić, nie istnieje!";
$mg2->lang['actions']                             = "Akcja";
$mg2->lang['backupcomplete']                      = "Kopia zapasowa bazy utworzona!";
$mg2->lang['backuplink']                          = "Stwórz kopię zapasową bazy";
$mg2->lang['viewlogfile']                         = "Zobacz plik LOG";
$mg2->lang['website']                             = "Do strony";
$mg2->lang['backtofolder']                        = "Powrót do folderu";
$mg2->lang['permerror1']                          = "BŁĄD DOSTĘPU: Nie można zapisać do głównego folderu galerii!";
$mg2->lang['whattodo1']                           = "Nadaj głównemu folderowi galerii CHMOD 777";
$mg2->lang['permerror2']                          = "BŁĄD DOSTĘPU: Nie można zapisać do folderu 'pictures'!";
$mg2->lang['whattodo2']                           = "Nadaj dla folderu 'pictures' CHMOD 777";
$mg2->lang['permerror3']                          = "BŁĄD DOSTĘPU: Nie można zapisać do pliku 'mg2db_idatabase.php'!";
$mg2->lang['whattodo3']                           = "Nadaj dla pliku 'mg2db_idatabase.php' CHMOD 777";
$mg2->lang['permerror4']                          = "BŁĄD DOSTĘPU: Nie można zapisać do pliku 'mg2db_idatabase_temp.php'!";
$mg2->lang['whattodo4']                           = "Nadaj dla pliku 'mg2db_idatabase_temp.php' CHMOD 777";
$mg2->lang['permerror5']                          = "BŁĄD DOSTĘPU: Nie można zapisać do pliku 'mg2db_fdatabase.php'!";
$mg2->lang['whattodo5']                           = "Nadaj dla pliku 'mg2db_fdatabase.php' CHMOD 777";
$mg2->lang['permerror6']                          = "BŁĄD DOSTĘPU: Nie można zapisać do pliku 'mg2db_fdatabase_temp.php'!";
$mg2->lang['whattodo6']                           = "Nadaj dla pliku 'mg2db_fdatabase_temp.php' CHMOD 777";
?>
