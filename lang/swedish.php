<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                     Svenska/Swedish                                 //
//                                                                                     //
//               TRANSLATED BY: Johan Stenehall (Chorizo) and Henrik Samuelsson	   //
//               EMAIL: johan@stenehall.se, henriksamuelsson@yahoo.se                  //
//                                                                                     //
//                               LAST UPDATED: 27. Jun 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galleri";
$mg2->lang['of']                                  = "av";
$mg2->lang['first']                               = "F&ouml;rsta";
$mg2->lang['prev']                                = "F&ouml;reg&aring;ende";
$mg2->lang['next']                                = "N&auml;sta";
$mg2->lang['last']                                = "Sista";
$mg2->lang['thumbs']                              = "Miniatyr";
$mg2->lang['exif info']                           = "Exif-information";
$mg2->lang['model']                               = "Modell";
$mg2->lang['shutter']                             = "Slutartid";
$mg2->lang['viewslideshow']                       = "Titta p&aring; bildspel";
$mg2->lang['stopslideshow']                       = "Avsluta bildspel";
$mg2->lang['aperture']                            = "Bl&auml;ndare ";
$mg2->lang['flash']                               = "Blixt";
$mg2->lang['focallength']                         = "Br&auml;nnvidd";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Exponeringskompenation";
$mg2->lang['original']                            = "Orginal";
$mg2->lang['metering']                            = "M&auml;tare";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Sida";
$mg2->lang['all']                                 = "Alla";
$mg2->lang['fullsize']                            = "Titta p&aring; fullskalig bild";
$mg2->lang['addcomment']                          = "L&auml;gg till kommentar";
$mg2->lang['name']                                = "Namn";
$mg2->lang['email']                               = "Epost";
$mg2->lang['commentadded']                        = "Kommentar tillagd";
$mg2->lang['commentexists']                       = "FEL: Kommentar existerar redan!";
$mg2->lang['commentmissing']                      = "FEL: Alla kommentarf&auml;lt m&aring;ste vara ifyllda!";
$mg2->lang['enterpassword']                       = "Skriv in l&ouml;senord";
$mg2->lang['thissection']                         = "Den h&auml;r delen &auml;r l&ouml;senordsskyddad";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Root";
$mg2->lang['thumb']                               = "Miniatyr";
$mg2->lang['dateadded']                           = "Datum tillagt";
$mg2->lang['upload']                              = "Ladda upp filer";
$mg2->lang['import']                              = "Importera uppladdade filer till";
$mg2->lang['newfolder']                           = "Ny mapp";
$mg2->lang['viewgallery']                         = "Visa galleriet";
$mg2->lang['setup']                               = "Inst&auml;llningar";
$mg2->lang['logoff']                              = "Logga ut";
$mg2->lang['menutxt_upload']                      = "Ladda upp";
$mg2->lang['menutxt_import']                      = "Importera";
$mg2->lang['menutxt_newfolder']                   = "Ny mapp";
$mg2->lang['menutxt_viewgallery']                 = "Visa galleriet";
$mg2->lang['menutxt_setup']                       = "Inst&auml;llningar";
$mg2->lang['menutxt_logoff']                      = "Logga ut";
$mg2->lang['delete']                              = "Ta bort";
$mg2->lang['cancel']                              = "Avbryt";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Ta bort mappen";
$mg2->lang['navigation']                          = "Navigera";
$mg2->lang['images']                              = "bild(er)";
$mg2->lang['filename']                            = "Filnamn";
$mg2->lang['title']                               = "Titel";
$mg2->lang['description']                         = "Beskrivning";
$mg2->lang['setasthumb']                          = "Anv&auml;nd som mappminiatyr";
$mg2->lang['editfolder']                          = "Redigera mappen";
$mg2->lang['editimage']                           = "Redigera bilden";
$mg2->lang['nofolderselected']                    = "Ingen mapp vald";
$mg2->lang['foldername']                          = "Mappnamn";
$mg2->lang['newpassword']                         = "Nytt l&ouml;senord";
$mg2->lang['deletepassword']                      = "Ta bort l&ouml;senord";
$mg2->lang['introtext']                           = "Introtext";
$mg2->lang['deletethumb']                         = "Ta bort miniatyr";
$mg2->lang['moveto']                              = "Flytta till";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Filstorlek";
$mg2->lang['width']                               = "Bredd";
$mg2->lang['height']                              = "H&ouml;jd";
$mg2->lang['date']                                = "Datum";
$mg2->lang['ascending']                           = "Fallande";
$mg2->lang['descending']                          = "Stigande";
$mg2->lang['newfolder']                           = "Ny mapp";
$mg2->lang['password']                            = "L&ouml;senord";
$mg2->lang['direction']                           = "Ordning";
$mg2->lang['sortby']                              = "Sortera efter";
$mg2->lang['gallerytitle']                        = "Gallerititel";
$mg2->lang['adminemail']                          = "Admin-epost";
$mg2->lang['language']                            = "Spr&aring;k";
$mg2->lang['skin']                                = "Utseende (skin)";
$mg2->lang['dateformat']                          = "Datumformat";
$mg2->lang['DDMMYY']                              = "DD MM &Aring;&Aring;&Aring;&Aring;";
$mg2->lang['MMDDYY']                              = "MMM DD, &Aring;&Aring;&Aring;&Aring;";
$mg2->lang['MM.DD.YY']                            = "MM.DD.YY";
$mg2->lang['DD.MM.YY']                            = "DD.MM.&Aring;&Aring;";
$mg2->lang['YYYYMMDD']                            = "&Aring;&Aring;&Aring;&Aring;MMDD";
$mg2->lang['sendmail']                            = "Skicka epostkommentar";
$mg2->lang['foldericons']                         = "Framtvinga mappikoner";
$mg2->lang['showexif']                            = "Visa Exif";
$mg2->lang['allowcomments']                       = "Till&aring;t kommentarer";
$mg2->lang['copyright']                           = "Upphovsr&auml;ttsinformation";
$mg2->lang['passwordchange']                      = "&Auml;ndra l&ouml;senord (3 x blankt = beh&aring;ll nuvarande)";
$mg2->lang['oldpasswordsetup']                    = "Skriv in nuvarande l&ouml;senord";
$mg2->lang['newpasswordsetup']                    = "Nytt l&ouml;senord (blankt = anv&auml;nd nuvarande)";
$mg2->lang['newpasswordsetupconfirm']             = "Skriv in nytt l&ouml;senord igen";
$mg2->lang['advanced']                            = "Avancerat";
$mg2->lang['allowedextensions']                   = "Till&aring;tna fil&auml;ndelser";
$mg2->lang['imgwidth']                            = "Max. bildbredd (0 = inaktivera)";
$mg2->lang['indexfile']                           = "Galleriets indexfil";
$mg2->lang['thumbquality']                        = "Miniatyrernas kvalité";
$mg2->lang['image']                               = "Bild";
$mg2->lang['edit']                                = "Redigera";
$mg2->lang['editcurrentfolder']                   = "Redigera nuvarande mapp";
$mg2->lang['deletecurrentfolder']                 = "Ta bort nuvarande mapp";
$mg2->lang['by']                                  = "av";
$mg2->lang['loginagain']                          = "Logga in igen";
$mg2->lang['securitylogoff']                      = "S&auml;kerhetsavloggning";
$mg2->lang['autologoff']                          = "Du har automatiskt blivit utloggad efter 15 minuters inaktivitet";
$mg2->lang['logoff']                              = "Logga ut";
$mg2->lang['forsecurity']                         = "Av s&auml;kerhetssk&auml;l &auml;r det rekomenderat att st&auml;nga det h&auml;r webbl&auml;sarf&ouml;nstret";
$mg2->lang['updatesuccess']                       = "Uppdatering lyckades";
$mg2->lang['renamefailure']                       = "FEL: Filnamnet inneh&aring;ller otill&aring;tna tecken!";
$mg2->lang['filedeleted']                         = "Filen borttagen";
$mg2->lang['filenotfound']                        = "Filen hittades inte!";
$mg2->lang['filesimported']                       = "fil(er) importerad(e)";
$mg2->lang['nofilestoimport']                     = "FEL: Inga filer att importera!";
$mg2->lang['foldernotempty']                      = "FEL: Mappen &auml;r inte tom!";
$mg2->lang['folderdeleted']                       = "Mappen borttagen";
$mg2->lang['folderupdated']                       = "Mappen uppdaterad";
$mg2->lang['foldercreated']                       = "Mappen skapad";
$mg2->lang['folderexists']                        = "FEL: Mappnamnet finns redan!";
$mg2->lang['filesuploaded']                       = "Fil(er) uppladdad(e) - Importerar";
$mg2->lang['settingssaved']                       = "Inst&auml;llningar sparade";
$mg2->lang['nopwdmatch']                          = "Inst&auml;llningar sparade<br /><br />FEL: Omaka l&ouml;senord - nytt l&ouml;senord ej sparat!";
$mg2->lang['filesmovedto']                        = "fil(er) flyttad(e) till";
$mg2->lang['filesdeleted']                        = "fil(er) raderad(e)";
$mg2->lang['file']                                = "fil";
$mg2->lang['files']                               = "filer";
$mg2->lang['folder']                              = "mapp";
$mg2->lang['folders']                             = "mappar";
$mg2->lang['rebuild']                             = "&Aring;terskapa";
$mg2->lang['rebuildimages']                       = "&Aring;terskapa miniatyrer";
$mg2->lang['rebuildsuccess']                      = "&Aring;terskapat!";
$mg2->lang['donate']                              = "MG2 &auml;r fri programvara, licensierad under GPL. Om du finner detta program anv&auml;ndbart, l&auml;mna d&aring; g&auml;rna ett bidrag till skaparen genom att klicka p&aring; knappen nedan.";
$mg2->lang['from']                                = "Fr&aring;n";
$mg2->lang['comment']                             = "Kommentar";
$mg2->lang['comments']                            = "Kommentarer";
$mg2->lang['by']                                  = "av";
$mg2->lang['commentsdeleted']                     = "Kommentar(er) borttag(en/na)";
$mg2->lang['buttonmove']                          = "Flytta";
$mg2->lang['buttondelete']                        = "Radera";
$mg2->lang['deleteconfirm']                       = "Radera valda filer?";
$mg2->lang['imagecolumns']                        = "Bildkolumner";
$mg2->lang['imagerows']                           = "Bildrader";
$mg2->lang['viewfolder']                          = "Visa mappen";
$mg2->lang['viewimage']                           = "Visa bild";
$mg2->lang['viewgallery']                         = "Visa galleri";
$mg2->lang['rotateright']                         = "Rotera 90 grader &aring;t h&ouml;ger";
$mg2->lang['rotateleft']                          = "Rotera 90 grader &aring;t v&auml;nster";
$mg2->lang['imagerotated']                        = "Bilden roterad!";
$mg2->lang['gifnotrotated']                       = "FEL: .GIF filer kan inte roteras p&aring;grund av begr&auml;nsningar i GD lib!";
$mg2->lang['help']                                = "Hj&auml;lp";
$mg2->lang['slideshowdelay']                      = "Bildspels f&ouml;rdr&ouml;jning";
$mg2->lang['websitelink']                         = "Webbsidel&auml;nk (blank = inaktivera)";
$mg2->lang['marknew']                             = "Markera objekt nyare &auml;n X dagar (0 = inaktivera)";
$mg2->lang['folderempty']                         = "Mappen &auml;r tom";
$mg2->lang['noimage']                             = "Efterfr&aring;gad bild existerar inte!";
$mg2->lang['actions']                             = "Underh&aring;ll";
$mg2->lang['backupcomplete']                      = "S&auml;kerhetskopiering av databasen klar";
$mg2->lang['backuplink']                          = "G&ouml;r en s&auml;kerhetskopia av databasen";
$mg2->lang['viewlogfile']                         = "Visa loggfilen";
$mg2->lang['website']                             = "Till webbsidan";
$mg2->lang['backtofolder']                        = "Tillbaka till mapp";
$mg2->lang['permerror1']                          = "TILLST&Aring;NDEN FEL: Kan inte skriva till galleriets rootmapp!";
$mg2->lang['whattodo1']                           = "Chmod ditt gallerimapp till 777";
$mg2->lang['permerror2']                          = "TILLST&Aring;NDEN FEL: Kan inte skriva till 'pictures' mappen!";
$mg2->lang['whattodo2']                           = "Chmod din 'pictures' mapp till 777";
$mg2->lang['permerror3']                          = "TILLST&Aring;NDEN FEL: Kan inte skriva till 'mg2db_idatabase.php' filen!";
$mg2->lang['whattodo3']                           = "Chmod 'mg2db_idatabase.php' filen till 777";
$mg2->lang['permerror4']                          = "TILLST&Aring;NDEN FEL: Kan inte skriva till 'mg2db_idatabase_temp.php' filen!";
$mg2->lang['whattodo4']                           = "Chmod 'mg2db_idatabase_temp.php' filen till 777";
$mg2->lang['permerror5']                          = "TILLST&Aring;NDEN FEL: Kan inte skriva till 'mg2db_fdatabase.php' filen!";
$mg2->lang['whattodo5']                           = "Chmod 'mg2db_fdatabase.php' filen till 777";
$mg2->lang['permerror6']                          = "TILLST&Aring;NDEN FEL: CKan inte skriva till 'mg2db_fdatabase_temp.php' filen!";
$mg2->lang['whattodo6']                           = "Chmod 'mg2db_fdatabase_temp.php' filen till 777";

?>
