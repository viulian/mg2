﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Bosanski                                    //
//                                                                                     //
//                               TRANSLATED BY: Mesud Hasanovic                        //
//                               EMAIL: pervanac@yahoo.fr                              //
//                                                                                     //
//                               LAST UPDATED: 17. mart 2005                           //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galerija";
$mg2->lang['of']                                  = "od";
$mg2->lang['first']                               = "Prva";
$mg2->lang['prev']                                = "Prethodna";
$mg2->lang['next']                                = "Slijedeca";
$mg2->lang['last']                                = "Zadnja";
$mg2->lang['thumbs']                              = "Minijaturne";
$mg2->lang['exif info']                           = "Exif Informacije";
$mg2->lang['model']                               = "Model";
$mg2->lang['shutter']                             = "Shutter speed";
$mg2->lang['viewslideshow']                       = "Pokrenuti slideshow";
$mg2->lang['stopslideshow']                       = "Zaustaviti slideshow";
$mg2->lang['aperture']                            = "Aperture";
$mg2->lang['flash']                               = "Flash";
$mg2->lang['focallength']                         = "Focal length";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Exposure compensation";
$mg2->lang['original']                            = "Orginal";
$mg2->lang['metering']                            = "Metering";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Stranica";
$mg2->lang['all']                                 = "Sve";
$mg2->lang['fullsize']                            = "Uvecati";
$mg2->lang['addcomment']                          = "Napisati komentar";
$mg2->lang['name']                                = "Ime";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Komentar upisan";
$mg2->lang['commentexists']                       = "ERROR: Komentar vec upisan!";
$mg2->lang['commentmissing']                      = "ERROR: Sva polja moraju biti ispunjena!";
$mg2->lang['enterpassword']                       = "Unesite lozinku";
$mg2->lang['thissection']                         = "Ova sekcija je zasticena lozinkom";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Korijen";
$mg2->lang['thumb']                               = "Minijaturne";
$mg2->lang['dateadded']                           = "Datum unosa";
$mg2->lang['upload']                              = "Ubaciti slike";
$mg2->lang['import']                              = "Prebaciti unesene slike u";
$mg2->lang['newfolder']                           = "Novi folder";
$mg2->lang['viewgallery']                         = "Pogledati galeriju";
$mg2->lang['setup']                               = "Podesavanje";
$mg2->lang['logoff']                              = "Izlaz";
$mg2->lang['menutxt_upload']                      = "Upload";
$mg2->lang['menutxt_import']                      = "Uvoz";
$mg2->lang['menutxt_newfolder']                   = "Novi folder";
$mg2->lang['menutxt_viewgallery']                 = "Pogledati galeriju";
$mg2->lang['menutxt_setup']                       = "Podesavanje";
$mg2->lang['menutxt_logoff']                      = "Izlaz";
$mg2->lang['delete']                              = "Izbrisati";
$mg2->lang['cancel']                              = "Ponistiti";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Izbrisati folder";
$mg2->lang['navigation']                          = "Navigacija";
$mg2->lang['images']                              = "Slike";
$mg2->lang['filename']                            = "Ime slike";
$mg2->lang['title']                               = "Naslov";
$mg2->lang['description']                         = "Opis";
$mg2->lang['setasthumb']                          = "Podesiti kao sliku foldera";
$mg2->lang['editfolder']                          = "Promjeniti folder";
$mg2->lang['editimage']                           = "Promjenuti sliku";
$mg2->lang['nofolderselected']                    = "Nijedan folder nije izabran";
$mg2->lang['foldername']                          = "Ime foldera";
$mg2->lang['newpassword']                         = "Nova lozinka";
$mg2->lang['deletepassword']                      = "Izbrisi lozinku";
$mg2->lang['introtext']                           = "Intro tekst";
$mg2->lang['deletethumb']                         = "Izbrisati minijaturnu";
$mg2->lang['moveto']                              = "Prenjeti u";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Velicina fajla";
$mg2->lang['width']                               = "Sirina";
$mg2->lang['height']                              = "Visina";
$mg2->lang['date']                                = "Datum";
$mg2->lang['ascending']                           = "A prema Z";
$mg2->lang['descending']                          = "Z prema A";
$mg2->lang['newfolder']                           = "Kreiranje novog foldera";
$mg2->lang['password']                            = "Lozinka";
$mg2->lang['direction']                           = "Pravac klasiranja";
$mg2->lang['sortby']                              = "Klasirati po";
$mg2->lang['gallerytitle']                        = "Naslov galerije";
$mg2->lang['adminemail']                          = "Admin email";
$mg2->lang['language']                            = "Jezik";
$mg2->lang['skin']                                = "Izgled (skin)";
$mg2->lang['dateformat']                          = "Format datuma";
$mg2->lang['DDMMYY']                              = "DD MMM GGGG";
$mg2->lang['MMDDYY']                              = "MMM DD, GGGG";
$mg2->lang['MM.DD.YY']                            = "MM.DD.GG";
$mg2->lang['DD.MM.YY']                            = "DD.MM.GG";
$mg2->lang['YYYYMMDD']                            = "GGGGMMDD";
$mg2->lang['sendmail']                            = "Dozvoliti slanje komentara na vas e-mail";
$mg2->lang['foldericons']                         = "Dopustiti ikone za foldere";
$mg2->lang['showexif']                            = "Prikazati Exif";
$mg2->lang['allowcomments']                       = "Dozvoliti komentare";
$mg2->lang['copyright']                           = "Oznaka za copyright";
$mg2->lang['passwordchange']                      = "Promjeniti lozinku (3 x prazno = bez promjena)";
$mg2->lang['oldpasswordsetup']                    = "Trenutna lozinka";
$mg2->lang['newpasswordsetup']                    = "Nova lozinka (Prazno = koristiti trenutnu lozinku)";
$mg2->lang['newpasswordsetupconfirm']             = "Unesiti ponovo novu lozinku";
$mg2->lang['advanced']                            = "Opcije sireg karaktera";
$mg2->lang['allowedextensions']                   = "Dozvoliti slijedece fajlove";
$mg2->lang['imgwidth']                            = "Maksimalna sirina (0 = dezaktivirano)";
$mg2->lang['indexfile']                           = "index fajl galerije";
$mg2->lang['thumbquality']                        = "Kvalitet minijatrunih";
$mg2->lang['uploadimport']                        = "Ne zaboravite uvesti(import) slike nakon sto ih unsete u system!";
$mg2->lang['image']                               = "Slika";
$mg2->lang['edit']                                = "Izmjeniti";
$mg2->lang['editcurrentfolder']                   = "Promjeniti trenutni folder";
$mg2->lang['deletecurrentfolder']                 = "Izbrisati trenutni folder";
$mg2->lang['by']                                  = " ";
$mg2->lang['loginagain']                          = "Ponovo uci u zasticenu sekciju";
$mg2->lang['securitylogoff']                      = "Sigurnosni izlaz";
$mg2->lang['autologoff']                          = "Automatski ste izbaceni iz administracije nakon 15 minuta neaktivnosti.";
$mg2->lang['logoff']                              = "Izlaz";
$mg2->lang['forsecurity']                         = "Iz sigurnosnih razloga, savjetovano je da zatvorite ovaj prozor.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Vasa stranica je sata X dana. Klinki ovdje da provjerite ima li nova verzija!</a></b>";
$mg2->lang['updatesuccess']                       = "Update uspjesan";
$mg2->lang['renamefailure']                       = "ERROR: Ime fajla sadrzi ilegalne znakove ili slova!";
$mg2->lang['filedeleted']                         = "Fajl izbrisan";
$mg2->lang['filenotfound']                        = "Fajl nije nadjen!";
$mg2->lang['filesimported']                       = "Slika uvezeno";
$mg2->lang['nofilestoimport']                     = "ERROR: Nema slika za uvoz!";
$mg2->lang['foldernotempty']                      = "ERROR: Folder nije prazan!";
$mg2->lang['folderdeleted']                       = "Folder izbrisan";
$mg2->lang['folderupdated']                       = "Folder promjenjen";
$mg2->lang['foldercreated']                       = "Folder napravljen";
$mg2->lang['folderexists']                        = "ERROR: Folder postoji sa istim imenom";
$mg2->lang['filesuploaded']                       = "Slike unesene";
$mg2->lang['settingssaved']                       = "Podesavanje snimljeno";
$mg2->lang['nopwdmatch']                          = "Podesavanje snimljeno<br /><br />ERROR: Lozinka ne stima - nova lozinka nije registrovana!";
$mg2->lang['filesmovedto']                        = "slike prenesene u";
$mg2->lang['filesdeleted']                        = "slike izbrisane!";
$mg2->lang['file']                                = "fajl";
$mg2->lang['files']                               = "komada";
$mg2->lang['folder']                              = "folder";
$mg2->lang['folders']                             = "foldera";
$mg2->lang['rebuild']                             = "Rekonstrukcija";
$mg2->lang['rebuildimages']                       = "Rekonstrukcija minijaturnih";
$mg2->lang['rebuildsuccess']                      = "Rekonstrukcija uradjena";
$mg2->lang['donate']                              = "MG2 je besplatan software, pod GPL licencom. Ako ovaj program smatrate korisnim, molimo vas za mali gest koji cete uciniti donacijom autoru kliknuci na dugme ispod..";
$mg2->lang['from']                                = "Od";
$mg2->lang['comment']                             = "Komentar";
$mg2->lang['comments']                            = "Komentari";
$mg2->lang['by']                                  = " ";
$mg2->lang['commentsdeleted']                     = "Komentari izbrisani";
$mg2->lang['buttonmove']                          = "Prenijeti";
$mg2->lang['buttondelete']                        = "Izbrisati";
$mg2->lang['deleteconfirm']                       = "Izbrisati izabrane slike?";
$mg2->lang['imagecolumns']                        = "Broj kolona slika";
$mg2->lang['imagerows']                           = "Broj linija slika";
$mg2->lang['viewfolder']                          = "Pogledati folder";
$mg2->lang['viewimage']                           = "Pogledati sliku";
$mg2->lang['viewgallery']                         = "Pogledati galeriju";
$mg2->lang['rotateright']                         = "Okrenuti 90° desno";
$mg2->lang['rotateleft']                          = "Okrenuti 90° lijevo";
$mg2->lang['imagerotated']                        = "Slika okrenuta!";
$mg2->lang['gifnotrotated']                       = "ERROR: .GIF Slika nemoze biti okrenuta zbog ogranicenosti GD !";
$mg2->lang['help']                                = "Pomoc";
$mg2->lang['slideshowdelay']                      = "Slideshow ,edjuvrijeme";
$mg2->lang['websitelink']                         = "URL vase stranice (prazno = dezaktivirano)";
$mg2->lang['marknew']                             = "Obiljeziti slike novije od X dana (0 = dezaktivirano)";
$mg2->lang['folderempty']                         = "Ovaj folder je prazan";
$mg2->lang['noimage']                             = "Trazena slika ne postoji!";
?>
