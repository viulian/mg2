﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         German                                      //
//                                                                                     //
//                               TRANSLATED BY: insaneDeich                            //
//                               EMAIL: support@minigal.dk                             //
//                                                                                     //
//                               LAST UPDATED: 19. Feb 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galerie";
$mg2->lang['of']                                  = "von";
$mg2->lang['first']                               = "Erstes";
$mg2->lang['prev']                                = "Vorheriges";
$mg2->lang['next']                                = "N&auml;chstes";
$mg2->lang['last']                                = "Letztes";
$mg2->lang['thumbs']                              = "Thumbs";
$mg2->lang['exif info']                           = "Exif Informationen";
$mg2->lang['model']                               = "Modell";
$mg2->lang['shutter']                             = "Verschlussgeschwindigkeit";
$mg2->lang['viewslideshow']                       = "Starte Diashow";
$mg2->lang['stopslideshow']                       = "Stoppe Diashow";
$mg2->lang['aperture']                            = "Blende";
$mg2->lang['flash']                               = "Blitz";
$mg2->lang['focallength']                         = "Brennweite";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Belichtungsabgleich";
$mg2->lang['original']                            = "Original";
$mg2->lang['metering']                            = "Abmessung";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Seite";
$mg2->lang['all']                                 = "Alle";
$mg2->lang['fullsize']                            = "Volle Gr&ouml;ße";
$mg2->lang['addcomment']                          = "Kommentar hinzuf&uuml;gen";
$mg2->lang['name']                                = "Name";
$mg2->lang['email']                               = "e-Mail";
$mg2->lang['commentadded']                        = "Kommentar hinzugef&uuml;gt";
$mg2->lang['commentexists']                       = "FEHLER: Kommentar existiert bereits!";
$mg2->lang['commentmissing']                      = "FEHLER: Nicht alle Felder ausgef&uuml;llt!";
$mg2->lang['enterpassword']                       = "Geben Sie das Passwort ein";
$mg2->lang['thissection']                         = "Dieser Abschnitt ist passwortgesch&uuml;tzt";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Root";
$mg2->lang['thumb']                               = "Thumb";
$mg2->lang['dateadded']                           = "Hinzugef&uuml;gt am";
$mg2->lang['upload']                              = "Lade Dateien hoch";
$mg2->lang['import']                              = "Importiere Dateien nach";
$mg2->lang['newfolder']                           = "Neuer Ordner";
$mg2->lang['viewgallery']                         = "Betrachte Galerie";
$mg2->lang['setup']                               = "Einstellungen";
$mg2->lang['logoff']                              = "Abmelden";
$mg2->lang['menutxt_upload']                      = "Hochladen";
$mg2->lang['menutxt_import']                      = "Importieren";
$mg2->lang['menutxt_newfolder']                   = "Neuer Ordner";
$mg2->lang['menutxt_viewgallery']                 = "Betrachte Galerie";
$mg2->lang['menutxt_setup']                       = "Einstellungen";
$mg2->lang['menutxt_logoff']                      = "Abmelden";
$mg2->lang['delete']                              = "L&ouml;schen";
$mg2->lang['cancel']                              = "Abbrechen";
$mg2->lang['ok']                                  = "OK";
$mg2->lang['deletefolder']                        = "L&ouml;sche Ordner";
$mg2->lang['navigation']                          = "Navigation";
$mg2->lang['images']                              = "Bild(er)";
$mg2->lang['filename']                            = "Dateiname";
$mg2->lang['title']                               = "&Uuml;berschrift";
$mg2->lang['description']                         = "Beschreibung";
$mg2->lang['setasthumb']                          = "Setze als Ordnerthumb";
$mg2->lang['editfolder']                          = "Bearbeitete Ordner";
$mg2->lang['editimage']                           = "Bearbeite Bild";
$mg2->lang['nofolderselected']                    = "Kein Ordner ausgew&auml;hlt";
$mg2->lang['foldername']                          = "Ordnername";
$mg2->lang['newpassword']                         = "Neues Passwort";
$mg2->lang['deletepassword']                      = "L&ouml;sche Passwort";
$mg2->lang['introtext']                           = "Einleitungstext";
$mg2->lang['deletethumb']                         = "L&ouml;sche Thumb";
$mg2->lang['moveto']                              = "Verschiebe nach";
$mg2->lang['id']                                  = "ID";
$mg2->lang['filesize']                            = "Dateigr&ouml;ße";
$mg2->lang['width']                               = "Breite";
$mg2->lang['height']                              = "H&ouml;he";
$mg2->lang['date']                                = "Datum";
$mg2->lang['ascending']                           = "Aufsteigend";
$mg2->lang['descending']                          = "Absteigend";
$mg2->lang['newfolder']                           = "Neuer Ordner";
$mg2->lang['password']                            = "Passwort";
$mg2->lang['direction']                           = "Anordnung";
$mg2->lang['sortby']                              = "Sortiere nach";
$mg2->lang['gallerytitle']                        = "Galerie&uuml;berschrift";
$mg2->lang['adminemail']                          = "Administrator e-Mail";
$mg2->lang['language']                            = "Sprache";
$mg2->lang['skin']                                = "Aussehen";
$mg2->lang['dateformat']                          = "Datumsformat";
$mg2->lang['DDMMYY']                              = "DD MMM YYYY";
$mg2->lang['MMDDYY']                              = "MMM DD, YYYY";
$mg2->lang['MM.DD.YY']                            = "MM.DD.YY";
$mg2->lang['DD.MM.YY']                            = "DD.MM.YY";
$mg2->lang['YYYYMMDD']                            = "YYYYMMDD";
$mg2->lang['sendmail']                            = "Versende e-Mail bei Kommentaren";
$mg2->lang['foldericons']                         = "Erzwinge Ordnericons";
$mg2->lang['showexif']                            = "Zeige Exif";
$mg2->lang['allowcomments']                       = "Kommentare zugelassen";
$mg2->lang['copyright']                           = "Urheberrechtshinweis";
$mg2->lang['passwordchange']                      = "&Auml;ndere Passwort (3 x leer = keine &Auml;nderung)";
$mg2->lang['oldpasswordsetup']                    = "Derzeitiges Passwort";
$mg2->lang['newpasswordsetup']                    = "Neues Passwort (leer = benutzte gegenwärtiges)";
$mg2->lang['newpasswordsetupconfirm']             = "Neues Passwort";
$mg2->lang['advanced']                            = "Erweiterte Einstellungen";
$mg2->lang['allowedextensions']                   = "Erlaube Dateierweiterungen";
$mg2->lang['imgwidth']                            = "maximale Bildbreite (0 = inaktiv)";
$mg2->lang['indexfile']                           = "Galerie Indexdatei";
$mg2->lang['thumbquality']                        = "Thumbnail Qualit&auml;t";
$mg2->lang['uploadimport']                        = "Vergessen Sie nicht ihre Bilder nach dem Hochladen auch zu importieren!";
$mg2->lang['image']                               = "Bild";
$mg2->lang['edit']                                = "Editieren";
$mg2->lang['editcurrentfolder']                   = "Editiere aktuellen Ordner";
$mg2->lang['deletecurrentfolder']                 = "L&ouml;sche aktuellen Ordner";
$mg2->lang['by']                                  = "von";
$mg2->lang['loginagain']                          = "Neu anmelden";
$mg2->lang['securitylogoff']                      = "Sicherheitsabmeldung";
$mg2->lang['autologoff']                          = "Sie wurden automatisch nach 15 Minuten Inaktivit&auml;t abgemeldet.";
$mg2->lang['logoff']                              = "Abmelden";
$mg2->lang['forsecurity']                         = "Aus Sicherheitsgr&uuml;nden wird empfohlen dieses Browserfenster zu schliessen!";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Diese Installation ist X Tage alt. Klicken sie hier um eine neuere Version zu installieren!</a></b>";
$mg2->lang['updatesuccess']                       = "Update erfolgreich";
$mg2->lang['renamefailure']                       = "Fehler: Dateiname enth&auml;lt verbotene Zeichen!";
$mg2->lang['filedeleted']                         = "Datei gel&ouml;scht";
$mg2->lang['filenotfound']                        = "Datei nicht gefunden!";
$mg2->lang['filesimported']                       = "Datei(en) importiert";
$mg2->lang['nofilestoimport']                     = "FEHLER: Keine Dateien zum importieren!";
$mg2->lang['foldernotempty']                      = "FEHLER: Ordner nicht leer!";
$mg2->lang['folderdeleted']                       = "Ordner gel&ouml;scht";
$mg2->lang['folderupdated']                       = "Ordner aktualisiert";
$mg2->lang['foldercreated']                       = "Ordner erstellt";
$mg2->lang['folderexists']                        = "FEHLER: Ordnername existiert bereits!";
$mg2->lang['filesuploaded']                       = "Datei(en) hochgeladen";
$mg2->lang['settingssaved']                       = "Einstellungen gespeichert";
$mg2->lang['nopwdmatch']                          = "Einstellungen gespeichert<br /><br />FEHLER: Passwortabweichung! - Neues Passwort wurde nicht gespeichert!";
$mg2->lang['filesmovedto']                        = "Datei(en) verschoben nach";
$mg2->lang['filesdeleted']                        = "Datei(en) gel&ouml;scht!";
$mg2->lang['file']                                = "Datei";
$mg2->lang['files']                               = "Dateien";
$mg2->lang['folder']                              = "Ordner";
$mg2->lang['folders']                             = "Ordner";
$mg2->lang['rebuild']                             = "Erneuern";
$mg2->lang['rebuildimages']                       = "Erneuere Thumbnails";
$mg2->lang['rebuildsuccess']                      = "Erneuerung durchgef&uuml;hrt";
$mg2->lang['donate']                              = "MG2 ist kostenlose Software unter GPL-Lizenz. Sollten Sie diese Software n&uuml;tzlich finden, so spenden sie bitte an den Autor, indem sie auf untenstehenden Link klciken.";
$mg2->lang['from']                                = "Von";
$mg2->lang['comment']                             = "Kommentar";
$mg2->lang['comments']                            = "Kommentare";
$mg2->lang['by']                                  = "von";
$mg2->lang['commentsdeleted']                     = "Kommentar(e) gel&ouml;scht";
$mg2->lang['buttonmove']                          = "Verschiebe";
$mg2->lang['buttondelete']                        = "L&ouml;sche";
$mg2->lang['deleteconfirm']                       = "Ausgew&auml;hlte Dateien l&ouml;schen?";
$mg2->lang['imagecolumns']                        = "Bilderspalten";
$mg2->lang['imagerows']                           = "Bilderzeilen";
$mg2->lang['viewfolder']                          = "Betrachte Ordner";
$mg2->lang['viewimage']                           = "Betrachte Bild";
$mg2->lang['viewgallery']                         = "Betrachte Galerie";
$mg2->lang['rotateright']                         = "Drehe um 90° nach rechts";
$mg2->lang['rotateleft']                          = "Drehe um 90° nach links";
$mg2->lang['imagerotated']                        = "Bild gedreht!";
$mg2->lang['gifnotrotated']                       = "FEHLER: .GIF-Dateien k&ouml;nnen wegen Einschr&auml;nkungen in der GD-Bibliothek nicht gedreht werden!";
$mg2->lang['help']                                = "Hilfe";
?>
