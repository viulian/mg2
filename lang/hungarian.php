﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Hungarian                                   //
//                                                                                     //
//                               TRANSLATED BY: Mark Gabor                             //
//                               EMAIL: drax2gma@freemail.hu                           //
//                                                                                     //
//                               LAST UPDATED: 28. Feb 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galéria";
$mg2->lang['of']                                  = "/";
$mg2->lang['first']                               = "Első";
$mg2->lang['prev']                                = "Előző";
$mg2->lang['next']                                = "Következő";
$mg2->lang['last']                                = "Utolsó";
$mg2->lang['thumbs']                              = "Előnézet";
$mg2->lang['exif info']                           = "Exif Információ";
$mg2->lang['model']                               = "Géptípus";
$mg2->lang['shutter']                             = "Zársebesség";
$mg2->lang['viewslideshow']                       = "Diavetítés";
$mg2->lang['stopslideshow']                       = "Diavetítés vége";
$mg2->lang['aperture']                            = "Rekeszérték";
$mg2->lang['flash']                               = "Vaku";
$mg2->lang['focallength']                         = "Fókusztávolság";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Expozíció-kompenzáció";
$mg2->lang['original']                            = "Eredeti";
$mg2->lang['metering']                            = "Fénymérés";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Oldal";
$mg2->lang['all']                                 = "Mind";
$mg2->lang['fullsize']                            = "Teljeskép-nézet";
$mg2->lang['addcomment']                          = "Hozzászólás írása";
$mg2->lang['name']                                = "Név";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Hozzászólás elküldve";
$mg2->lang['commentexists']                       = "HIBA: A hozzászólás már létezik!";
$mg2->lang['commentmissing']                      = "HIBA: Minden mező kitöltése kötelező!";
$mg2->lang['enterpassword']                       = "Kérem a jelszót";
$mg2->lang['thissection']                         = "Ez a terület jelszóval védett";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Gyökér";
$mg2->lang['thumb']                               = "Előnézet";
$mg2->lang['dateadded']                           = "Dátum";
$mg2->lang['upload']                              = "Fájlok feltöltése";
$mg2->lang['import']                              = "Fájlok importálása ide";
$mg2->lang['newfolder']                           = "Új mappa";
$mg2->lang['viewgallery']                         = "Galéria megtekintése";
$mg2->lang['setup']                               = "Beállítások";
$mg2->lang['logoff']                              = "Kijelentkezés";
$mg2->lang['menutxt_upload']                      = "Feltöltés";
$mg2->lang['menutxt_import']                      = "Importálás";
$mg2->lang['menutxt_newfolder']                   = "Új mappa";
$mg2->lang['menutxt_viewgallery']                 = "Galéria megtekintése";
$mg2->lang['menutxt_setup']                       = "Beállítások";
$mg2->lang['menutxt_logoff']                      = "Kijelentkezés";
$mg2->lang['delete']                              = "Törlés";
$mg2->lang['cancel']                              = "Mégsem";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Mappa törlése";
$mg2->lang['navigation']                          = "Navigáció";
$mg2->lang['images']                              = "kép(ek)";
$mg2->lang['filename']                            = "Fájlnév";
$mg2->lang['title']                               = "Cím";
$mg2->lang['description']                         = "Leírás";
$mg2->lang['setasthumb']                          = "Beállítás mappa-képnek";
$mg2->lang['editfolder']                          = "Mappa szerkesztése";
$mg2->lang['editimage']                           = "Kép szerkesztése";
$mg2->lang['nofolderselected']                    = "Nincs mappa kiválasztva";
$mg2->lang['foldername']                          = "Mappa neve";
$mg2->lang['newpassword']                         = "Új jelszó";
$mg2->lang['deletepassword']                      = "Jelszó törlése";
$mg2->lang['introtext']                           = "Bevezető szöveg";
$mg2->lang['deletethumb']                         = "Előnézeti kép törlése";
$mg2->lang['moveto']                              = "Áthelyezés ide";
$mg2->lang['id']                                  = "Azonosító";
$mg2->lang['filesize']                            = "Fájlméret";
$mg2->lang['width']                               = "Szélesség";
$mg2->lang['height']                              = "Magasság";
$mg2->lang['date']                                = "Dátum";
$mg2->lang['ascending']                           = "Növekvő";
$mg2->lang['descending']                          = "Csökkenő";
$mg2->lang['newfolder']                           = "Új mappa";
$mg2->lang['password']                            = "Jelszó";
$mg2->lang['direction']                           = "Irány";
$mg2->lang['sortby']                              = "Rendezés";
$mg2->lang['gallerytitle']                        = "Galéria címe";
$mg2->lang['adminemail']                          = "Admin email";
$mg2->lang['language']                            = "Nyelv";
$mg2->lang['skin']                                = "Kinézet";
$mg2->lang['dateformat']                          = "Dátumformátum";
$mg2->lang['DDMMYY']                              = "NN HHH ÉÉÉÉ";
$mg2->lang['MMDDYY']                              = "HHH NN, ÉÉÉÉ";
$mg2->lang['MM.DD.YY']                            = "HH.NN.ÉÉ";
$mg2->lang['DD.MM.YY']                            = "NN.HH.ÉÉ";
$mg2->lang['YYYYMMDD']                            = "ÉÉÉÉHHNN";
$mg2->lang['sendmail']                            = "Hozzászólások küldése e-mailben";
$mg2->lang['foldericons']                         = "Mappa ikonok kikényszerítése";
$mg2->lang['showexif']                            = "Exif mutatása";
$mg2->lang['allowcomments']                       = "Megjegyzések engedélyezése";
$mg2->lang['copyright']                           = "Szerzői jogi információ";
$mg2->lang['passwordchange']                      = "Jelszó változtatása (3 x üres = marad a régi)";
$mg2->lang['oldpasswordsetup']                    = "Jelenlegi jelszó";
$mg2->lang['newpasswordsetup']                    = "Új jelszó (üres = marad a régi)";
$mg2->lang['newpasswordsetupconfirm']             = "Új jelszó mégegyszer";
$mg2->lang['advanced']                            = "Haladó";
$mg2->lang['allowedextensions']                   = "Engedélyezett kiterjesztések";
$mg2->lang['imgwidth']                            = "Max. képszélesség (0 = letiltva)";
$mg2->lang['indexfile']                           = "Galéria index fájl";
$mg2->lang['thumbquality']                        = "Előnézeti kép minőség";
$mg2->lang['uploadimport']                        = "Feltöltés után a képeket be is kell importálni!";
$mg2->lang['image']                               = "Kép";
$mg2->lang['edit']                                = "Szerkesztés";
$mg2->lang['editcurrentfolder']                   = "Aktuális mappa szerkesztése";
$mg2->lang['deletecurrentfolder']                 = "Aktuális mappa törlése";
$mg2->lang['by']                                  = "";
$mg2->lang['loginagain']                          = "Újra belépés";
$mg2->lang['securitylogoff']                      = "Biztonsági kiléptetés";
$mg2->lang['autologoff']                          = "Ön automatikusan ki lett léptetve 15 perc inaktivitás után.";
$mg2->lang['logoff']                              = "Kijelentezés";
$mg2->lang['forsecurity']                         = "Binztonsági okokból kilépés után célszerű a böngészőablakot bezárni.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Ez az installáció X napos. Ellenőrizze az esetleges frissítéseket itt!</a></b>";
$mg2->lang['updatesuccess']                       = "Frissítés sikeres volt";
$mg2->lang['renamefailure']                       = "HIBA: A fájlnév nem megengedett karaktereket is tartalmaz!";
$mg2->lang['filedeleted']                         = "A fájl törölve";
$mg2->lang['filenotfound']                        = "A fájl nem található!";
$mg2->lang['filesimported']                       = "fájl importálva";
$mg2->lang['nofilestoimport']                     = "HIBA: Nincs fájl kijelölve importálásra!";
$mg2->lang['foldernotempty']                      = "HIBA: Mappa nem üres!";
$mg2->lang['folderdeleted']                       = "Mappa törölve";
$mg2->lang['folderupdated']                       = "Mappa frissítve";
$mg2->lang['foldercreated']                       = "Mappa létrehozva";
$mg2->lang['folderexists']                        = "HIBA: Létező mappanév!";
$mg2->lang['filesuploaded']                       = "Fájl(ok) feltöltve";
$mg2->lang['settingssaved']                       = "Beállítások mentve";
$mg2->lang['nopwdmatch']                          = "Beállítások mentve<br /><br />HIBA: Jelszó nem egyezik - új jelszó nincs mentve!";
$mg2->lang['filesmovedto']                        = "fájl(ok) mozgatva ide";
$mg2->lang['filesdeleted']                        = "fájl(ok) törölve!";
$mg2->lang['file']                                = "fájl";
$mg2->lang['files']                               = "fájl";
$mg2->lang['folder']                              = "mappa";
$mg2->lang['folders']                             = "mappa";
$mg2->lang['rebuild']                             = "Újragenerálás";
$mg2->lang['rebuildimages']                       = "Előnézet újragenerálása";
$mg2->lang['rebuildsuccess']                      = "Újragenerálás kész";
$mg2->lang['donate']                              = "MG2 egy szabad szoftver, a GPL alatt licenszelve. Ha hasznosnak találja ezt a szoftvert, adományt küldhet a szoftver fejlesztőjének a lenti gombot megnyomva.";
$mg2->lang['from']                                = "Kitől";
$mg2->lang['comment']                             = "Megjegyzés";
$mg2->lang['comments']                            = "Megjegyzések";
$mg2->lang['by']                                  = "";
$mg2->lang['commentsdeleted']                     = "Megjegyzés(ek) törölve";
$mg2->lang['buttonmove']                          = "Áthelyezés";
$mg2->lang['buttondelete']                        = "Törlés";
$mg2->lang['deleteconfirm']                       = "Törli a kijelölt fájlokat?";
$mg2->lang['imagecolumns']                        = "Képek oszlopainak száma";
$mg2->lang['imagerows']                           = "Képek sorainak száma";
$mg2->lang['viewfolder']                          = "Mappa megtekintése";
$mg2->lang['viewimage']                           = "Kép megtekintése";
$mg2->lang['viewgallery']                         = "Galéria megtekintése";
$mg2->lang['rotateright']                         = "Jobbra fordítás 90 fokkal";
$mg2->lang['rotateleft']                          = "Balra fordítás 90 fokkal";
$mg2->lang['imagerotated']                        = "Kép elforgatva!";
$mg2->lang['gifnotrotated']                       = "HIBA: .GIF fájlok nem forgathatóak a GD függvénykönyvtár limitált képességei miatt!";
$mg2->lang['help']                                = "Súgó";
?>
