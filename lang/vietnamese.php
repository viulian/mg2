<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.12c10.net                              //
//                                                                                     //
//                                         Tiếng Việt                                     //
//                                                                                     //
//                               TRANSLATED BY: TPhongTran                         //
//                               EMAIL: tieuphongtran@gmail.com                             //
//                                                                                     //
//                               LAST UPDATED: 29. May 2009                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Quay lại thư viện hình";
$mg2->lang['of']                                  = "của";
$mg2->lang['first']                               = "Đầu tiên";
$mg2->lang['prev']                                = "Lùi lại";
$mg2->lang['next']                                = "Tiếp theo";
$mg2->lang['last']                                = "Cuối cùng";
$mg2->lang['thumbs']                              = "Thumbs";
$mg2->lang['exif info']                           = "Exif Information";
$mg2->lang['model']                               = "Model";
$mg2->lang['shutter']                             = "Shutter speed";
$mg2->lang['viewslideshow']                       = "Xem trình chiếu";
$mg2->lang['stopslideshow']                       = "Stop slideshow";
$mg2->lang['aperture']                            = "Aperture";
$mg2->lang['flash']                               = "Flash";
$mg2->lang['focallength']                         = "Focal length";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Exposure compensation";
$mg2->lang['original']                            = "Original";
$mg2->lang['metering']                            = "Metering";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Trang";
$mg2->lang['all']                                 = "Tất cả";
$mg2->lang['fullsize']                            = "View fullsize image";
$mg2->lang['addcomment']                          = "Thêm nhận xét (bắt buộc phải nhập hết tất cả các mục)";
$mg2->lang['name']                                = "Tên";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Nhận xét đã được thêm";
$mg2->lang['commentexists']                       = "ERROR: Comment already exists!";
$mg2->lang['commentmissing']                      = "ERROR: All comment fields must be filled!";
$mg2->lang['enterpassword']                       = "Enter password";
$mg2->lang['thissection']                         = "This section is password protected";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Rỗng";
$mg2->lang['thumb']                               = "Thumb";
$mg2->lang['dateadded']                           = "Date added";
$mg2->lang['upload']                              = "Đưa hình lên";
$mg2->lang['import']                              = "Chèn tệp đã tải lên đến";
$mg2->lang['newfolder']                           = "Thư mục mới";
$mg2->lang['viewgallery']                         = "View gallery";
$mg2->lang['setup']                               = "Cài đặt";
$mg2->lang['logoff']                              = "Đăng xuất";
$mg2->lang['menutxt_upload']                      = "Đưa hình lên mạng";
$mg2->lang['menutxt_import']                      = "Import";
$mg2->lang['menutxt_newfolder']                   = "Thư mục mới";
$mg2->lang['menutxt_viewgallery']                 = "View gallery";
$mg2->lang['menutxt_setup']                       = "Cài đặt";
$mg2->lang['menutxt_logoff']                      = "Logoff";
$mg2->lang['delete']                              = "Xóa";
$mg2->lang['cancel']                              = "Hủy";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Delete folder";
$mg2->lang['navigation']                          = "Navigation";
$mg2->lang['images']                              = "image(s)";
$mg2->lang['filename']                            = "Filename";
$mg2->lang['title']                               = "Title";
$mg2->lang['description']                         = "Description";
$mg2->lang['setasthumb']                          = "Set as folder thumb";
$mg2->lang['editfolder']                          = "Edit folder";
$mg2->lang['editimage']                           = "Edit image";
$mg2->lang['nofolderselected']                    = "No folder selected";
$mg2->lang['foldername']                          = "Folder name";
$mg2->lang['newpassword']                         = "New password";
$mg2->lang['deletepassword']                      = "Delete password";
$mg2->lang['introtext']                           = "Intro text";
$mg2->lang['deletethumb']                         = "Delete thumb";
$mg2->lang['moveto']                              = "Move to";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Filesize";
$mg2->lang['width']                               = "Width";
$mg2->lang['height']                              = "Height";
$mg2->lang['date']                                = "Date";
$mg2->lang['ascending']                           = "Ascending";
$mg2->lang['descending']                          = "Descending";
$mg2->lang['newfolder']                           = "New folder";
$mg2->lang['password']                            = "Password";
$mg2->lang['direction']                           = "Direction";
$mg2->lang['sortby']                              = "Sort by";
$mg2->lang['gallerytitle']                        = "Gallery title";
$mg2->lang['adminemail']                          = "Admin email";
$mg2->lang['language']                            = "Language";
$mg2->lang['skin']                                = "Skin";
$mg2->lang['dateformat']                          = "Date format";
$mg2->lang['DDMMYY']                              = "DD MMM YYYY";
$mg2->lang['MMDDYY']                              = "MMM DD, YYYY";
$mg2->lang['MM.DD.YY']                            = "MM.DD.YY";
$mg2->lang['DD.MM.YY']                            = "DD.MM.YY";
$mg2->lang['YYYYMMDD']                            = "YYYYMMDD";
$mg2->lang['sendmail']                            = "Send comment emails";
$mg2->lang['foldericons']                         = "Force folder icons";
$mg2->lang['showexif']                            = "Show Exif";
$mg2->lang['allowcomments']                       = "Allow comments";
$mg2->lang['copyright']                           = "Copyright notice";
$mg2->lang['passwordchange']                      = "Change password (3 x blank = keep current)";
$mg2->lang['oldpasswordsetup']                    = "Nhập mật khẩu cũ";
$mg2->lang['newpasswordsetup']                    = "Mật khẩu mới (để trống = sử dụng mật khẩu cũ)";
$mg2->lang['newpasswordsetupconfirm']             = "Nhập lại mật khẩu mới";
$mg2->lang['advanced']                            = "Chức năng nâng cao";
$mg2->lang['allowedextensions']                   = "Allowed extensions";
$mg2->lang['imgwidth']                            = "Max. image width (0 = disable)";
$mg2->lang['indexfile']                           = "Gallery index file";
$mg2->lang['thumbquality']                        = "Thumbnail quality";
$mg2->lang['image']                               = "Hình";
$mg2->lang['edit']                                = "Sửa";
$mg2->lang['editcurrentfolder']                   = "Sửa thư mục đã có";
$mg2->lang['deletecurrentfolder']                 = "Xóa thư mục đã có";
$mg2->lang['by']                                  = "bởi";
$mg2->lang['loginagain']                          = "Đăng nhập lại";
$mg2->lang['securitylogoff']                      = "Khóa bảo mật";
$mg2->lang['autologoff']                          = "Bạn đã bị tự động khóa đăng nhập sau 15 phút không truy vấn hoạt động.";
$mg2->lang['logoff']                              = "Khóa đăng nhập";
$mg2->lang['forsecurity']                         = "For security reasons, it is recommended to close this browser window.";
$mg2->lang['updatesuccess']                       = "Update successful";
$mg2->lang['renamefailure']                       = "ERROR: Filename contains illegal characters!";
$mg2->lang['filedeleted']                         = "Tệp được đã xóa";
$mg2->lang['filenotfound']                        = "Không tìm thấy tệp!";
$mg2->lang['filesimported']                       = "file(s) imported";
$mg2->lang['nofilestoimport']                     = "LỖI: Không có tệp nào để nhập!";
$mg2->lang['foldernotempty']                      = "ERROR: Folder not empty!";
$mg2->lang['folderdeleted']                       = "Thư mục đã được xóa";
$mg2->lang['folderupdated']                       = "Đã cập nhật thư mục";
$mg2->lang['foldercreated']                       = "Thư mục đã được tạo";
$mg2->lang['folderexists']                        = "Lỗi: Tên thư mục đã có!";
$mg2->lang['filesuploaded']                       = "File(s) uploaded - Importing...";
$mg2->lang['settingssaved']                       = "Đã lưu lại cài đặt";
$mg2->lang['nopwdmatch']                          = "Settings saved<br /><br />ERROR: Password mismatch - new password not saved!";
$mg2->lang['filesmovedto']                        = "file(s) moved to";
$mg2->lang['filesdeleted']                        = "file(s) deleted!";
$mg2->lang['file']                                = "file";
$mg2->lang['files']                               = "files";
$mg2->lang['folder']                              = "folder";
$mg2->lang['folders']                             = "folders";
$mg2->lang['rebuild']                             = "Rebuild";
$mg2->lang['rebuildimages']                       = "Rebuild thumbnails";
$mg2->lang['rebuildsuccess']                      = "Rebuild completed";
$mg2->lang['donate']                              = "MG2 là phần mền miễn phí, sử dụng giấy phép của GPL. If you find this software useful, please make a donation to the author by pressing the button below.";
$mg2->lang['from']                                = "Từ";
$mg2->lang['comment']                             = "Nhận xét";
$mg2->lang['comments']                            = "Những nhận xét";
$mg2->lang['by']                                  = "bởi";
$mg2->lang['commentsdeleted']                     = "Nhận xét đã được xóa";
$mg2->lang['buttonmove']                          = "Di chuyển";
$mg2->lang['buttondelete']                        = "Xóa";
$mg2->lang['deleteconfirm']                       = "Xóa tệp đã chọn?";
$mg2->lang['imagecolumns']                        = "Số hình trong 1 cột";
$mg2->lang['imagerows']                           = "Số hình trong 1 hàng";
$mg2->lang['viewfolder']                          = "Xem thư mục";
$mg2->lang['viewimage']                           = "Xem hình";
$mg2->lang['viewgallery']                         = "Xem thư viện hình";
$mg2->lang['rotateright']                         = "Rotate 90 degrees right";
$mg2->lang['rotateleft']                          = "Rotate 90 degrees left";
$mg2->lang['imagerotated']                        = "Image rotated!";
$mg2->lang['gifnotrotated']                       = "ERROR: .GIF files can't be rotated due to limitations in GD lib!";
$mg2->lang['help']                                = "Help";
$mg2->lang['slideshowdelay']                      = "Slideshow delay";
$mg2->lang['websitelink']                         = "Website link (blank = disable)";
$mg2->lang['marknew']                             = "Mark items newer than X days (0 = disable)";
$mg2->lang['folderempty']                         = "This folder is empty";
$mg2->lang['noimage']                             = "The requested image does not exist!";

// ----8<--------------------------------
// INSTRUCTIONS 0.5.0
//
// 1. Remove the following lines from above:
// $mg2->lang['uploadimport']
// $mg2->lang['upgradenote']
//
// 2. Update the translations for the following strings:
// $mg2->lang['filesuploaded']
//
// 3. Add the lines below into your existing translation
// 4. Translate the lines you just inserted into your own language file
// 5. Remove this comment
// 6. Update the 'last updated' comment in beginning of file
// 7. Upload the new language file to the addon directory (www.minigal.dk/login.php)
//
// (If you don't have a login, send a mail to support@minigal.dk)
// ----8<--------------------------------

$mg2->lang['actions']                             = "Hành động";
$mg2->lang['backupcomplete']                      = "Database backup complete";
$mg2->lang['backuplink']                          = "Make database backup";
$mg2->lang['viewlogfile']                         = "View logfile";
$mg2->lang['website']                             = "To website";
$mg2->lang['backtofolder']                        = "Trở lại thư mục";
$mg2->lang['permerror1']                          = "PERMISSION ERROR: Cannot write to gallery root folder!";
$mg2->lang['whattodo1']                           = "Chmod your gallery folder to 777";
$mg2->lang['permerror2']                          = "PERMISSION ERROR: Cannot write to 'pictures' folder!";
$mg2->lang['whattodo2']                           = "Chmod your 'pictures' folder to 777";
$mg2->lang['permerror3']                          = "PERMISSION ERROR: Cannot write to 'mg2db_idatabase.php' file!";
$mg2->lang['whattodo3']                           = "Chmod 'mg2db_idatabase.php' file to 777";
$mg2->lang['permerror4']                          = "PERMISSION ERROR: Cannot write to 'mg2db_idatabase_temp.php' file!";
$mg2->lang['whattodo4']                           = "Chmod 'mg2db_idatabase_temp.php' file to 777";
$mg2->lang['permerror5']                          = "PERMISSION ERROR: Cannot write to 'mg2db_fdatabase.php' file!";
$mg2->lang['whattodo5']                           = "Định dạng lại tệp 'mg2db_fdatabase.php' thành 777";
$mg2->lang['permerror6']                          = "PERMISSION ERROR: Không thể viết lên tệp Cơ sở dữ liệu 'mg2db_fdatabase_temp.php' !";
$mg2->lang['whattodo6']                           = "Định dạng lại tệp 'mg2db_fdatabase_temp.php' thành 777";

?>
