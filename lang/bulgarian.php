﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                        Bulgarian                                    //
//                                                                                     //
//                               TRANSLATED BY: Shark6286 (Shark6286)                  //
//                               EMAIL: support@minigal.dk                             //
//                                                                                     //
//                               LAST UPDATED: 09. Apr 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Галерия";
$mg2->lang['of']                                  = "от";
$mg2->lang['first']                               = "Първа";
$mg2->lang['prev']                                = "Предишна";
$mg2->lang['next']                                = "Следваща";
$mg2->lang['last']                                = "Последна";
$mg2->lang['thumbs']                              = "Иконки";
$mg2->lang['exif info']                           = "Exif информация";
$mg2->lang['model']                               = "Модел";
$mg2->lang['shutter']                             = "Време за експониране";
$mg2->lang['viewslideshow']                       = "Стартирай slideshow";
$mg2->lang['stopslideshow']                       = "Спри slideshow";
$mg2->lang['aperture']                            = "Бленда";
$mg2->lang['flash']                               = "Светкавица";
$mg2->lang['focallength']                         = "Фокусно разстояние";
$mg2->lang['mm']                                  = "мм";
$mg2->lang['exposurecomp']                        = "Компенсация на експонирането";
$mg2->lang['original']                            = "Оригинал";
$mg2->lang['metering']                            = "Metering";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "сек.";
$mg2->lang['page']                                = "Страница";
$mg2->lang['all']                                 = "Всички";
$mg2->lang['fullsize']                            = "Пълен размер на изображението";
$mg2->lang['addcomment']                          = "Добави коментар";
$mg2->lang['name']                                = "Име";
$mg2->lang['email']                               = "Имейл";
$mg2->lang['commentadded']                        = "Коментара е добавен";
$mg2->lang['commentexists']                       = "ГРЕШКА: Коментарът вече съществува!";
$mg2->lang['commentmissing']                      = "ГРЕШКА: Всички полета за коментар са задължителни!";
$mg2->lang['enterpassword']                       = "Въведете парола";
$mg2->lang['thissection']                         = "Тази секция е защитена с парола";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Root";
$mg2->lang['thumb']                               = "Иконка";
$mg2->lang['dateadded']                           = "Дата на добавяне";
$mg2->lang['upload']                              = "Зареждане на файлове";
$mg2->lang['import']                              = "Импорт на заредените файлове в";
$mg2->lang['newfolder']                           = "Нова папка";
$mg2->lang['viewgallery']                         = "Покажи галерията";
$mg2->lang['setup']                               = "Настройки";
$mg2->lang['logoff']                              = "Изход";
$mg2->lang['menutxt_upload']                      = "Зареждане";
$mg2->lang['menutxt_import']                      = "Импорт";
$mg2->lang['menutxt_newfolder']                   = "Нова папка";
$mg2->lang['menutxt_viewgallery']                 = "Покажи галерията";
$mg2->lang['menutxt_setup']                       = "Настройки";
$mg2->lang['menutxt_logoff']                      = "Изход";
$mg2->lang['delete']                              = "Изтрий";
$mg2->lang['cancel']                              = "Отказ";
$mg2->lang['ok']                                  = "Потвърди";
$mg2->lang['deletefolder']                        = "Изтрий папка";
$mg2->lang['navigation']                          = "Навигация";
$mg2->lang['images']                              = "изображение(я)";
$mg2->lang['filename']                            = "Име на файла";
$mg2->lang['title']                               = "Заглавие";
$mg2->lang['description']                         = "Описание";
$mg2->lang['setasthumb']                          = "Избери като иконка за папка";
$mg2->lang['editfolder']                          = "Редакция на папка";
$mg2->lang['editimage']                           = "Редакция на изображение";
$mg2->lang['nofolderselected']                    = "Не е избрана папка";
$mg2->lang['foldername']                          = "Име на папка";
$mg2->lang['newpassword']                         = "Нова парола";
$mg2->lang['deletepassword']                      = "Изтрий паролата";
$mg2->lang['introtext']                           = "Въвеждащ текст";
$mg2->lang['deletethumb']                         = "Изтрий иконката";
$mg2->lang['moveto']                              = "Премести в";
$mg2->lang['id']                                  = "Идентификатор";
$mg2->lang['filesize']                            = "Размер на файла";
$mg2->lang['width']                               = "Ширина";
$mg2->lang['height']                              = "Височина";
$mg2->lang['date']                                = "Дата";
$mg2->lang['ascending']                           = "По нарастване";
$mg2->lang['descending']                          = "По намаляване";
$mg2->lang['newfolder']                           = "Нова папка";
$mg2->lang['password']                            = "Парола";
$mg2->lang['direction']                           = "Посока";
$mg2->lang['sortby']                              = "Сортиране по";
$mg2->lang['gallerytitle']                        = "Име на галерията";
$mg2->lang['adminemail']                          = "Имейл на администратора";
$mg2->lang['language']                            = "Език";
$mg2->lang['skin']                                = "Скин";
$mg2->lang['dateformat']                          = "Формат на датата";
$mg2->lang['DDMMYY']                              = "ДД МММ ГГГГ";
$mg2->lang['MMDDYY']                              = "МММ ДД, ГГГГ";
$mg2->lang['MM.DD.YY']                            = "ММ.ММ.ГГ";
$mg2->lang['DD.MM.YY']                            = "ДД.ММ.ГГ";
$mg2->lang['YYYYMMDD']                            = "ГГГГММДД";
$mg2->lang['sendmail']                            = "Изпрати коментар по имейл";
$mg2->lang['foldericons']                         = "Принудителен избор на иконка за папките";
$mg2->lang['showexif']                            = "Покажи Exif";
$mg2->lang['allowcomments']                       = "Разреши коментарите";
$mg2->lang['copyright']                           = "Авторски права";
$mg2->lang['passwordchange']                      = "Промени паролата (3 x празно = запази текущата)";
$mg2->lang['oldpasswordsetup']                    = "Въведи текущата парола";
$mg2->lang['newpasswordsetup']                    = "Нова парола (празно = използвай текущата)";
$mg2->lang['newpasswordsetupconfirm']             = "Въведете новата парола отново";
$mg2->lang['advanced']                            = "Специални настройки";
$mg2->lang['allowedextensions']                   = "Разрешени файлови разширения";
$mg2->lang['imgwidth']                            = "Max. ширина на изображението (0 = няма)";
$mg2->lang['indexfile']                           = "Начален файл на галерията";
$mg2->lang['thumbquality']                        = "Качество на иконките";
$mg2->lang['uploadimport']                        = "Не забравяйте да импортнете изображенията след зареждането им!";
$mg2->lang['image']                               = "Изображение";
$mg2->lang['edit']                                = "Редактиране";
$mg2->lang['editcurrentfolder']                   = "Редактиране на текущата папка";
$mg2->lang['deletecurrentfolder']                 = "Изтриване на текущата папка";
$mg2->lang['by']                                  = "от";
$mg2->lang['loginagain']                          = "Влезте отново";
$mg2->lang['securitylogoff']                      = "Изход от системата";
$mg2->lang['autologoff']                          = "Вие автоматично излязохте от системата след 15 минути бездействие.";
$mg2->lang['logoff']                              = "Изход от системата";
$mg2->lang['forsecurity']                         = "От съображения за сигурност е препоръчително да затворите този прозорец на браузера.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Тази инсталация е стара X дни. Натиснете тук за да проверите за нова версия!</a></b>";
$mg2->lang['updatesuccess']                       = "Обновяването е успешно";
$mg2->lang['renamefailure']                       = "ГРЕШКА: Файлът съдържа неразрешени символи!";
$mg2->lang['filedeleted']                         = "Файла е изтрит";
$mg2->lang['filenotfound']                        = "Файла не е намерен!";
$mg2->lang['filesimported']                       = "файла са импортирани";
$mg2->lang['nofilestoimport']                     = "ГРЕШКА: Няма файлове за импортиране!";
$mg2->lang['foldernotempty']                      = "ГРЕШКА: Папката не е празна!";
$mg2->lang['folderdeleted']                       = "Папката е изтрита";
$mg2->lang['folderupdated']                       = "Папката е обновена";
$mg2->lang['foldercreated']                       = "Папката е създадена";
$mg2->lang['folderexists']                        = "ГРЕШКА: Папка с това име съществува!";
$mg2->lang['filesuploaded']                       = "файла са заредени";
$mg2->lang['settingssaved']                       = "Настройките са записани";
$mg2->lang['nopwdmatch']                          = "Настройките са записани<br /><br />ГРЕШКА: Паролата не съответства - новата парола не е записана!";
$mg2->lang['filesmovedto']                        = "файла са преместени в";
$mg2->lang['filesdeleted']                        = "файла са изтрити!";
$mg2->lang['file']                                = "файл";
$mg2->lang['files']                               = "файлове";
$mg2->lang['folder']                              = "папка";
$mg2->lang['folders']                             = "папки";
$mg2->lang['rebuild']                             = "Обнови";
$mg2->lang['rebuildimages']                       = "Обнови иконките";
$mg2->lang['rebuildsuccess']                      = "Обновлението е завършено";
$mg2->lang['donate']                              = "MG2 е безплатен програма, лицензирана по GPL. Ако мислите, че тази програма е полезна, моля направете дарение на автора чрез натискане на бутона по-долу.";
$mg2->lang['from']                                = "От";
$mg2->lang['comment']                             = "Коментар";
$mg2->lang['comments']                            = "Коментари";
$mg2->lang['by']                                  = "от";
$mg2->lang['commentsdeleted']                     = "Коментар(и) изтрит(и)";
$mg2->lang['buttonmove']                          = "Премести";
$mg2->lang['buttondelete']                        = "Изтрий";
$mg2->lang['deleteconfirm']                       = "Да изтрия избраните файлове?";
$mg2->lang['imagecolumns']                        = "Колони изображения";
$mg2->lang['imagerows']                           = "Редове изображения";
$mg2->lang['viewfolder']                          = "Покажи папка";
$mg2->lang['viewimage']                           = "Покажи изображение";
$mg2->lang['viewgallery']                         = "Покажи галерията";
$mg2->lang['rotateright']                         = "Завърти на 90 градуса надясно";
$mg2->lang['rotateleft']                          = "Завърти на 90 градуса наляво";
$mg2->lang['imagerotated']                        = "Изображението е завъртяно!";
$mg2->lang['gifnotrotated']                       = "ГРЕШКА: .GIF файловете не могат да бъдат завъртени поради ограничение в библиотеката GD !";
$mg2->lang['help']                                = "Помощ";
$mg2->lang['slideshowdelay']                      = "Slideshow интервал";
$mg2->lang['websitelink']                         = "";
$mg2->lang['marknew']                             = "Маркирай елементите по-нови от X дни (0 = няма)";
$mg2->lang['folderempty']                         = "Тази папка е празна";
$mg2->lang['noimage']                             = "Търсеното изображение не съществува!";
?>
