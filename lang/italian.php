﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Italiano                                    //
//                                                                                     //
//                               TRANSLATED BY: Pasquale Annunziata                    //
//                               EMAIL: zero6@tiscali.it                               //
//                                                                                     //
//                               LAST UPDATED: 22. Feb 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

//TRADUZIONE FRASI DELLA GALLERIA
$mg2->lang['gallery']                             = "Galleria";
$mg2->lang['of']                                  = "di";
$mg2->lang['first']                               = "Inizio";
$mg2->lang['prev']                                = "Precedente";
$mg2->lang['next']                                = "Successivo";
$mg2->lang['last']                                = "Fine";
$mg2->lang['thumbs']                              = "Miniature";
$mg2->lang['exif info']                           = "Dati Exif";
$mg2->lang['model']                               = "Modello";
$mg2->lang['shutter']                             = "velocità otturatore";
$mg2->lang['viewslideshow']                       = "Avvia proiezioni";
$mg2->lang['stopslideshow']                       = "Ferma proiezioni";
$mg2->lang['aperture']                            = "Apertura";
$mg2->lang['flash']                               = "Flash";
$mg2->lang['focallength']                         = "Lunghezza focale";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Compensazione Esposizione";
$mg2->lang['original']                            = "Originale";
$mg2->lang['metering']                            = "Misurare";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Pagina";
$mg2->lang['all']                                 = "Tutto";
$mg2->lang['fullsize']                            = "Mostra misure originali immegini";
$mg2->lang['addcomment']                          = "Aggiungi commento";
$mg2->lang['name']                                = "Nome";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Commento aggiunto";
$mg2->lang['commentexists']                       = "ERRORE: il commento già esistente!";
$mg2->lang['commentmissing']                      = "ERRORE: Tutti i campi del commento devono essere riempiti!";
$mg2->lang['enterpassword']                       = "Inserisci la password";
$mg2->lang['thissection']                         = "Questa sezione è protetta da password";

// TRADUZIONE FRASI AMMINISTRATORE
$mg2->lang['root']                                = "Root";
$mg2->lang['thumb']                               = "Miniatura";
$mg2->lang['dateadded']                           = "Data invio";
$mg2->lang['upload']                              = "Invia files";
$mg2->lang['import']                              = "Importa file trasmessi da";
$mg2->lang['newfolder']                           = "Nuova cartella";
$mg2->lang['viewgallery']                         = "Vedi Galleria";
$mg2->lang['setup']                               = "Impostazioni";
$mg2->lang['logoff']                              = "Scollegati";
$mg2->lang['menutxt_upload']                      = "Invia";
$mg2->lang['menutxt_import']                      = "Importa";
$mg2->lang['menutxt_newfolder']                   = "Nuova cartella";
$mg2->lang['menutxt_viewgallery']                 = "Vedi Galleria";
$mg2->lang['menutxt_setup']                       = "Impostazioni";
$mg2->lang['menutxt_logoff']                      = "Scollegati";
$mg2->lang['delete']                              = "Cancella";
$mg2->lang['cancel']                              = "Annulla";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Cancella cartella";
$mg2->lang['navigation']                          = "Navigazione";
$mg2->lang['images']                              = "immagine/i";
$mg2->lang['filename']                            = "Nome file";
$mg2->lang['title']                               = "Titolo";
$mg2->lang['description']                         = "Descrizione";
$mg2->lang['setasthumb']                          = "Setta come icona cartella";
$mg2->lang['editfolder']                          = "Modifica cartella";
$mg2->lang['editimage']                           = "Modifica immagine";
$mg2->lang['nofolderselected']                    = "Nessuna cartella selezionata";
$mg2->lang['foldername']                          = "Nome cartella";
$mg2->lang['newpassword']                         = "Nuova password";
$mg2->lang['deletepassword']                      = "Cancella password";
$mg2->lang['introtext']                           = "Testo inroduttivo";
$mg2->lang['deletethumb']                         = "Cancella miniatura";
$mg2->lang['moveto']                              = "Sposta a";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Grandezza file";
$mg2->lang['width']                               = "Larghezza";
$mg2->lang['height']                              = "Altezza";
$mg2->lang['date']                                = "Data";
$mg2->lang['ascending']                           = "Ascendente";
$mg2->lang['descending']                          = "Discendente";
$mg2->lang['newfolder']                           = "Nuova cartella";
$mg2->lang['password']                            = "Password";
$mg2->lang['direction']                           = "Direzione";
$mg2->lang['sortby']                              = "Ordina per";
$mg2->lang['gallerytitle']                        = "Titolo Galleria";
$mg2->lang['adminemail']                          = "Email Amministratore";
$mg2->lang['language']                            = "Lingua";
$mg2->lang['skin']                                = "Aspetto";
$mg2->lang['dateformat']                          = "Formato data";
$mg2->lang['DDMMYY']                              = "DD MMM YYYY";
$mg2->lang['MMDDYY']                              = "MMM DD, YYYY";
$mg2->lang['MM.DD.YY']                            = "MM.DD.YY";
$mg2->lang['DD.MM.YY']                            = "DD.MM.YY";
$mg2->lang['YYYYMMDD']                            = "YYYYMMDD";
$mg2->lang['sendmail']                            = "Invia email commento";
$mg2->lang['foldericons']                         = "scegli icone cartella";
$mg2->lang['showexif']                            = "Mostra Exif";
$mg2->lang['allowcomments']                       = "Permetti commenti";
$mg2->lang['copyright']                           = "Notizie sul Copyright";
$mg2->lang['passwordchange']                      = "Modifica password (lascia vuota per non modificarla)";
$mg2->lang['oldpasswordsetup']                    = "Inserisci la password attuale";
$mg2->lang['newpasswordsetup']                    = "Nuova Password (lascia vuota = usa quella attuale)";
$mg2->lang['newpasswordsetupconfirm']             = "Inserisci di nuovo la nuova password";
$mg2->lang['advanced']                            = "Avanzata";
$mg2->lang['allowedextensions']                   = "Estensioni permesse";
$mg2->lang['imgwidth']                            = "Larghezza massima immagine (0 = disabilita)";
$mg2->lang['indexfile']                           = "File indice della Galleria";
$mg2->lang['thumbquality']                        = "Qualita' miniatura";
$mg2->lang['uploadimport']                        = "Ricordati di importare le tue immagini dopo averle inviate!";
$mg2->lang['image']                               = "Immagine";
$mg2->lang['edit']                                = "Modifica";
$mg2->lang['editcurrentfolder']                   = "Modifica cartella corrente";
$mg2->lang['deletecurrentfolder']                 = "Cancella cartella corrente";
$mg2->lang['by']                                  = "di";
$mg2->lang['loginagain']                          = "Effettua di nuovo il login";
$mg2->lang['securitylogoff']                      = "Disconnessione";
$mg2->lang['autologoff']                          = "Sei stato disconnesso dopo 15 minuti di inattività.";
$mg2->lang['logoff']                              = "Disconnesso";
$mg2->lang['forsecurity']                         = "Per motivi di sicurezza è raccomandato chiudere questa finestra.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Questa installazione è vecchia di X giorni. Clicca qui per controllare eventuali aggiornamenti!</a></b>";
$mg2->lang['updatesuccess']                       = "Aggiornamento effettuato con successo!";
$mg2->lang['renamefailure']                       = "ERRORE: Il nome del file contiene caratteri non consentiti!";
$mg2->lang['filedeleted']                         = "File cancellato";
$mg2->lang['filenotfound']                        = "File non trovato!";
$mg2->lang['filesimported']                       = "file(s) importato";
$mg2->lang['nofilestoimport']                     = "ERRORE: nessun file importato!";
$mg2->lang['foldernotempty']                      = "ERRORE: Cartella non vuota!";
$mg2->lang['folderdeleted']                       = "Cartella cancellata";
$mg2->lang['folderupdated']                       = "Cartella modificata";
$mg2->lang['foldercreated']                       = "Cartella creata";
$mg2->lang['folderexists']                        = "ERRORE: la cartella già esiste!";
$mg2->lang['filesuploaded']                       = "File(s) trasmesso(i)";
$mg2->lang['settingssaved']                       = "Settaggi salvati";
$mg2->lang['nopwdmatch']                          = "Settaggi salvati<br /><br />ERRORE: Password non corrispondenti - la nuova password non è stata salvata!";
$mg2->lang['filesmovedto']                        = "file(s) spostato(i) in";
$mg2->lang['filesdeleted']                        = "file(s) cancellato(i)!";
$mg2->lang['file']                                = "file";
$mg2->lang['files']                               = "files";
$mg2->lang['folder']                              = "cartella";
$mg2->lang['folders']                             = "cartelle";
$mg2->lang['rebuild']                             = "Ricalcolo";
$mg2->lang['rebuildimages']                       = "Ricalcola miniature";
$mg2->lang['rebuildsuccess']                      = "Ricalcolo completata";
$mg2->lang['donate']                              = "MG2 è un software gratuiro,  è distribuirlo sotto licenza GPL. Se ritieni questo software utile, sei pregato di fare una donazione all'autore premendo il tasto qui sotto.";
$mg2->lang['from']                                = "Da";
$mg2->lang['comment']                             = "Commento";
$mg2->lang['comments']                            = "Commenti";
$mg2->lang['by']                                  = "di";
$mg2->lang['commentsdeleted']                     = "Commento(i) cancellato(i)";
$mg2->lang['buttonmove']                          = "Sposta";
$mg2->lang['buttondelete']                        = "Cancella";
$mg2->lang['deleteconfirm']                       = "Vuoi cancellare i files selezionati?";
$mg2->lang['imagecolumns']                        = "Colonne immagini";
$mg2->lang['imagerows']                           = "Righe immagini";
$mg2->lang['viewfolder']                          = "Vedi cartella";
$mg2->lang['viewimage']                           = "Vedi immagine";
$mg2->lang['viewgallery']                         = "Vedi galleria";
$mg2->lang['rotateright']                         = "Ruota di 90 gradi a destra";
$mg2->lang['rotateleft']                          = "Ruota di 90 gradi a sinistra";
$mg2->lang['imagerotated']                        = "Immagine ruotata!";
$mg2->lang['gifnotrotated']                       = "ERRORE: i file .GIF non possono essere ruotati per una limitazione della libreria GD!";
$mg2->lang['help']                                = "aiuto";
?>
