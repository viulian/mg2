﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Estonian                                    //
//                                                                                     //
//                               TRANSLATED BY: Priit Tsirel                           //
//                               EMAIL: bestpriidik@yahoo.com                          //
//                                                                                     //
//                               LAST UPDATED: 21. Jul 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galerii";
$mg2->lang['of']                                  = " ";
$mg2->lang['first']                               = "Esimene";
$mg2->lang['prev']                                = "Eelmine";
$mg2->lang['next']                                = "J&auml;rgmine";
$mg2->lang['last']                                = "Viimane";
$mg2->lang['thumbs']                              = "Pisipildid";
$mg2->lang['exif info']                           = "Exif Informatioon";
$mg2->lang['model']                               = "Mudel";
$mg2->lang['shutter']                             = "Katiku kiirus";
$mg2->lang['viewslideshow']                       = "Vaata slideshow-d";
$mg2->lang['stopslideshow']                       = "Peata slideshow";
$mg2->lang['aperture']                            = "Avaus";
$mg2->lang['flash']                               = "V&auml;lk";
$mg2->lang['focallength']                         = "Fookuskaugus";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "S&auml;rituse kompensatsioon";
$mg2->lang['original']                            = "Originaal";
$mg2->lang['metering']                            = "Metering";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Lehek&uuml;lg";
$mg2->lang['all']                                 = "K&otilde;ik";
$mg2->lang['fullsize']                            = "Vaata pilti t&auml;issuuruses";
$mg2->lang['addcomment']                          = "Lisa kommentaar";
$mg2->lang['name']                                = "Nimi";
$mg2->lang['email']                               = "E-mail";
$mg2->lang['commentadded']                        = "Kommentaar lisatud";
$mg2->lang['commentexists']                       = "VIGA: Kommentaar juba eksisteerib!";
$mg2->lang['commentmissing']                      = "VIGA: K&otilde;ik kommentaari v&auml;ljad peavad olema t&auml;idetud!";
$mg2->lang['enterpassword']                       = "Sisesta salas&otilde;na";
$mg2->lang['thissection']                         = "See osa on salas&otilde;naga kaitstud";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Juurkataloog";
$mg2->lang['thumb']                               = "Pisipilt";
$mg2->lang['dateadded']                           = "Lisamise kuup&auml;ev";
$mg2->lang['upload']                              = "Lae failid &uuml;les";
$mg2->lang['import']                              = "Impordi &uuml;leslaetud failid...";
$mg2->lang['newfolder']                           = "Uus kataloog";
$mg2->lang['viewgallery']                         = "Vaata galeriid";
$mg2->lang['setup']                               = "Seaded";
$mg2->lang['logoff']                              = "Logi v&auml;lja";
$mg2->lang['menutxt_upload']                      = "Lae &uuml;les";
$mg2->lang['menutxt_import']                      = "Impordi";
$mg2->lang['menutxt_newfolder']                   = "Uus kaust";
$mg2->lang['menutxt_viewgallery']                 = "Vaata galeriid";
$mg2->lang['menutxt_setup']                       = "Setup";
$mg2->lang['menutxt_logoff']                      = "V&auml;ljalogimine";
$mg2->lang['delete']                              = "Kustuta";
$mg2->lang['cancel']                              = "T&uuml;hista";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Kustuta kaust";
$mg2->lang['navigation']                          = "Navigatsioon";
$mg2->lang['images']                              = "pilt(i)";
$mg2->lang['filename']                            = "Faili nimi";
$mg2->lang['title']                               = "Pealkiri";
$mg2->lang['description']                         = "Kirjeldus";
$mg2->lang['setasthumb']                          = "Sea kausta pisipildiks";
$mg2->lang['editfolder']                          = "Muuda kausta";
$mg2->lang['editimage']                           = "Muuda pilti";
$mg2->lang['nofolderselected']                    = "&uuml;htegi kausta pole valitud";
$mg2->lang['foldername']                          = "Kausta nimi";
$mg2->lang['newpassword']                         = "Uus salas&otilde;na";
$mg2->lang['deletepassword']                      = "Kustuta salas&otilde;na";
$mg2->lang['introtext']                           = "Intro tekst";
$mg2->lang['deletethumb']                         = "Kustuta pisipilt";
$mg2->lang['moveto']                              = "Liiguta...";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Faili suurus";
$mg2->lang['width']                               = "Laius";
$mg2->lang['height']                              = "K&otilde;rgus";
$mg2->lang['date']                                = "Kuup&auml;ev";
$mg2->lang['ascending']                           = "Kasvav";
$mg2->lang['descending']                          = "Kahanev";
$mg2->lang['newfolder']                           = "Uus kaust";
$mg2->lang['password']                            = "Salas&otilde;na";
$mg2->lang['direction']                           = "Suund";
$mg2->lang['sortby']                              = "Sorteeri";
$mg2->lang['gallerytitle']                        = "Galerii pealkiri";
$mg2->lang['adminemail']                          = "Administraatori e-mail";
$mg2->lang['language']                            = "Keel";
$mg2->lang['skin']                                = "Skin";
$mg2->lang['dateformat']                          = "Kuup&auml;va formaat";
$mg2->lang['DDMMYY']                              = "PP KKK AAAA";
$mg2->lang['MMDDYY']                              = "KKK PP, AAAA";
$mg2->lang['MM.DD.YY']                            = "KK.PP.AA";
$mg2->lang['DD.MM.YY']                            = "PP.KK.AA";
$mg2->lang['YYYYMMDD']                            = "AAAAKKPP";
$mg2->lang['sendmail']                            = "Saada kommentaarid mailile";
$mg2->lang['foldericons']                         = "Sunni kausta ikoonid";
$mg2->lang['showexif']                            = "N&auml;ita Exif";
$mg2->lang['allowcomments']                       = "Luba kommentaaarid";
$mg2->lang['copyright']                           = "Koopiakaitse";
$mg2->lang['passwordchange']                      = "Muuda salas&otilde;na (3 x t&uuml;hi = endine)";
$mg2->lang['oldpasswordsetup']                    = "Sisesta praegune salas&otilde;na";
$mg2->lang['newpasswordsetup']                    = "Uus salas&otilde;na (t&uuml;hi = kasuta praegust)";
$mg2->lang['newpasswordsetupconfirm']             = "Sisesta uus salas&otilde;na uuesti";
$mg2->lang['advanced']                            = "Advanced";
$mg2->lang['allowedextensions']                   = "Lubatud laiendused";
$mg2->lang['imgwidth']                            = "Max. pildi laius (0 = ei kasuta)";
$mg2->lang['indexfile']                           = "Galerii index fail";
$mg2->lang['thumbquality']                        = "Pisipildi kvaliteet";
$mg2->lang['image']                               = "Pilt";
$mg2->lang['edit']                                = "Muuda";
$mg2->lang['editcurrentfolder']                   = "Muuda praegust kausta";
$mg2->lang['deletecurrentfolder']                 = "Kustuta praegune kausst";
$mg2->lang['by']                                  = " ";
$mg2->lang['loginagain']                          = "Logi uuesti sisse";
$mg2->lang['securitylogoff']                      = "Turvalisuse v&auml;lja logimine";
$mg2->lang['autologoff']                          = "Sind logiti automaatselt v&auml;lja peale 15 minutilist tegevusetust.";
$mg2->lang['logoff']                              = "Logi v&auml;lja";
$mg2->lang['forsecurity']                         = "Turvalisuse huvides tuleks sulgeda praegune brauseri aken sulgeda .";
$mg2->lang['updatesuccess']                       = "Uuendus &otilde;nnestus";
$mg2->lang['renamefailure']                       = "VIGA: Faili nimi sisaldab mitte lubatud t&auml;hem&auml;rke!";
$mg2->lang['filedeleted']                         = "Fail kustutatud";
$mg2->lang['filenotfound']                        = "Faili ei leitud!";
$mg2->lang['filesimported']                       = "fail(id) imporditud";
$mg2->lang['nofilestoimport']                     = "VIGA: Ei ole faile mida importida!";
$mg2->lang['foldernotempty']                      = "VIGA: Kaust ei ole t&uuml;hi!";
$mg2->lang['folderdeleted']                       = "Kaust kustutatud";
$mg2->lang['folderupdated']                       = "Kaust uuendatud";
$mg2->lang['foldercreated']                       = "Kaust loodud";
$mg2->lang['folderexists']                        = "VIGA: Kausta nimi on juba kasutusel!";
$mg2->lang['filesuploaded']                       = "Fail(id) &uuml;les laetud - Impordin...";
$mg2->lang['settingssaved']                       = "Seaded salvestatud";
$mg2->lang['nopwdmatch']                          = "Seaded salvestatud<br /><br />VIGA: Salas&otilde;na ei klapi - uut salas&otilde;na ei salvestatud!";
$mg2->lang['filesmovedto']                        = "fail(id) liigutatud";
$mg2->lang['filesdeleted']                        = "fail(id) kustutatud!";
$mg2->lang['file']                                = "fail";
$mg2->lang['files']                               = "failid";
$mg2->lang['folder']                              = "kaust";
$mg2->lang['folders']                             = "kaustad";
$mg2->lang['rebuild']                             = "Tee uuest";
$mg2->lang['rebuildimages']                       = "Tee pisipildid uuesti";
$mg2->lang['rebuildsuccess']                      = "Uuesti tegemine l&otilde;petatud";
$mg2->lang['donate']                              = "MG2 on tasuta tarkvara, litsenseeritud GPL-i all. Kui leiad et pragramm on sulle kasulik, siis palun tee autorile annetus vajutades allpool asetsevat nuppu.";
$mg2->lang['from']                                = "Kellelt";
$mg2->lang['comment']                             = "Kommentaar";
$mg2->lang['comments']                            = "Kommentaarid";
$mg2->lang['by']                                  = " ";
$mg2->lang['commentsdeleted']                     = "Kommentaar(id) kustutatud";
$mg2->lang['buttonmove']                          = "Liiguta";
$mg2->lang['buttondelete']                        = "Kustuta";
$mg2->lang['deleteconfirm']                       = "Kustuta valitud failid?";
$mg2->lang['imagecolumns']                        = "Pildi tulbad";
$mg2->lang['imagerows']                           = "Pildi read";
$mg2->lang['viewfolder']                          = "Vaata kausta";
$mg2->lang['viewimage']                           = "Vaata pilti";
$mg2->lang['viewgallery']                         = "Vaata galeriid";
$mg2->lang['rotateright']                         = "Pööra 90 kraadi paremale";
$mg2->lang['rotateleft']                          = "Pööra 90 kraadi vasakule";
$mg2->lang['imagerotated']                        = "Pilt pööratud!";
$mg2->lang['gifnotrotated']                       = "VIGA: .GIF faile ei sa pöörata, kuna seda ei v&otilde;imalda GD lib!";
$mg2->lang['help']                                = "Abi";
$mg2->lang['slideshowdelay']                      = "Slideshow viivitus";
$mg2->lang['websitelink']                         = "Veebilehe link (t&uuml;hi = ei ole kasutusel)";
$mg2->lang['marknew']                             = "Markeeri elemendid mis on uuemad kui 10 p&auml;eva (0 = ei ole kasutusel)";
$mg2->lang['folderempty']                         = "See kaust on t&uuml;hi";
$mg2->lang['noimage']                             = "Soovitud pilti ei eksisteeri!";



$mg2->lang['actions']                             = "Tegevused";
$mg2->lang['backupcomplete']                      = "Andmebaasi varundamine l&otilde;petatud";
$mg2->lang['backuplink']                          = "Tee andmebaasist varukoopia";
$mg2->lang['viewlogfile']                         = "Vaata logifaili";
$mg2->lang['website']                             = "Veebilehele";
$mg2->lang['backtofolder']                        = "Tagasi kataloogi";
$mg2->lang['permerror1']                          = "KASUTUS&Otilde;IGUSE VIGA: Ei saa kirjutada galerii juurkataloogi!";
$mg2->lang['whattodo1']                           = "Chmod-i oma galerii kataloog 777-ks";
$mg2->lang['permerror2']                          = "KASUTUS&Otilde;IGUSE VIGA: Ei saa kirjutada 'pictures' kataloogi!";
$mg2->lang['whattodo2']                           = "Chmod-i oma galerii 'pictures' kataloog 777-ks";
$mg2->lang['permerror3']                          = "KASUTUS&Otilde;IGUSE VIGA: Ei saa kirjutada faili 'mg2db_idatabase.php'!";
$mg2->lang['whattodo3']                           = "Chmod-i 'mg2db_idatabase.php' fail 777-ks";
$mg2->lang['permerror4']                          = "KASUTUS&Otilde;IGUSE VIGA: Ei saa kirjutada faili 'mg2db_idatabase_temp.php'!";
$mg2->lang['whattodo4']                           = "Chmod-i 'mg2db_idatabase_temp.php' fail 777-ks";
$mg2->lang['permerror5']                          = "KASUTUS&Otilde;IGUSE VIGA: Ei saa kirjutada faili 'mg2db_fdatabase.php'!";
$mg2->lang['whattodo5']                           = "Chmod-i 'mg2db_fdatabase.php' fail 777-ks";
$mg2->lang['permerror6']                          = "KASUTUS&Otilde;IGUSE VIGA: Ei saa kirjutada faili 'mg2db_fdatabase_temp.php'!";
$mg2->lang['whattodo6']                           = "Chmod-i 'mg2db_fdatabase_temp.php' fail 777-ks";

?>
