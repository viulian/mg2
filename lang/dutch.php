﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Dutch                                       //
//                                                                                     //
//                               TRANSLATED BY: Thijs Haenen                           //
//                               EMAIL: thijsie@gmail.com                              //
//                                                                                     //
//                               LAST UPDATED: 17. Mar 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Gallerij";
$mg2->lang['of']                                  = "van";
$mg2->lang['first']                               = "Eerste";
$mg2->lang['prev']                                = "Vorige";
$mg2->lang['next']                                = "Volgende";
$mg2->lang['last']                                = "Laatste";
$mg2->lang['thumbs']                              = "Thumbs";
$mg2->lang['exif info']                           = "Exif Informatie";
$mg2->lang['model']                               = "Model";
$mg2->lang['shutter']                             = "Sluitersnelheid";
$mg2->lang['viewslideshow']                       = "Start slideshow";
$mg2->lang['stopslideshow']                       = "Stop slideshow";
$mg2->lang['aperture']                            = "Opening";
$mg2->lang['flash']                               = "Flits";
$mg2->lang['focallength']                         = "Brandpuntslengte";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Blootstellingcompensatie";
$mg2->lang['original']                            = "Origineel";
$mg2->lang['metering']                            = "Het meten";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Pagina";
$mg2->lang['all']                                 = "Alle";
$mg2->lang['fullsize']                            = "Toon afbeelding van volledige grootte";
$mg2->lang['addcomment']                          = "Plaats commentaar";
$mg2->lang['name']                                = "Naam";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Commentaar toegevoegd";
$mg2->lang['commentexists']                       = "FOUT: Commentaar bestaat al!";
$mg2->lang['commentmissing']                      = "FOUT: Alle commentaarvelden moeten ingevuld worden!";
$mg2->lang['enterpassword']                       = "Typ wachtwoord";
$mg2->lang['thissection']                         = "Dit gedeelte is met een wachtwoord beveiligd";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Begin";
$mg2->lang['thumb']                               = "Thumb";
$mg2->lang['dateadded']                           = "Datum toegevoegd";
$mg2->lang['upload']                              = "Upload bestanden";
$mg2->lang['import']                              = "Importeer geüploadde bestanden naar";
$mg2->lang['newfolder']                           = "Nieuwe map";
$mg2->lang['viewgallery']                         = "Toon gallerij";
$mg2->lang['setup']                               = "Instellingen";
$mg2->lang['logoff']                              = "Log uit";
$mg2->lang['menutxt_upload']                      = "Upload";
$mg2->lang['menutxt_import']                      = "Importeer";
$mg2->lang['menutxt_newfolder']                   = "Nieuwe map";
$mg2->lang['menutxt_viewgallery']                 = "Toon gallerij";
$mg2->lang['menutxt_setup']                       = "Instellingen";
$mg2->lang['menutxt_logoff']                      = "Log uit";
$mg2->lang['delete']                              = "Verwijder";
$mg2->lang['cancel']                              = "Annuleer";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Verwijder map";
$mg2->lang['navigation']                          = "Navigatie";
$mg2->lang['images']                              = "afbeelding(en)";
$mg2->lang['filename']                            = "Bestandsnaam";
$mg2->lang['title']                               = "Titel";
$mg2->lang['description']                         = "Beschrijving";
$mg2->lang['setasthumb']                          = "Zet als mapthumbnail";
$mg2->lang['editfolder']                          = "Wijzig map";
$mg2->lang['editimage']                           = "Wijzig afbeelding";
$mg2->lang['nofolderselected']                    = "Geen map geselecteerd";
$mg2->lang['foldername']                          = "Mapnaam";
$mg2->lang['newpassword']                         = "Nieuw wachtwoord";
$mg2->lang['deletepassword']                      = "Verwijder wachtwoord";
$mg2->lang['introtext']                           = "Introtext";
$mg2->lang['deletethumb']                         = "Verwijder thumbnail";
$mg2->lang['moveto']                              = "Verplaats naar";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Bestandsgrootte";
$mg2->lang['width']                               = "Breedte";
$mg2->lang['height']                              = "Hoogte";
$mg2->lang['date']                                = "Datum";
$mg2->lang['ascending']                           = "Oplopend";
$mg2->lang['descending']                          = "Aflopend";
$mg2->lang['newfolder']                           = "Nieuwe map";
$mg2->lang['password']                            = "Wachtwoord";
$mg2->lang['direction']                           = "Richting";
$mg2->lang['sortby']                              = "Sorteer op";
$mg2->lang['gallerytitle']                        = "Gallerijtitel";
$mg2->lang['adminemail']                          = "Admin email";
$mg2->lang['language']                            = "Taal";
$mg2->lang['skin']                                = "Skin";
$mg2->lang['dateformat']                          = "Datumformaat";
$mg2->lang['DDMMYY']                              = "DD MM JJJJ";
$mg2->lang['MMDDYY']                              = "MM DD, JJJJ";
$mg2->lang['MM.DD.YY']                            = "MM.DD.JJ";
$mg2->lang['DD.MM.YY']                            = "DD.MM.JJ";
$mg2->lang['YYYYMMDD']                            = "JJJJMMDD";
$mg2->lang['sendmail']                            = "Verstuur commentaaremails";
$mg2->lang['foldericons']                         = "Verplicht mapiconen";
$mg2->lang['showexif']                            = "Toon Exif";
$mg2->lang['allowcomments']                       = "Sta commentaar toe";
$mg2->lang['copyright']                           = "Copyrightmelding";
$mg2->lang['passwordchange']                      = "Verander wachtwoord (3 x leeg = houd huidige)";
$mg2->lang['oldpasswordsetup']                    = "Voer huidig wachtwoord in";
$mg2->lang['newpasswordsetup']                    = "Voer nieuw wachtwoord in (leeg = gebruik huidige)";
$mg2->lang['newpasswordsetupconfirm']             = "Voer nieuw wachtwoord opnieuw in";
$mg2->lang['advanced']                            = "Geavanceerd";
$mg2->lang['allowedextensions']                   = "Toegestane bestandstypen";
$mg2->lang['imgwidth']                            = "Max. afbeeldingbreedte (0 = niet beschikbaar)";
$mg2->lang['indexfile']                           = "Gallerij indexbestand";
$mg2->lang['thumbquality']                        = "Thumbnailkwaliteit";
$mg2->lang['uploadimport']                        = "Vergeet niet je afbeeldingen te importeren na het uploaden!";
$mg2->lang['image']                               = "Afbeelding";
$mg2->lang['edit']                                = "Wijzig";
$mg2->lang['editcurrentfolder']                   = "Wijzig huidige map";
$mg2->lang['deletecurrentfolder']                 = "Verwijder huidige map";
$mg2->lang['by']                                  = "door";
$mg2->lang['loginagain']                          = "Log opnieuw in";
$mg2->lang['securitylogoff']                      = "Uitgelogd voor veiligheid";
$mg2->lang['autologoff']                          = "Je werd automatisch uitgelogd na 15 minuten zonder activiteit.";
$mg2->lang['logoff']                              = "Log uit";
$mg2->lang['forsecurity']                         = "Voor veiligheidsredenen wordt het aangeraden om dit browservenster te sluiten.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Deze installatie is X dagen oud. Klik hier om nu te controleren op updates!</a></b>";
$mg2->lang['updatesuccess']                       = "Update succesvol";
$mg2->lang['renamefailure']                       = "FOUT: Bestandsnaam bevat illegale tekens!";
$mg2->lang['filedeleted']                         = "Bestand verwijderd";
$mg2->lang['filenotfound']                        = "Bestand niet gevonden!";
$mg2->lang['filesimported']                       = "bestand(en) geïmporteerd";
$mg2->lang['nofilestoimport']                     = "FOUT: Geen bestanden om te importeren!";
$mg2->lang['foldernotempty']                      = "FOUT: Map niet leeg!";
$mg2->lang['folderdeleted']                       = "Map verwijderd";
$mg2->lang['folderupdated']                       = "Map geupdate";
$mg2->lang['foldercreated']                       = "Map gemaakt";
$mg2->lang['folderexists']                        = "FOUT: Mapnaam bestaat al!";
$mg2->lang['filesuploaded']                       = "Bestand(en) geupload";
$mg2->lang['settingssaved']                       = "Instellingen bewaard";
$mg2->lang['nopwdmatch']                          = "Instellingen bewaard<br /><br />FOUT: Wachtwoorden komen niet overeen - nieuw wachtwoord niet bewaard!";
$mg2->lang['filesmovedto']                        = "bestand(en) verplaatst naar";
$mg2->lang['filesdeleted']                        = "bestand(en) verwijderd!";
$mg2->lang['file']                                = "bestand";
$mg2->lang['files']                               = "bestanden";
$mg2->lang['folder']                              = "map";
$mg2->lang['folders']                             = "mappen";
$mg2->lang['rebuild']                             = "Herbouw";
$mg2->lang['rebuildimages']                       = "Herbouw thumbnails";
$mg2->lang['rebuildsuccess']                      = "Herbouwen compleet";
$mg2->lang['donate']                              = "MG2 is gratis software, onder licentie van de GPL. Als je deze software bruikbaar vindt, doe dan alstublieft een donatie aan de auteur door op de knop hieronder te klikken.";
$mg2->lang['from']                                = "Van";
$mg2->lang['comment']                             = "Commentaar";
$mg2->lang['comments']                            = "Commentaar";
$mg2->lang['by']                                  = "door";
$mg2->lang['commentsdeleted']                     = "Commentaar verwijderd";
$mg2->lang['buttonmove']                          = "Verplaats";
$mg2->lang['buttondelete']                        = "Verwijder";
$mg2->lang['deleteconfirm']                       = "Verwijder geselecteerde bestanden?";
$mg2->lang['imagecolumns']                        = "Afbeeldingkolommen";
$mg2->lang['imagerows']                           = "Afbeeldingrijen";
$mg2->lang['viewfolder']                          = "Toon Map";
$mg2->lang['viewimage']                           = "Toon afbeelding";
$mg2->lang['viewgallery']                         = "Toon gallerij";
$mg2->lang['rotateright']                         = "Draai 90 graden naar rechts";
$mg2->lang['rotateleft']                          = "Draai 90 graden naar links";
$mg2->lang['imagerotated']                        = "Afbeelding gedraaid!";
$mg2->lang['gifnotrotated']                       = "FOUT: .GIF bestanden kunnen niet worden gedraaid vanwege beperkingen in de GD lib!";
$mg2->lang['']                                    = "";
$mg2->lang['help']                                = "Help";
$mg2->lang['slideshowdelay']                      = "Diavoorstellingsvertraging";
$mg2->lang['websitelink']                         = "Website link (blanco = niet beschikbaar)";
$mg2->lang['marknew']                             = "Markeer voorwerpen nieuwer dan X dagen (0 = niet beschikbaar)";
$mg2->lang['folderempty']                         = "Deze map is leeg";
$mg2->lang['noimage']                             = "De gezochtte afbeelding bestaat niet!";
$mg2->lang['websitelink']                         = "Link terug naar de website";
?>
