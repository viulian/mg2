<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                      Norwegian/Norsk                                   //
//                                                                                     //
//                               TRANSLATED BY: Roy Hansen                           //
//                               EMAIL: video-friluft@home.no                             //
//                                                                                     //
//                               LAST UPDATED: 16. Mar 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galleri";
$mg2->lang['of']                                  = "av";
$mg2->lang['first']                               = "F&oslash;rste";
$mg2->lang['prev']                                = "Forrige";
$mg2->lang['next']                                = "Neste";
$mg2->lang['last']                                = "Siste";
$mg2->lang['thumbs']                              = "Miniatyrer";
$mg2->lang['exif info']                           = "Exif Opplysninger";
$mg2->lang['model']                               = "Model";
$mg2->lang['shutter']                             = "Lukkehastighet";
$mg2->lang['viewslideshow']                       = "Se diasshow";
$mg2->lang['stopslideshow']                       = "Stopp diasshow";
$mg2->lang['aperture']                            = "Blender";
$mg2->lang['flash']                               = "Blitz";
$mg2->lang['focallength']                         = "Brennvidde";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Eksponeringskompensasjon";
$mg2->lang['original']                            = "Opprinnelig";
$mg2->lang['metering']                            = "Lysm&aring;ling";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Side";
$mg2->lang['all']                                 = "Alle";
$mg2->lang['fullsize']                            = "Vis full st&oslash;rrelse";
$mg2->lang['addcomment']                          = "Tilf&oslash;y kommentar";
$mg2->lang['name']                                = "Navn";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Kommentar tilf&oslash;yet";
$mg2->lang['commentexists']                       = "FEIL: Kommentar findes allerede!";
$mg2->lang['commentmissing']                      = "FEIL: Alle felter skal utfylles!";
$mg2->lang['enterpassword']                       = "Tast inn passord";
$mg2->lang['thissection']                         = "Denne mappen er beskyttet med passord";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Galleri topp";
$mg2->lang['thumb']                               = "Miniatyr";
$mg2->lang['dateadded']                           = "Dato tilf&oslash;yet";
$mg2->lang['upload']                              = "Last opp filer";
$mg2->lang['import']                              = "Importer opplastede filer til";
$mg2->lang['newfolder']                           = "Ny mappe";
$mg2->lang['viewgallery']                         = "Vis galleri";
$mg2->lang['setup']                               = "Oppsett";
$mg2->lang['logoff']                              = "Logg av";
$mg2->lang['menutxt_upload']                      = "Last opp";
$mg2->lang['menutxt_import']                      = "Importer";
$mg2->lang['menutxt_newfolder']                   = "Ny mappe";
$mg2->lang['menutxt_viewgallery']                 = "Vis galleri";
$mg2->lang['menutxt_setup']                       = "Oppsett";
$mg2->lang['menutxt_logoff']                      = "Logg av";
$mg2->lang['delete']                              = "Slett";
$mg2->lang['cancel']                              = "Annuller";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Slett mappe";
$mg2->lang['navigation']                          = "Navigasjon";
$mg2->lang['images']                              = "bilde(r)";
$mg2->lang['filename']                            = "Filnavn";
$mg2->lang['title']                               = "Tittel";
$mg2->lang['description']                         = "Beskrivelse";
$mg2->lang['setasthumb']                          = "Velg miniatyr til mappe";
$mg2->lang['editfolder']                          = "Rediger mappe";
$mg2->lang['editimage']                           = "Rediger bilde";
$mg2->lang['nofolderselected']                    = "Ingen mappe valgt";
$mg2->lang['foldername']                          = "Mappenavn";
$mg2->lang['newpassword']                         = "Nytt passord";
$mg2->lang['deletepassword']                      = "Slett passord";
$mg2->lang['introtext']                           = "Introduksjonstekst";
$mg2->lang['deletethumb']                         = "Slett miniatyr";
$mg2->lang['moveto']                              = "Flytt til";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Filst&oslash;rrelse";
$mg2->lang['width']                               = "Bredde";
$mg2->lang['height']                              = "H&oslash;yde";
$mg2->lang['date']                                = "Dato";
$mg2->lang['ascending']                           = "Stigende";
$mg2->lang['descending']                          = "Fallende";
$mg2->lang['newfolder']                           = "Ny mappe";
$mg2->lang['password']                            = "Passord";
$mg2->lang['direction']                           = "Retning";
$mg2->lang['sortby']                              = "Sorter etter";
$mg2->lang['gallerytitle']                        = "Galleritittel";
$mg2->lang['adminemail']                          = "Administrators email";
$mg2->lang['charset']                          = "Karaktersett";
$mg2->lang['language']                            = "Spr&aring;k";
$mg2->lang['skin']                                = "Utseende (skin)";
$mg2->lang['dateformat']                          = "Dato format";
$mg2->lang['DDMMYY']                              = "DD MMM &Aring;&Aring;&Aring;&Aring;";
$mg2->lang['MMDDYY']                              = "MMM DD, &Aring;&Aring;&Aring;&Aring;";
$mg2->lang['MM.DD.YY']                            = "MM.DD.&Aring;&Aring;";
$mg2->lang['DD.MM.YY']                            = "DD.MM.&Aring;&Aring;";
$mg2->lang['YYYYMMDD']                            = "&Aring;&Aring;&Aring;&Aring;MMDD";
$mg2->lang['sendmail']                            = "Send email ved kommentarer";
$mg2->lang['foldericons']                         = "Bruk mappeikoner";
$mg2->lang['showexif']                            = "Vis Exif";
$mg2->lang['allowcomments']                       = "Tillat kommentarer";
$mg2->lang['copyright']                           = "Copyright notis";
$mg2->lang['passwordchange']                      = "Skift passord (La v&aelig;re tom for det samme)";
$mg2->lang['oldpasswordsetup']                    = "N&aring;v&aelig;rende passord";
$mg2->lang['newpasswordsetup']                    = "Nytt passord";
$mg2->lang['newpasswordsetupconfirm']             = "Nytt passord igjen";
$mg2->lang['advanced']                            = "Avansert";
$mg2->lang['allowedextensions']                   = "Gyldige filtyper";
$mg2->lang['imgwidth']                            = "Maks. bildebredde (0 = sl&aring; fra)";
$mg2->lang['indexfile']                           = "Galleri index fil";
$mg2->lang['thumbquality']                        = "Miniatyrkvalitet";
$mg2->lang['uploadimport']                        = "Husk &aring; importere bildene etter opplasting!";
$mg2->lang['image']                               = "Bilde";
$mg2->lang['edit']                                = "Rediger";
$mg2->lang['editcurrentfolder']                   = "Rediger denne mappen";
$mg2->lang['deletecurrentfolder']                 = "Slett denne mappen";
$mg2->lang['by']                                  = "av";
$mg2->lang['loginagain']                          = "Logg p&aring; igen";
$mg2->lang['securitylogoff']                      = "Logget av!";
$mg2->lang['autologoff']                          = "Du er blitt automatisk logget av etter 15 minutters inaktivitet.";
$mg2->lang['logoff']                              = "Logg av";
$mg2->lang['forsecurity']                         = "Av sikkerhetsgrunner anbefales det &aring; lukke dette browser vinduet.";
$mg2->lang['upgradenote']                        = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Denne installasjon er X dager gammel! Trykk her for &aring; se efter oppgraderinger</a></b>";
$mg2->lang['updatesuccess']                       = "Opdateringen lyktes";
$mg2->lang['renamefailure']                       = "FEIL: Filnavnet innholder illegale karakterer!";
$mg2->lang['filedeleted']                         = "Filen er slettet";
$mg2->lang['filenotfound']                        = "Filen ble ikke funnet!";
$mg2->lang['filesimported']                       = "fil(er) importert";
$mg2->lang['nofilestoimport']                     = "FEIL: Ingen filer &aring; importere!";
$mg2->lang['foldernotempty']                      = "FEIL: Mappen er ikke tom!";
$mg2->lang['folderdeleted']                       = "Mappen er slettet";
$mg2->lang['folderupdated']                       = "Mappen er oppdatert";
$mg2->lang['foldercreated']                       = "Mappen er opprettet";
$mg2->lang['folderexists']                        = "FEIL: Mappenavnet eksisterer allerede!";
$mg2->lang['filesuploaded']                       = "Fil(er) lastet opp";
$mg2->lang['settingssaved']                       = "Oppsettet er gjemt";
$mg2->lang['nopwdmatch']                          = "Oppsetting gjemt<br /><br />FEIL: Passordet passer ikke, ble ikke gjemt!";
$mg2->lang['filesmovedto']                        = "fil(er) flyttet til";
$mg2->lang['filesdeleted']                        = "fil(er) slettet!";
$mg2->lang['file']                                = "fil";
$mg2->lang['files']                               = "filer";
$mg2->lang['folder']                              = "mappe";
$mg2->lang['folders']                             = "mapper";
$mg2->lang['rebuild']                             = "Gjennoppbygg";
$mg2->lang['rebuildimages']                       = "Gjennoppbygg miniatyrer";
$mg2->lang['rebuildsuccess']                      = "Gjennoppbygging ferdig";
$mg2->lang['donate']                              = "MG2 er gratis software under GPL lisens. Hvis du synes om det, s&aring; gi en donasjon ved &aring; trykke p&aring; denne knappen:";
$mg2->lang['from']                                = "Fra";
$mg2->lang['comment']                             = "Kommentar";
$mg2->lang['comments']                            = "Kommentarer";
$mg2->lang['by']                                  = "av";
$mg2->lang['commentsdeleted']                     = "Kommentar(er) slettet";
$mg2->lang['buttonmove']                          = "Flytt";
$mg2->lang['buttondelete']                        = "Slett";
$mg2->lang['deleteconfirm']                       = "Slett disse filer?";
$mg2->lang['imagecolumns']                        = "Bilder pr. kolonne";
$mg2->lang['imagerows']                           = "Bilder pr. rekke";
$mg2->lang['viewfolder']                          = "Vis mappe";
$mg2->lang['viewimage']                           = "Vis bilde";
$mg2->lang['rotateright']                         = "Roter 90 grader til h&oslash;yre";
$mg2->lang['rotateleft']                          = "Roter 90 grader til venstre";
$mg2->lang['imagerotated']                        = "Bilde ble rotert";
$mg2->lang['gifnotrotated']                       = "FEIL. .GIF bilder kan ikke roteres pga. begrensninger i GD lib!";
$mg2->lang['help']                                = "Hjelp";
$mg2->lang['slideshowdelay']                      = "Slideshow forsinkelse";
$mg2->lang['websitelink']                         = "Link til nettsted (tom = disable)";
$mg2->lang['marknew']                             = "Merk objekter nyere enn  X dager (0 = disable)";
$mg2->lang['folderempty']                         = "Denne mappen er tom";
$mg2->lang['noimage']                             = "Bildet du spurte etter finnes ikke!";
?>