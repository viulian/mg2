<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGUAGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                      Danish/Dansk                                   //
//                                                                                     //
//                               TRANSLATED BY: Thomas Rybak                           //
//                               EMAIL: support@minigal.dk                             //
//                                                                                     //
//                               LAST UPDATED: 18. Aug 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galleri";
$mg2->lang['of']                                  = "af";
$mg2->lang['first']                               = "F&oslash;rste";
$mg2->lang['prev']                                = "Forrige";
$mg2->lang['next']                                = "N&aelig;ste";
$mg2->lang['last']                                = "Sidste";
$mg2->lang['thumbs']                              = "Miniaturer";
$mg2->lang['exif info']                           = "Exif oplysninger";
$mg2->lang['model']                               = "Model";
$mg2->lang['shutter']                             = "Lukkehastighed";
$mg2->lang['viewslideshow']                       = "Se diasshow";
$mg2->lang['stopslideshow']                       = "Stop diasshow";
$mg2->lang['aperture']                            = "Bl&aelig;nde";
$mg2->lang['flash']                               = "Blitz";
$mg2->lang['focallength']                         = "Br&aelig;ndvidde";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Eksponeringskompensation";
$mg2->lang['original']                            = "Oprindelig";
$mg2->lang['metering']                            = "Lysm&aring;ling";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Side";
$mg2->lang['all']                                 = "Alle";
$mg2->lang['fullsize']                            = "Vis fuld st&oslash;rrelse";
$mg2->lang['addcomment']                          = "Tilf&oslash;j kommentar";
$mg2->lang['name']                                = "Navn";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Kommentar tilf&oslash;jet";
$mg2->lang['commentexists']                       = "FEJL: Kommentar findes allerede!";
$mg2->lang['commentmissing']                      = "FEJL: Alle felter skal udfyldes!";
$mg2->lang['enterpassword']                       = "Indtast kodeord";
$mg2->lang['thissection']                         = "Denne mappe er beskyttet med kodeord";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Galleri top";
$mg2->lang['thumb']                               = "Miniature";
$mg2->lang['dateadded']                           = "Dato tilf&oslash;jet";
$mg2->lang['upload']                              = "Upload filer";
$mg2->lang['import']                              = "Importer uploadede filer til";
$mg2->lang['newfolder']                           = "Ny mappe";
$mg2->lang['viewgallery']                         = "Vis galleri";
$mg2->lang['setup']                               = "Ops&aelig;tning";
$mg2->lang['logoff']                              = "Log af";
$mg2->lang['menutxt_upload']                      = "Upload";
$mg2->lang['menutxt_import']                      = "Importer";
$mg2->lang['menutxt_newfolder']                   = "Ny mappe";
$mg2->lang['menutxt_viewgallery']                 = "Vis galleri";
$mg2->lang['menutxt_setup']                       = "Ops&aelig;tning";
$mg2->lang['menutxt_logoff']                      = "Log af";
$mg2->lang['delete']                              = "Slet";
$mg2->lang['cancel']                              = "Annuller";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Slet mappe";
$mg2->lang['navigation']                          = "Navigation";
$mg2->lang['images']                              = "billede(r)";
$mg2->lang['filename']                            = "Filnavn";
$mg2->lang['title']                               = "Titel";
$mg2->lang['description']                         = "Beskrivelse";
$mg2->lang['setasthumb']                          = "V&aelig;lg miniature til mappe";
$mg2->lang['editfolder']                          = "Rediger mappe";
$mg2->lang['editimage']                           = "Rediger billede";
$mg2->lang['nofolderselected']                    = "Ingen mappe valgt";
$mg2->lang['foldername']                          = "Mappe navn";
$mg2->lang['newpassword']                         = "Ny kodeord";
$mg2->lang['deletepassword']                      = "Slet kodeord";
$mg2->lang['introtext']                           = "Introduktion tekst";
$mg2->lang['deletethumb']                         = "Slet miniature";
$mg2->lang['moveto']                              = "Flyt til";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Filst&oslash;rrelse";
$mg2->lang['width']                               = "Bredde";
$mg2->lang['height']                              = "H&oslash;jde";
$mg2->lang['date']                                = "Dato";
$mg2->lang['ascending']                           = "Stigende";
$mg2->lang['descending']                          = "Faldende";
$mg2->lang['newfolder']                           = "Ny mappe";
$mg2->lang['password']                            = "Kodeord";
$mg2->lang['direction']                           = "Retning";
$mg2->lang['sortby']                              = "Sorter efter";
$mg2->lang['gallerytitle']                        = "Galleri titel";
$mg2->lang['adminemail']                          = "Administrators email";
$mg2->lang['language']                            = "Sprog";
$mg2->lang['skin']                                = "Udseende (skin)";
$mg2->lang['dateformat']                          = "Dato format";
$mg2->lang['DDMMYY']                              = "DD MMM &Aring;&Aring;&Aring;&Aring;";
$mg2->lang['MMDDYY']                              = "MMM DD, &Aring;&Aring;&Aring;&Aring;";
$mg2->lang['MM.DD.YY']                            = "MM.DD.&Aring;&Aring;";
$mg2->lang['DD.MM.YY']                            = "DD.MM.&Aring;&Aring;";
$mg2->lang['YYYYMMDD']                            = "&Aring;&Aring;&Aring;&Aring;MMDD";
$mg2->lang['sendmail']                            = "Send email ved kommentarer";
$mg2->lang['foldericons']                         = "Brug mappe ikoner";
$mg2->lang['showexif']                            = "Vis Exif";
$mg2->lang['allowcomments']                       = "Tillad kommentarer";
$mg2->lang['copyright']                           = "Copyright besked";
$mg2->lang['passwordchange']                      = "Skift kodeord (efterlad tom for at beholde nuv&aelig;rende)";
$mg2->lang['oldpasswordsetup']                    = "Nuv&aelig;rende kodeord";
$mg2->lang['newpasswordsetup']                    = "Nyt kodeord";
$mg2->lang['newpasswordsetupconfirm']             = "Nyt kodeord igen";
$mg2->lang['advanced']                            = "Avanceret";
$mg2->lang['allowedextensions']                   = "Tilladte filtyper";
$mg2->lang['imgwidth']                            = "Maks. billede bredde (0 = sl&aring; fra)";
$mg2->lang['indexfile']                           = "Galleri index fil";
$mg2->lang['thumbquality']                        = "Miniature kvalitet";
$mg2->lang['image']                               = "Billede";
$mg2->lang['edit']                                = "Rediger";
$mg2->lang['editcurrentfolder']                   = "Rediger denne mappe";
$mg2->lang['deletecurrentfolder']                 = "Slet denne mappe";
$mg2->lang['by']                                  = "af";
$mg2->lang['loginagain']                          = "Log p&aring; igen";
$mg2->lang['securitylogoff']                      = "Logget af!";
$mg2->lang['autologoff']                          = "Du er blevet automatisk logget af efter 15 minutters inaktivitet.";
$mg2->lang['logoff']                              = "Log af";
$mg2->lang['forsecurity']                         = "Af sikkerhedsgrunde anbefales det at lukke dette browser vindue.";
$mg2->lang['updatesuccess']                       = "Opdateringen lykkedes";
$mg2->lang['renamefailure']                       = "FEJL: Filnavn indeholder illegale karakterer!";
$mg2->lang['filedeleted']                         = "Filen er slettet";
$mg2->lang['filenotfound']                        = "Filen blev ikke fundet!";
$mg2->lang['filesimported']                       = "fil(er) importeret";
$mg2->lang['nofilestoimport']                     = "FEJL: Ingen filer at importere!";
$mg2->lang['foldernotempty']                      = "FEJL: Mappen er ikke tom!";
$mg2->lang['folderdeleted']                       = "Mappen er slettet";
$mg2->lang['folderupdated']                       = "Mappen er opdateret";
$mg2->lang['foldercreated']                       = "Mappen er oprettet";
$mg2->lang['folderexists']                        = "FEJL: Mappenavn eksisterer allerede!";
$mg2->lang['filesuploaded']                       = "Fil(er) uploadet - Importerer...";
$mg2->lang['settingssaved']                       = "Ops&aelig;tning gemt";
$mg2->lang['nopwdmatch']                          = "Ops&aelig;tning gemt<br /><br />FEJL: Kodeord passer ikke, blev ikke gemt!";
$mg2->lang['filesmovedto']                        = "fil(er) flyttet til";
$mg2->lang['filesdeleted']                        = "fil(er) slettet!";
$mg2->lang['file']                                = "fil";
$mg2->lang['files']                               = "filer";
$mg2->lang['folder']                              = "mappe";
$mg2->lang['folders']                             = "mapper";
$mg2->lang['rebuild']                             = "Genopbyg";
$mg2->lang['rebuildimages']                       = "Genopbyg miniaturer";
$mg2->lang['rebuildsuccess']                      = "Genopbygning f&aelig;rdig";
$mg2->lang['donate']                              = "MG2 er gratis software under GPL licens. Hvis du syntes om det, s&aring; giv en donation ved at trykke p&aring; denne knap:";
$mg2->lang['from']                                = "Fra";
$mg2->lang['comment']                             = "Kommentar";
$mg2->lang['comments']                            = "Kommentarer";
$mg2->lang['by']                                  = "af";
$mg2->lang['commentsdeleted']                     = "Kommentar(er) slettet";
$mg2->lang['buttonmove']                          = "Flyt";
$mg2->lang['buttondelete']                        = "Slet";
$mg2->lang['deleteconfirm']                       = "Slet disse filer?";
$mg2->lang['imagecolumns']                        = "Billeder pr. kolonne";
$mg2->lang['imagerows']                           = "Billeder pr. r&aelig;kke";
$mg2->lang['viewfolder']                          = "Vis mappe";
$mg2->lang['viewimage']                           = "Vis billede";
$mg2->lang['rotateright']                         = "Roter 90 grader til h&oslash;jre";
$mg2->lang['rotateleft']                          = "Roter 90 grader til venstre";
$mg2->lang['imagerotated']                        = "Billede roteret";
$mg2->lang['gifnotrotated']                       = "FEJL: .GIF billeder kan ikke roteres pga. begr&aelig;nsninger i GD lib!";
$mg2->lang['help']                                = "Hj&aelig;lp";
$mg2->lang['slideshowdelay']                      = "Diasshow forsinkelse";
$mg2->lang['websitelink']                         = "Hjemmeside link (tom = ingen)";
$mg2->lang['marknew']                             = "Marker filer/mapper nyere end X dage (0 = sl&aring; fra)";
$mg2->lang['folderempty']                         = "Denne mappe er tom";
$mg2->lang['noimage']                             = "Billedet findes ikke!";
$mg2->lang['actions']                             = "Handlinger";
$mg2->lang['backupcomplete']                      = "Database backup udf&oslash;rt";
$mg2->lang['backuplink']                          = "Lav backup af database";
$mg2->lang['viewlogfile']                         = "Vis logfil";
$mg2->lang['website']                             = "Til hjemmesiden";
$mg2->lang['backtofolder']                        = "Tilbage til mappen";
$mg2->lang['permerror1']                          = "FEJL I TILLADELSE: Kan ikke skrive til galleriets mappe!";
$mg2->lang['whattodo1']                           = "Du skal &aelig;ndre chmod p&aring; galleriets mappe til 777";
$mg2->lang['permerror2']                          = "FEJL I TILLADELSE: Kan ikke skrive til 'pictures' mappen!";
$mg2->lang['whattodo2']                           = "Du skal &aelig;ndre chmod p&aring; 'pictures' mappen til 777";
$mg2->lang['permerror3']                          = "FEJL I TILLADELSE: Kan ikke skrive til filen 'mg2db_idatabase.php'!";
$mg2->lang['whattodo3']                           = "Du skal &aelig;ndre chmod p&aring; 'mg2db_idatabase.php' til 777";
$mg2->lang['permerror4']                          = "FEJL I TILLADELSE: Kan ikke skrive til filen 'mg2db_idatabase_temp.php'!";
$mg2->lang['whattodo4']                           = "Du skal &aelig;ndre chmod p&aring; 'mg2db_idatabase_temp.php' til 777";
$mg2->lang['permerror5']                          = "FEJL I TILLADELSE: Kan ikke skrive til filen 'mg2db_fdatabase.php'!";
$mg2->lang['whattodo5']                           = "Du skal &aelig;ndre chmod p&aring; 'mg2db_fdatabase.php' til 777";
$mg2->lang['permerror6']                          = "FEJL I TILLADELSE: Kan ikke skrive til filen 'mg2db_fdatabase_temp.php'!";
$mg2->lang['whattodo6']                           = "Du skal &aelig;ndre chmod p&aring; 'mg2db_fdatabase_temp.php' til 777";
?>
