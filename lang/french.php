﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         French                                      //
//                                                                                     //
//                               TRANSLATED BY: Charles DUBOIS                         //
//                               EMAIL: mrpapoo@gmail.com                              //
//                                                                                     //
//                  LAST UPDATED: 21. Juillet 2005															       //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galerie";
$mg2->lang['of']                                  = "de";
$mg2->lang['first']                               = "Premi&egrave;re";
$mg2->lang['prev']                                = "Pr&eacute;c&eacute;dente";
$mg2->lang['next']                                = "Suivante";
$mg2->lang['last']                                = "Derni&egrave;re";
$mg2->lang['thumbs']                              = "Miniatures";
$mg2->lang['exif info']                           = "Information Exif";
$mg2->lang['model']                               = "Mod&egrave;le";
$mg2->lang['shutter']                             = "Vitesse d'obturation";
$mg2->lang['viewslideshow']                       = "D&eacute;marrer le slideshow";
$mg2->lang['stopslideshow']                       = "Arr&ecirc;ter le slideshow";
$mg2->lang['aperture']                            = "Ouverture";
$mg2->lang['flash']                               = "Flash";
$mg2->lang['focallength']                         = "Distance focale";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Programme d'exposition";
$mg2->lang['original']                            = "Original";
$mg2->lang['metering']                            = "Metering";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Page";
$mg2->lang['all']                                 = "Toutes";
$mg2->lang['fullsize']                            = "Voir l'image en taille originale";
$mg2->lang['addcomment']                          = "Ajouter un commentaire";
$mg2->lang['name']                                = "Nom";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Commentaire ajout&eacute;";
$mg2->lang['commentexists']                       = "ERREUR: Le commentaire exist d&eacute;j&agrave; !";
$mg2->lang['commentmissing']                      = "ERREUR: Tous les champs du commentaire doivent &ecirc;tre remplis !";
$mg2->lang['enterpassword']                       = "Entrer le mot de passe";
$mg2->lang['thissection']                         = "Cette rubrique est prot&eacute;g&eacute;e par un mot de passe";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Racine";
$mg2->lang['thumb']                               = "Miniature";
$mg2->lang['dateadded']                           = "Date ajout&eacute;e";
$mg2->lang['upload']                              = "Envoyer des fichiers";
$mg2->lang['import']                              = "Importer les fichiers vers";
$mg2->lang['newfolder']                           = "Nouveau dossier";
$mg2->lang['viewgallery']                         = "Voir la galerie";
$mg2->lang['setup']                               = "Pr&eacute;f&eacute;rences";
$mg2->lang['logoff']                              = "Log off";
$mg2->lang['menutxt_upload']                      = "Envoyer";
$mg2->lang['menutxt_import']                      = "Importer";
$mg2->lang['menutxt_newfolder']                   = "Nouveau dossier";
$mg2->lang['menutxt_viewgallery']                 = "Voir la galerie";
$mg2->lang['menutxt_setup']                       = "Pr&eacute;f&eacute;rences";
$mg2->lang['menutxt_logoff']                      = "Logoff";
$mg2->lang['delete']                              = "Supprimer";
$mg2->lang['cancel']                              = "Annuler";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Supprimer le dossier";
$mg2->lang['navigation']                          = "Navigation";
$mg2->lang['images']                              = "image(s)";
$mg2->lang['filename']                            = "Nom de fichier";
$mg2->lang['title']                               = "Titre";
$mg2->lang['description']                         = "Description";
$mg2->lang['setasthumb']                          = "D&eacute;finir comme miniature de dossier";
$mg2->lang['editfolder']                          = "Editer le dossier";
$mg2->lang['editimage']                           = "Editer l'image";
$mg2->lang['nofolderselected']                    = "Pas de dossier s&eacute;lectionn&eacute;";
$mg2->lang['foldername']                          = "Nom du dossier";
$mg2->lang['newpassword']                         = "Nouveau mot de passe";
$mg2->lang['deletepassword']                      = "Supprimer le mot de passe";
$mg2->lang['introtext']                           = "Texte d'intro";
$mg2->lang['deletethumb']                         = "Supprimer la miniature";
$mg2->lang['moveto']                              = "D&eacute;placer vers";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Taille de fichier";
$mg2->lang['width']                               = "Largeur";
$mg2->lang['height']                              = "Hauteur";
$mg2->lang['date']                                = "Date";
$mg2->lang['ascending']                           = "Croissant";
$mg2->lang['descending']                          = "D&eacute;croissant";
$mg2->lang['newfolder']                           = "Nouveau dossier";
$mg2->lang['password']                            = "Mot de passe";
$mg2->lang['direction']                           = "Direction";
$mg2->lang['sortby']                              = "Trier par";
$mg2->lang['gallerytitle']                        = "Titre de la galerie";
$mg2->lang['adminemail']                          = "Email de l'administrateur";
$mg2->lang['language']                            = "Langue";
$mg2->lang['skin']                                = "Skin";
$mg2->lang['dateformat']                          = "Format de la date";
$mg2->lang['DDMMYY']                              = "JJ MMM AAAA";
$mg2->lang['MMDDYY']                              = "MMM JJ, AAAA";
$mg2->lang['MM.DD.YY']                            = "MM.JJ.AA";
$mg2->lang['DD.MM.YY']                            = "JJ.MM.AA";
$mg2->lang['YYYYMMDD']                            = "AAAAMMJJ";
$mg2->lang['sendmail']                            = "Envoyer les commentaires par mail";
$mg2->lang['foldericons']                         = "Forcer les ic&ocirc;nes de dossiers";
$mg2->lang['showexif']                            = "Montrer d&eacute;tails Exif";
$mg2->lang['allowcomments']                       = "Autoriser les commentaires";
$mg2->lang['copyright']                           = "Note de Copyright";
$mg2->lang['passwordchange']                      = "Changer le mot de passe (3 x vide = conserver le mot de passe actuel)";
$mg2->lang['oldpasswordsetup']                    = "Entrer le mot de passe actuel";
$mg2->lang['newpasswordsetup']                    = "Nouveau mot de passe (vide = utiliser l'actuel)";
$mg2->lang['newpasswordsetupconfirm']             = "Entrer de nouveau le mot de passe";
$mg2->lang['advanced']                            = "Avanc&eacute;";
$mg2->lang['allowedextensions']                   = "Extensions autoris&eacute;es";
$mg2->lang['imgwidth']                            = "Largeur max de l'image (0 = d&eacute;sactiv&eacute;)";
$mg2->lang['indexfile']                           = "Fichier d'index de la galerie";
$mg2->lang['thumbquality']                        = "Qualit&eacute; de miniature";
$mg2->lang['image']                               = "Image";
$mg2->lang['edit']                                = "Editer";
$mg2->lang['editcurrentfolder']                   = "Editer le dossier courant";
$mg2->lang['deletecurrentfolder']                 = "Supprimer le dossier courant";
$mg2->lang['by']                                  = "par";
$mg2->lang['loginagain']                          = "Identifiez-vous une nouvelle fois";
$mg2->lang['securitylogoff']                      = "Fin de session de s&eacute;curit&eacute;";
$mg2->lang['autologoff']                          = "Votre session a &eacute;t&eacute; annul&eacute;e apr&egrave;s 15 minutes d'inactivit&eacute;.";
$mg2->lang['logoff']                              = "Fin de session";
$mg2->lang['forsecurity']                         = "Pour des raisons de s&eacute;curit&eacute;, il est recommand&eacute; de fermer cette fen&ecirc;tre de navigateur.";
$mg2->lang['updatesuccess']                       = "Mise &agrave; jour r&eacute;ussie";
$mg2->lang['renamefailure']                       = "ERREUR: Le nom de fichier contient des caract&egrave;res non autoris&eacute;s !";
$mg2->lang['filedeleted']                         = "Fichier supprim&eacute;";
$mg2->lang['filenotfound']                        = "Fichier non trouv&eacute; !";
$mg2->lang['filesimported']                       = "fichier(s) import&eacute;(s)";
$mg2->lang['nofilestoimport']                     = "ERREUR: Pas de fichiers &agrave; importer !";
$mg2->lang['foldernotempty']                      = "ERREUR: Dossier non vide !";
$mg2->lang['folderdeleted']                       = "Dossier supprim&eacute;";
$mg2->lang['folderupdated']                       = "Dossier mis &agrave; jour";
$mg2->lang['foldercreated']                       = "Dossier cr&eacute;&eacute;";
$mg2->lang['folderexists']                        = "ERREUR: Le nom de dossier existe d&eacute;j&agrave; !";
$mg2->lang['filesuploaded']                       = "Fichier(s) envoy&eacute;(s) - Importation...";
$mg2->lang['settingssaved']                       = "Pr&eacute;f&eacute;rences sauvegard&eacute;es";
$mg2->lang['nopwdmatch']                          = "Pr&eacute;f&eacute;rences sauvegard&eacute;es<br /><br />ERREUR: V&eacute;rification du nouveau mot de passe &eacute;chou&eacute;e - le nouveau mot de passe ne sera pas sauvegard&eacute; !";
$mg2->lang['filesmovedto']                        = "fichier(s) d&eacute;plac&eacute;(s) vers";
$mg2->lang['filesdeleted']                        = "fichier(s) supprim&eacute;(s)!";
$mg2->lang['file']                                = "fichier";
$mg2->lang['files']                               = "fichiers";
$mg2->lang['folder']                              = "dossier";
$mg2->lang['folders']                             = "dossiers";
$mg2->lang['rebuild']                             = "Reconstruire";
$mg2->lang['rebuildimages']                       = "Reconstruire les miniatures";
$mg2->lang['rebuildsuccess']                      = "Reconstruction termin&eacute;e";
$mg2->lang['donate']                              = "MG2 est un logiciel gratuit, sous license GPL. Si vous trouvez ce logiciel utile, SVP faites une donation &agrave; l'auteur en cliquant sur le bouton ci-dessous.";
$mg2->lang['from']                                = "De";
$mg2->lang['comment']                             = "Commentaire";
$mg2->lang['comments']                            = "Commentaires";
$mg2->lang['by']                                  = "par";
$mg2->lang['commentsdeleted']                     = "Commentaire(s) supprim&eacute;(s)";
$mg2->lang['buttonmove']                          = "D&eacute;placer";
$mg2->lang['buttondelete']                        = "Supprimer";
$mg2->lang['deleteconfirm']                       = "Supprimer les fichiers s&eacute;lectionn&eacute;s ?";
$mg2->lang['imagecolumns']                        = "Colonnes d'images";
$mg2->lang['imagerows']                           = "Lignes d'images";
$mg2->lang['viewfolder']                          = "Voir le dossier";
$mg2->lang['viewimage']                           = "Voir l'image";
$mg2->lang['viewgallery']                         = "Voir la galerie";
$mg2->lang['rotateright']                         = "Rotation de 90 degr&eacute;s vers la droite";
$mg2->lang['rotateleft']                          = "Rotation de 90 degr&eacute;s vers la gauche";
$mg2->lang['imagerotated']                        = "Image tourn&eacute;e !";
$mg2->lang['gifnotrotated']                       = "ERREUR: les fichiers .GIF ne peuvent &ecirc;tre tourn&eacute;s &agrave; cause des limitations de la librairie GD !";
$mg2->lang['help'] 																= "aide";

$mg2->lang['slideshowdelay']                      = "D&eacute;lai du Slideshow";
$mg2->lang['websitelink']                         = "Lien du site web (vide = non actif)";
$mg2->lang['marknew']                             = "Marquer les nouveaux objets de moins de X jours (0 = non actif)";
$mg2->lang['folderempty']                         = "Ce dossier est vide";
$mg2->lang['noimage']                             = "L'image demand&eacute;e n'existe pas !";

$mg2->lang['actions']                             = "Actions";
$mg2->lang['backupcomplete']                      = "Sauvegarde de la base accomplie";
$mg2->lang['backuplink']                          = "Faire une sauvegarde de la base";
$mg2->lang['viewlogfile']                         = "Voir le fichier log";
$mg2->lang['website']                             = "Vers le site web";
$mg2->lang['backtofolder']                        = "Retour au dossier";
$mg2->lang['permerror1']                          = "ERREUR DE PERMISSION: Impossible d'&eacute;crire dans le dossier racine!";
$mg2->lang['whattodo1']                           = "Faites un chmod 777 sur le dossier de la galerie";
$mg2->lang['permerror2']                          = "ERREUR DE PERMISSION: Impossible d'&eacute;crire dans le dossier 'pictures'!";
$mg2->lang['whattodo2']                           = "Faites un chmod 777 sur le dossier 'pictures'";
$mg2->lang['permerror3']                          = "ERREUR DE PERMISSION: Impossible d'&eacute;crire dans 'mg2db_idatabase.php'!";
$mg2->lang['whattodo3']                           = "Faites un chmod 777 sur 'mg2db_idatabase.php'";
$mg2->lang['permerror4']                          = "ERREUR DE PERMISSION: Impossible d'&eacute;crire dans 'mg2db_idatabase_temp.php'!";
$mg2->lang['whattodo4']                           = "Faites un chmod 777 sur 'mg2db_idatabase_temp.php'";
$mg2->lang['permerror5']                          = "ERREUR DE PERMISSION: Impossible d'&eacute;crire dans 'mg2db_fdatabase.php'!";
$mg2->lang['whattodo5']                           = "Faites un chmod 777 sur 'mg2db_fdatabase.php'";
$mg2->lang['permerror6']                          = "ERREUR DE PERMISSION: Impossible d'&eacute;crire dans 'mg2db_fdatabase_temp.php'!";
$mg2->lang['whattodo6']                           = "Faites un chmod 777 sur 'mg2db_fdatabase_temp.php'";
?>
