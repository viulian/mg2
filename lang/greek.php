﻿<?php
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                                                 //
//                                               MG2 LANGAUGE FILE:   Ελληνικά                              //
//                                                   http://www.minigal.dk                                                  //
//                                                                                                                                 //
//                                                                                                          //
//                                                                                     //
//                               ΜΕΤΑΦΡΑΣΗ: Σαπρίκης Χρίστος                           //
//                                 EMAIL: christos@xsap.gr                             //
//                                   http://www.xsap.gr                                //
//                                                                                     //
//                               LAST UPDATED: 5 Αυγούστου 2005                        //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////

// CHARSET INFORMATION
$mg2->charset = "utf-8";

//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Έκθεση Φωτογραφιών";
$mg2->lang['of']                                  = "of";
$mg2->lang['first']                               = "Αρχική";
$mg2->lang['prev']                                = "Προηγούμενη";
$mg2->lang['next']                                = "Επόμενη";
$mg2->lang['last']                                = "Τελευταία";
$mg2->lang['thumbs']                              = "Μικρογραφίες";
$mg2->lang['exif info']                           = "Πλήροφορίες Exif";
$mg2->lang['model']                               = "Μοντέλο";
$mg2->lang['shutter']                             = "Ταχύτητα";
$mg2->lang['viewslideshow']                       = "Συνεχόμενη προβολή των φωτογραφιών";
$mg2->lang['stopslideshow']                       = "Σταμάτησε την προβολή";
$mg2->lang['aperture']                            = "Διάφραγμα";
$mg2->lang['flash']                               = "Χρήση Φλας";
$mg2->lang['focallength']                         = "Εστιακή απόσταση";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Διόρθωση έκθεσης";
$mg2->lang['original']                            = "Λήψη";
$mg2->lang['metering']                            = "Metering";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Σελίδα";
$mg2->lang['all']                                 = "Όλες";
$mg2->lang['fullsize']                            = "Δες την εικόνα στο κανονικό της μέγεθος.<P>";
$mg2->lang['addcomment']                          = "Στείλε το σχόλιό σου";
$mg2->lang['name']                                = "Όνομα";
$mg2->lang['email']                               = "Email";
$mg2->lang['commentadded']                        = "Το σχόλιο προστέθηκε";
$mg2->lang['commentexists']                       = "ΛΑΘΟΣ: Το σχόλιο ήδη υπάρχει!";
$mg2->lang['commentmissing']                      = "ΛΑΘΟΣ: Πρέπει να συμπηρωθούν όλα τα πεδία!";
$mg2->lang['enterpassword']                       = "Γράψε τον κωδικό";
$mg2->lang['thissection']                         = "Αυτή η περιοχή είναι προστατευμένη με κωδικό!";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Root";
$mg2->lang['thumb']                               = "Μικρογραφία";
$mg2->lang['dateadded']                           = "Ημ/νια δημιουργίας";
$mg2->lang['upload']                              = "Μεταφορά αρχείων στην έκθεση";
$mg2->lang['import']                              = "Εισαγωγή των αρχείων που μεταφέρθηκαν σε";
$mg2->lang['newfolder']                           = "Νέος φάκελος";
$mg2->lang['viewgallery']                         = "Δες την έκθεση των φωτογραφιών";
$mg2->lang['setup']                               = "Ρυθμίσεις";
$mg2->lang['logoff']                              = "Αποσύνδεση";
$mg2->lang['menutxt_upload']                      = "Μεταφορά αρχείων στην έκθεση";
$mg2->lang['menutxt_import']                      = "Εισαγωγή";
$mg2->lang['menutxt_newfolder']                   = "Δημιουργία <br>Νέου φακέλου";
$mg2->lang['menutxt_viewgallery']                 = "Δες την έκθεση των φωτογραφιών";
$mg2->lang['menutxt_setup']                       = "Ρυθμίσεις";
$mg2->lang['menutxt_logoff']                      = "Αποσύνδεση";
$mg2->lang['delete']                              = "Διαγραφή";
$mg2->lang['cancel']                              = "Ακύρωση";
$mg2->lang['ok']                                  = "Ok";
$mg2->lang['deletefolder']                        = "Διαγραφή του φακέλου";
$mg2->lang['navigation']                          = "Πλοήγηση";
$mg2->lang['images']                              = "Φωτογραφίες";
$mg2->lang['filename']                            = "Όνομα αρχείου";
$mg2->lang['title']                               = "Τίτλος";
$mg2->lang['description']                         = "Περιγραφή";
$mg2->lang['setasthumb']                          = "Ορισμός ως εικόνα φακέλου";
$mg2->lang['editfolder']                          = "Επεξεργασία φακέλου";
$mg2->lang['editimage']                           = "Επεξεργασία φωτογραφίας";
$mg2->lang['nofolderselected']                    = "ΔΕΝ έχει επιλεγεί κάποιος φάκελος";
$mg2->lang['foldername']                          = "Όνομα φακέλου";
$mg2->lang['newpassword']                         = "Νέος κωδικός";
$mg2->lang['deletepassword']                      = "Διαγραφή κωδικού";
$mg2->lang['introtext']                           = "Εισαγωγικό κέιμενο";
$mg2->lang['deletethumb']                         = "Διαγραφή μικρογραφιών";
$mg2->lang['moveto']                              = "Μετακίνηση σε";
$mg2->lang['id']                                  = "Id";
$mg2->lang['filesize']                            = "Μέγεθος αρχείου";
$mg2->lang['width']                               = "Πλάτος Φωτ.";
$mg2->lang['height']                              = "Ύψος Φωτ.";
$mg2->lang['date']                                = "Ημερομηνία";
$mg2->lang['ascending']                           = "Αύξουσα";
$mg2->lang['descending']                          = "Φθίνουσα";
$mg2->lang['newfolder']                           = "Νέος φάκελος";
$mg2->lang['password']                            = "Κωδικός";
$mg2->lang['direction']                           = "Κατέυθυνση";
$mg2->lang['sortby']                              = "Ταξινόμηση κατά";
$mg2->lang['gallerytitle']                        = "Τίτλος έκθεσης φωτογραφίας";
$mg2->lang['adminemail']                          = "Εmail διαχειριστή";
$mg2->lang['language']                            = "Γλώσσα";
$mg2->lang['skin']                                = "Αλλαγή εμφάνισης";
$mg2->lang['dateformat']                          = "Μορφή ημερομηνίας";
$mg2->lang['DDMMYY']                              = "ΗΗ ΜΜΜ ΕΕΕΕ";
$mg2->lang['MMDDYY']                              = "ΜΜΜ ΗΗ, ΕΕΕΕ";
$mg2->lang['MM.DD.YY']                            = "ΜΜ.ΗΗ.ΕΕ";
$mg2->lang['DD.MM.YY']                            = "ΗΗ.ΜΜ.ΕΕ";
$mg2->lang['YYYYMMDD']                            = "ΕΕΕΕΜΜΗΗ";
$mg2->lang['sendmail']                            = "Να στέλνονται τα σχόλια στο email σου";
$mg2->lang['foldericons']                         = "Εμφάνιση φακέλων με στάνταρ εικονίδιο";
$mg2->lang['showexif']                            = "Εμφάνιση Exif";
$mg2->lang['allowcomments']                       = "Εμφάνιση σχολιών";
$mg2->lang['copyright']                           = "Πληροφορίες Copyright";
$mg2->lang['passwordchange']                      = "Αλλαγή κωδικού (παραμένει ο υπάρχον αν μείνουν και τα 3 πεδία άδεια)";
$mg2->lang['oldpasswordsetup']                    = "Γράψε τον υπάρχοντα κωδικό";
$mg2->lang['newpasswordsetup']                    = "Γράψε το νέο κωδικό (άδειο = παραμένει ο υπάρχον)";
$mg2->lang['newpasswordsetupconfirm']             = "Γράψε πάλι το νέο κωδικό";
$mg2->lang['advanced']                            = "Προηγμένες ρυθμίσεις";
$mg2->lang['allowedextensions']                   = "Επεκτάσεις αρχείων";
$mg2->lang['imgwidth']                            = "Μέγιστο πλάτος φωτογραφίας (0 = απενεργοποίηση)";
$mg2->lang['indexfile']                           = "Gallery index file";
$mg2->lang['thumbquality']                        = "Ποιότητα μικροφωτογραφιών";
$mg2->lang['image']                               = "Φωτογραφία";
$mg2->lang['edit']                                = "Επεξεργασία";
$mg2->lang['editcurrentfolder']                   = "Επεξεργασία συγκεκριμένου φακέλου";
$mg2->lang['deletecurrentfolder']                 = "Διαγραφή συγκεκριμένου φακέλου";
$mg2->lang['by']                                  = "by";
$mg2->lang['loginagain']                          = "Επανασύνδεση";
$mg2->lang['securitylogoff']                      = "Ασφαλής αποσύνδεση";
$mg2->lang['autologoff']                          = "Μετά από 15 λεπτά αδράνειας αποσυνδέεσαι αυτόματα.";
$mg2->lang['logoff']                              = "Αποσύνδεση";
$mg2->lang['forsecurity']                         = "Για λόγους ασφαλειας κλείσε αυτό το παράθυρο";
$mg2->lang['updatesuccess']                       = "Η αναβάθμιση ολοκληρώθηκε";
$mg2->lang['renamefailure']                       = "ΛΑΘΟΣ: Υπάρχουν με επιτρεπτοί χαρακτήρες στο όνομα του αρχείου!";
$mg2->lang['filedeleted']                         = "Το αρχείο διαγράφτηκε!";
$mg2->lang['filenotfound']                        = "Το αρχείο δεν βρέθηκε!";
$mg2->lang['filesimported']                       = "Η εισαγωγή των αρχείων ολοκληρώθηκε.";
$mg2->lang['nofilestoimport']                     = "ΛΑΘΟΣ: Δεν έγινε εισαγωγή αρχείων!";
$mg2->lang['foldernotempty']                      = "ΛΑΘΟΣ: Ο φάκελος ΔΕΝ είναι άδειος!";
$mg2->lang['folderdeleted']                       = "Ο φάκελος διαγράφτηκε";
$mg2->lang['folderupdated']                       = "Ο φάκελεος ανανεώθηκε";
$mg2->lang['foldercreated']                       = "Ο φάκελος δημιουργήθηκε";
$mg2->lang['folderexists']                        = "ΛΑΘΟΣ: Το όνομα του φακέλου ήδη υπάρχει!";
$mg2->lang['filesuploaded']                       = "Τα αρχεία μεταφέρθηκαν - Γίνεται εισαγωγή";
$mg2->lang['settingssaved']                       = "Οι ρυθμίσεις αποθηκεύτηκαν.";
$mg2->lang['nopwdmatch']                          = "Οι ρυθμίσεις αποθηκεύτηκαν.<br /><br />ΛΑΘΟΣ: Μη αποδεκτός κωδικός - ο νέος κωδικός δεν αποθηκεύτηκε!";
$mg2->lang['filesmovedto']                        = "τα  αρχεία μετακινήθηκαν στο";
$mg2->lang['filesdeleted']                        = "τα αρχεία διαγράφτηκαν!";
$mg2->lang['file']                                = "αρχείο";
$mg2->lang['files']                               = "αρχεία";
$mg2->lang['folder']                              = "φάκελος";
$mg2->lang['folders']                             = "φάκελοι";
$mg2->lang['rebuild']                             = "Αναδημιουργία";
$mg2->lang['rebuildimages']                       = "Αναδημιουργία μικρογραφιών";
$mg2->lang['rebuildsuccess']                      = "Η αναδημιουργία των μικρογραφιών ολοκληρώθηκε";
$mg2->lang['donate']                              = "Η MG2 είναι λογισμικό ελεύθερης διανομής με άδεια χρήσης σύμφωνα με τους κανόνες του GPL.<br>Αν νομίζεις πως το συγκεκριμένο λογισμικό σε έχει βοηθήσει μπορείς να ενισχύσεις τη βελτίωσή του.<br>Κάνε μια δωρεά πατώντας το παρακάτω κουμπί.";
$mg2->lang['from']                                = "Από";
$mg2->lang['comment']                             = "Σχόλιο";
$mg2->lang['comments']                            = "Σχόλια";
$mg2->lang['by']                                  = "by";
$mg2->lang['commentsdeleted']                     = "Το σχόλιο διαγράφτηκε";
$mg2->lang['buttonmove']                          = "Μετακίνηση";
$mg2->lang['buttondelete']                        = "Διαγραφή";
$mg2->lang['deleteconfirm']                       = "Να διαγραφούν τα επιλεγμένα αρχεία;";
$mg2->lang['imagecolumns']                        = "Στήλες με φωτογραφίες";
$mg2->lang['imagerows']                           = "Γραμμές με φωτογραφίες";
$mg2->lang['viewfolder']                          = "Δες το φάκελο";
$mg2->lang['viewimage']                           = "Δες τη φωτογραφία";
$mg2->lang['viewgallery']                         = "Δες την έκθεση των φωτογραφιών";
$mg2->lang['rotateright']                         = "Στορφή 90 μοιρών δεξιά";
$mg2->lang['rotateleft']                          = "Στορφή 90 μοιρών αριστερά";
$mg2->lang['imagerotated']                        = "Η περιστροφή της φωτογραφίας ολοκληρώθηκε!";
$mg2->lang['gifnotrotated']                       = "ΛΑΘΟΣ: τα αρχεία τύπου .GIF δεν μπορούν να περιστραφούν λόγω περιορισμών της βιβλιοθήκης GD!";
$mg2->lang['help']                                = "Βοήθεια";
$mg2->lang['slideshowdelay']                      = "Ρύθμιση χρόνου στη συνεχόμενη προβολή των φωτογραφιών";
$mg2->lang['websitelink']                         = "Επιστροφή στην Αρχική Σελίδα";
$mg2->lang['marknew']                             = "Τσέκαρε τα αντικείμενα που είναι νεώτερα από X ημέρες (0 = απενεργοποίηση)";
$mg2->lang['folderempty']                         = "Αυτός ο φάκελος είναι άδειος";
$mg2->lang['noimage']                             = "Η εικόνα που ψάχνεις δεν υπάρχει!";

$mg2->lang['actions']                             = "Ενέργειες";
$mg2->lang['backupcomplete']                      = "Το backup της βάσης δεδομένων ολοκληρώθηκε";
$mg2->lang['backuplink']                          = "Κάνε backup στη βάση δεδομένων<br>";
$mg2->lang['viewlogfile']                         = "Κοίταξε το αρχείο log";
$mg2->lang['website']                             = "Επιστροφή στην αρχική σελίδα";
$mg2->lang['backtofolder']                        = "Επιστροφή...";
$mg2->lang['permerror1']                          = "PERMISSION ERROR: Δεν μπορείτε να γράψετε στο φάκελο  'root'!";
$mg2->lang['whattodo1']                           = "Chmod your gallery folder to 777";
$mg2->lang['permerror2']                          = "PERMISSION ERROR: Δεν μπορείτε να γράψετε στο φάκελο 'pictures'!";
$mg2->lang['whattodo2']                           = "Chmod your 'pictures' folder to 777";
$mg2->lang['permerror3']                          = "PERMISSION ERROR: Δεν μπορείτε να γράψετε στο αρχείο 'mg2db_idatabase.php'!";
$mg2->lang['whattodo3']                           = "Chmod 'mg2db_idatabase.php' file to 777";
$mg2->lang['permerror4']                          = "PERMISSION ERROR: Δεν μπορείτε να γράψετε στο αρχείο 'mg2db_idatabase_temp.php'!";
$mg2->lang['whattodo4']                           = "Chmod 'mg2db_idatabase_temp.php' file to 777";
$mg2->lang['permerror5']                          = "PERMISSION ERROR: Δεν μπορείτε να γράψετε στο αρχείο 'mg2db_fdatabase.php'!";
$mg2->lang['whattodo5']                           = "Chmod 'mg2db_fdatabase.php' file to 777";
$mg2->lang['permerror6']                          = "PERMISSION ERROR: Δεν μπορείτε να γράψετε στο αρχείο 'mg2db_fdatabase_temp.php'!";
$mg2->lang['whattodo6']                           = "Chmod 'mg2db_fdatabase_temp.php' file to 777";

?>
