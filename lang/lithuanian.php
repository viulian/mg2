<?php
/////////////////////////////////////////////////////////////////////////////////////////
//                                                                                     //
//                                    MG2 LANGAUGE FILE:                               //
//                                  http://www.minigal.dk                              //
//                                                                                     //
//                                         Lithuanian                                  //
//                                                                                     //
//                               TRANSLATED BY: Dominykas Speicys                      //
//                               EMAIL: domas.speicys@gmail.com                        //
//                                                                                     //
//                               LAST UPDATED: 16. Mar 2005                            //
//                                                                                     //
//         You are welcome to translate this file into your own language, but          //
//         be sure to check the Addon directory if your langauge is already            //
//         supported (http://addons.minigal.dk)                                        //
//                                                                                     //
//         Submit translated/updated language files to support@minigal.dk              //
//                                                                                     //
//         HOW TO TRANSLATE THIS FILE:                                                 //
//         Only edit the text to the right of the equal signs. Translate               //
//         this text to the language of your choice.                                   //
//         It is recommended to keep the letter cases intact in the                    //
//         finished translation. This will look the best.                              //
//                                                                                     //
/////////////////////////////////////////////////////////////////////////////////////////
//GALLERY LANGUAGE STRINGS
$mg2->lang['gallery']                             = "Galerija";
$mg2->lang['of']                                  = "is";
$mg2->lang['first']                               = "Pirmas";
$mg2->lang['prev']                                = "Buves";
$mg2->lang['next']                                = "Sekantis";
$mg2->lang['last']                                = "Paskutinis";
$mg2->lang['thumbs']                              = "Thumbs";
$mg2->lang['exif info']                           = "Exif informacija";
$mg2->lang['model']                               = "Modelis";
$mg2->lang['shutter']                             = "Shutterio greitis";
$mg2->lang['viewslideshow']                       = "Pradeti";
$mg2->lang['stopslideshow']                       = "Stabdyti";
$mg2->lang['aperture']                            = "Aperture";
$mg2->lang['flash']                               = "Blicas";
$mg2->lang['focallength']                         = "Zidinio nuotolis";
$mg2->lang['mm']                                  = "mm";
$mg2->lang['exposurecomp']                        = "Exposure compensation";
$mg2->lang['original']                            = "Originalus";
$mg2->lang['metering']                            = "Metering";
$mg2->lang['iso']                                 = "ISO";
$mg2->lang['seconds']                             = "s";
$mg2->lang['page']                                = "Puslapis";
$mg2->lang['all']                                 = "Visi";
$mg2->lang['fullsize']                            = "Rodyti pilno dydzio nuotrauka";
$mg2->lang['addcomment']                          = "Komentuoti";
$mg2->lang['name']                                = "Vardas";
$mg2->lang['email']                               = "Elektoninis pastas";
$mg2->lang['commentadded']                        = "Komentaras idetas";
$mg2->lang['commentexists']                       = "KLAIDA: Komentaras jau egzistuoja!";
$mg2->lang['commentmissing']                      = "KLAIDA: uzpildyti turi buti VISI laukeliai!";
$mg2->lang['enterpassword']                       = "Ivesk slaptazodi";
$mg2->lang['thissection']                         = "Si vieta apsaugota slaptazodziu";

// ADMIN LANGUAGE STRINGS
$mg2->lang['root']                                = "Pagrindinis";
$mg2->lang['thumb']                               = "Thumb";
$mg2->lang['dateadded']                           = "Idejimo data";
$mg2->lang['upload']                              = "Uploadinti failus";
$mg2->lang['import']                              = "Ideti uploadintus failus i";
$mg2->lang['newfolder']                           = "Naujas katalogas";
$mg2->lang['viewgallery']                         = "Ziureti galerija";
$mg2->lang['setup']                               = "Nustatymai";
$mg2->lang['logoff']                              = "Atsijungti";
$mg2->lang['menutxt_upload']                      = "Siusti";
$mg2->lang['menutxt_import']                      = "Ideti";
$mg2->lang['menutxt_newfolder']                   = "Naujas katalogas";
$mg2->lang['menutxt_viewgallery']                 = "Ziureti galerija";
$mg2->lang['menutxt_setup']                       = "Nustatymai";
$mg2->lang['menutxt_logoff']                      = "Atsijungti";
$mg2->lang['delete']                              = "Trinti";
$mg2->lang['cancel']                              = "Atsaukti";
$mg2->lang['ok']                                  = "OK";
$mg2->lang['deletefolder']                        = "Trinti kataloga";
$mg2->lang['navigation']                          = "Navigacija";
$mg2->lang['images']                              = "Paveiksliukas(-ai)(-u)";
$mg2->lang['filename']                            = "Failo pavadinimas";
$mg2->lang['title']                               = "Pavadinimas";
$mg2->lang['description']                         = "Apibudinimas";
$mg2->lang['setasthumb']                          = "Nustatyti kaip katalogo paveiksliuka";
$mg2->lang['editfolder']                          = "Redaguoti kataloga";
$mg2->lang['editimage']                           = "Redaguoti paveiksliuka";
$mg2->lang['nofolderselected']                    = "Nepasirinktas joks katalogas";
$mg2->lang['foldername']                          = "Katalogo pavadinimas";
$mg2->lang['newpassword']                         = "Naujas slaptazodis";
$mg2->lang['deletepassword']                      = "Trinti slaptazodi";
$mg2->lang['introtext']                           = "Intro text";
$mg2->lang['deletethumb']                         = "Trinti thumb";
$mg2->lang['moveto']                              = "Perkelti i";
$mg2->lang['id']                                  = "ID";
$mg2->lang['filesize']                            = "Failo dydis";
$mg2->lang['width']                               = "Plotis";
$mg2->lang['height']                              = "Aukstis";
$mg2->lang['date']                                = "Data";
$mg2->lang['ascending']                           = "Didejancia tvarka";
$mg2->lang['descending']                          = "Mazejancia tvarka";
$mg2->lang['newfolder']                           = "Naujas katalogas";
$mg2->lang['password']                            = "Slaptazodis";
$mg2->lang['direction']                           = "Direktorija";
$mg2->lang['sortby']                              = "Rusiuoti pagal";
$mg2->lang['gallerytitle']                        = "Galerijos pavadinimas";
$mg2->lang['adminemail']                          = "Administratoriaus e-mail'as";
$mg2->lang['language']                            = "Kalba";
$mg2->lang['skin']                                = "Apvalkalas";
$mg2->lang['dateformat']                          = "Datos formatas";
$mg2->lang['DDMMYY']                              = "DD MMM YYYY";
$mg2->lang['MMDDYY']                              = "MMM DD, YYYY";
$mg2->lang['MM.DD.YY']                            = "MM.DD.YY";
$mg2->lang['DD.MM.YY']                            = "DD.MM.YY";
$mg2->lang['YYYYMMDD']                            = "YYYYMMDD";
$mg2->lang['sendmail']                            = "Siusti komentaru e-mail'us";
$mg2->lang['foldericons']                         = "Force folder icons";
$mg2->lang['showexif']                            = "Rodyti Exif";
$mg2->lang['allowcomments']                       = "Leisti komentavimus";
$mg2->lang['copyright']                           = "Autoriniu teisiu zinute";
$mg2->lang['passwordchange']                      = "Keisti slaptazodi (3 x blankai = palikti dabartini)";
$mg2->lang['oldpasswordsetup']                    = "Ivesk dabartini slaptazodi";
$mg2->lang['newpasswordsetup']                    = "Naujas slaptazodis (blankas = naudoti dabartini)";
$mg2->lang['newpasswordsetupconfirm']             = "Ivesk nauja slaptazodi dar karta";
$mg2->lang['advanced']                            = "Pazangesni nustatymai";
$mg2->lang['allowedextensions']                   = "Leistini pletiniai";
$mg2->lang['imgwidth']                            = "Maksimalus paveiksliuko plotis (0 = be limitu)";
$mg2->lang['indexfile']                           = "Galerijos indexinis failas";
$mg2->lang['thumbquality']                        = "Thumbnailo kokybe";
$mg2->lang['uploadimport']                        = "Nepamirsk ideti uploadintu paveiksliuku!";
$mg2->lang['image']                               = "Paveiksliukas";
$mg2->lang['edit']                                = "Redaguoti";
$mg2->lang['editcurrentfolder']                   = "Redaguoti si kataloga";
$mg2->lang['deletecurrentfolder']                 = "Trinti si kataloga";
$mg2->lang['by']                                  = "pagal";
$mg2->lang['loginagain']                          = "Prisijungti is naujo";
$mg2->lang['securitylogoff']                      = "Security logoff";
$mg2->lang['autologoff']                          = "Tu automatiskai atsijungei po 15 minuciu neaktyvumo.";
$mg2->lang['logoff']                              = "Atsijungti";
$mg2->lang['forsecurity']                         = "Del saugumo rekomenduojama uzdaryti narsykle.";
$mg2->lang['upgradenote']                         = "<b><a href=\"http://www.minigal.dk/download.php\" target=\"blank\">Si versija yra X dienu senumo. Spausk cia, kad patikrintum, ar yra nauja versija!</a></b>";
$mg2->lang['updatesuccess']                       = "Atnaujinimas sekmingai baigtas";
$mg2->lang['renamefailure']                       = "KLAIDA: Failo pavadinime yra neleidziamu simboliu!";
$mg2->lang['filedeleted']                         = "Failas istrintas";
$mg2->lang['filenotfound']                        = "Failas nerastas!";
$mg2->lang['filesimported']                       = "failas(-ai)(-u) idetas(-i)(-a)";
$mg2->lang['nofilestoimport']                     = "KLAIDA: Nera failu idejimui!";
$mg2->lang['foldernotempty']                      = "KLAIDA: Katalogas nera tuscias!";
$mg2->lang['folderdeleted']                       = "Katalogas istrintas";
$mg2->lang['folderupdated']                       = "Katalogas atnaujintas";
$mg2->lang['foldercreated']                       = "Katalogas sukurtas";
$mg2->lang['folderexists']                        = "KLAIDA: Katalogas jau egzistuoja!";
$mg2->lang['filesuploaded']                       = "Failas(-ai) uploadinti";
$mg2->lang['settingssaved']                       = "Nustatymai issaugoti";
$mg2->lang['nopwdmatch']                          = "Nustatymai issaugoti<br /><br />KLAIDA: Slaptazodzio neatitikimas - naujas slaptazodis neissaugotas!";
$mg2->lang['filesmovedto']                        = "file(s) moved to";
$mg2->lang['filesdeleted']                        = "Failas(-ai)(-u) istrintas(-i)(-a)!";
$mg2->lang['file']                                = "failas";
$mg2->lang['files']                               = "failas(-ai)(-u)";
$mg2->lang['folder']                              = "katalogas";
$mg2->lang['folders']                             = "katalogas(-ai)(-u)";
$mg2->lang['rebuild']                             = "Perkurti";
$mg2->lang['rebuildimages']                       = "Perkurti thumbnails";
$mg2->lang['rebuildsuccess']                      = "Perkurimas baigtas";
$mg2->lang['donate']                              = "MG2 yra nemokamas softas, licenzijuotas pagal GPL. Paremti gali paspausdamas zemiau esanti mygtuka.";
$mg2->lang['from']                                = "Nuo";
$mg2->lang['comment']                             = "Komentaras";
$mg2->lang['comments']                            = "Komentarai";
$mg2->lang['by']                                  = "anot";
$mg2->lang['commentsdeleted']                     = "Komentaras(-ai) istintas(-i)";
$mg2->lang['buttonmove']                          = "Perkelti";
$mg2->lang['buttondelete']                        = "Istrinti";
$mg2->lang['deleteconfirm']                       = "Istrinti pazymetus failus?";
$mg2->lang['imagecolumns']                        = "Paveiksliuku stulpeliai";
$mg2->lang['imagerows']                           = "Paveiksliuku eilutes";
$mg2->lang['viewfolder']                          = "Rodyti kataloga";
$mg2->lang['viewimage']                           = "Rodyti paveiksliuka";
$mg2->lang['viewgallery']                         = "Ziureti galerija";
$mg2->lang['rotateright']                         = "Pasukti 90 laipsniu i desine";
$mg2->lang['rotateleft']                          = "Pasukti 90 laipsniu i kaire";
$mg2->lang['imagerotated']                        = "Paveiksliukas pasuktas!";
$mg2->lang['gifnotrotated']                       = "KLAIDA: .GIF failai negali buti pasukti del GD limitu!";
$mg2->lang['help']                                = "Pagalba";
$mg2->lang['slideshowdelay']                      = "Keitimo laikas";
$mg2->lang['websitelink']                         = "Webo linkas (tuscias blankas = nenaudoti)";
$mg2->lang['marknew']                             = "Pazymeti objektus, naujesnius kaip X dienu (0 = nenaudoti)";
$mg2->lang['folderempty']                         = "Sis katalogas yra tuscias";
$mg2->lang['noimage']                             = "Uzklaustas paveiksliukas neegzistuoja!";
?>
