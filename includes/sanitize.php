<?php

    /**
     * Backwards compatible behaviour as PHP changed how _count() behaves.
     * Calling it with uncountable object will produce the warning:
     * "Warning: _count(): Parameter must be an array or an object that implements Countable in C:\xampp\htdocs\mg2\admin.php on line 214"
     * This preserves the old behaviour: 
     */
    function _count($object) {
        if ($object === false) {
            return 1;
        }

        if (is_countable($object)) {
            return count($object);
        }

        return 0;
    }

    /**
     * Sanitize $_REQUEST[] array before it gets processed by the code.
     * 
     * CVE-2005-3432 describes how '*' can be sent as 'list' value, causing the code to
     * list all files irrespective of security settings, etc.
     * 
     */
    function sanitize_request() {

        // Code seems to have many guards against missing arguments, so for arguments that are
        // supposed to be integers, if the value is not integer, the value is reset to empty
        // string.
        sanitize_integer_arguments(['list', 'id', 'slideshow']);

        // For admin page too.
        sanitize_integer_arguments(['rebuildID', 'deleteID', 'deletefolder', 'deletepassword',
            'deletethumb', 'editID', 'editfolder', 'erasefolder', 'moveto', 'rebuildID', 'rotate']);

        // 'page' can be Integer or 'all', but nothing else.
        sanitize_page_argument();

        // Fatal error: Uncaught Error: Call to undefined method mg2db::changelanguage() in C:\xampp\htdocs\mg2\index.php:70
        $_REQUEST['changelang'] = "";
    }

    function sanitize_integer_arguments($argumentNames) {
        foreach($argumentNames as $argumentName) {

            // No value passed -> carry on ...
            if (!isset($_REQUEST[$argumentName])) {
                continue;
            }

            $value = $_REQUEST[$argumentName];
            $options = array(
                'options' => array(
                    'min_range' => 1
                )
            );

            $filteredValue = filter_var($value, FILTER_VALIDATE_INT, $options);
            if ($filteredValue !== false) {
                $_REQUEST[$argumentName] = strval($filteredValue);
                continue;
            }

            $_REQUEST[$argumentName] = "";
        }
    }

    function sanitize_page_argument() {

        // No value passed -> carry on ...
        if (!isset($_REQUEST['page'])) {
            return;
        }

        $value = $_REQUEST['page'];

        $options = array(
            'options' => array(
                'min_range' => 1
            )
        );

        $filteredValue = filter_var($value, FILTER_VALIDATE_INT, $options);
        if ($filteredValue !== false) {
            $_REQUEST['page'] = strval($filteredValue);
            return;
        }

        // 'all' is also allowed
        if ($value === 'all') {
            return;
        }

        // Not Integer nor 'all' ?
        $_REQUEST['page'] = "";
    }

    /**
     * Prevent 'undefined index' errors when accessing arrays by indexes that do not exist.
     */
    function getByIndex($array, $index) {
        if (isset($array[$index])) {
            return $array[$index];
        }

        return NULL;
    }
?>